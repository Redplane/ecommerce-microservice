﻿using Product.Domain.Entities;
using Product.Domain.SeedWork;

namespace Product.Infrastructure.Repositories
{
    public interface ICategoryRepository : IRepository<Category>
    {
    }
}
