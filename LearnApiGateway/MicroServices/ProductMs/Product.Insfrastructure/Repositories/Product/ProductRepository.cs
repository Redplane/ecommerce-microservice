﻿using Product.Domain.SeedWork;

namespace Product.Infrastructure.Repositories
{
    public class ProductRepository : BaseRepository<Domain.Entities.Product>, IProductRepository
    {
        public ProductRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
