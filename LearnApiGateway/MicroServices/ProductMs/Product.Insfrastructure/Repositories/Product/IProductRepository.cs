﻿using Product.Domain.SeedWork;

namespace Product.Infrastructure.Repositories
{
    public interface IProductRepository : IRepository<Domain.Entities.Product>
    {
    }
}
