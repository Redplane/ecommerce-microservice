﻿namespace Product.Business.ViewModels.Request
{
    public class CategoryViewModel
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public bool IsActive { get; set; }
    }
}
