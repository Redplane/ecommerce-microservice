﻿namespace Product.Business.ViewModels.Request
{
    public class SearchProductViewModel : BaseSearchViewModel
    {
        public string Name { get; set; }

        public double Price { get; set; }
    }
}