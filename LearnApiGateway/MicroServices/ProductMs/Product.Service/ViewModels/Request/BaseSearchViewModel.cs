﻿namespace Product.Business.ViewModels.Request
{
    public abstract class BaseSearchViewModel
    {
        public int Page { get; set; } = 1;

        public int Records { get; set; } = 10;

        public virtual int Skip => (Page - 1) * Records;
    }
}
