﻿using DomainShared.Models;
using Product.Business.ViewModels.Request;
using Product.Business.ViewModels.Response;
using System;
using System.Threading.Tasks;

namespace Product.Business.Services.Product
{
    public interface IProductService
    {
        ProductResponse Create(ProductViewModel model);

        Guid? Update(Guid id, ProductViewModel model);

        Guid? Delete(Guid id);

        Task<ProductResponse> Get(Guid id);

        Task<SearchResultValueObject<ProductResponse>> Search(SearchProductViewModel model);
    }
}
