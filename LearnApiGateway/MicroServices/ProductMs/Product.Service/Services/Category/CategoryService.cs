﻿using AutoMapper;
using DomainShared.Models;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using Product.Business.ViewModels.Request;
using Product.Business.ViewModels.Response;
using Product.Domain.SeedWork;
using Product.Infrastructure.Repositories;
using ServicesShared.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Product.Business.Services.Category
{
    public class CategoryService : ICategoryService
    {
        #region Fields
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICategoryRepository _categoryRepository;
        private readonly IBaseTimeService _timeService;
        private readonly IMapper _mapper;

        #endregion


        /// <summary>
        /// Inject Dependency Object
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="categoryRepository"></param>
        /// <param name="timeService"></param>
        public CategoryService(IUnitOfWork unitOfWork,
            ICategoryRepository categoryRepository,
            IBaseTimeService timeService,
            IMapper mapper
            )
        {
            _unitOfWork = unitOfWork;
            _categoryRepository = categoryRepository;
            _timeService = timeService;
            _mapper = mapper;
        }

        #region Methods

        /// <summary>
        /// Add new category
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public CategoryResponse Create(CategoryViewModel model)
        {
            var category = new Domain.Entities.Category
            {
                Name = model.Name,
                Description = model.Description,
                IsActive = model.IsActive,
                CreatedTime = _timeService.DateTimeUtcToUnix(DateTime.UtcNow)
            };

            return _mapper.Map<CategoryResponse>(category);
        }

        /// <summary>
        /// Update Category by Id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public Guid? Update(Guid id, CategoryViewModel model)
        {
            var categoryUpdate = _categoryRepository.GetById(id);

            if (categoryUpdate == null)
                throw new Exception("Category Not Found");

            categoryUpdate.IsActive = model.IsActive;
            categoryUpdate.Description = model.Description;
            categoryUpdate.UpdatedTime = _timeService.DateTimeUtcToUnix(DateTime.UtcNow);

            Domain.Entities.Category result = _categoryRepository.Update(categoryUpdate);

            return result.Id;
        }

        /// <summary>
        /// Delete Category by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Guid? Delete(Guid id)
        {
            var categoryToDelete = _categoryRepository.GetById(id);

            if (categoryToDelete == null)
                throw new Exception("Category Not Found");

            _categoryRepository.Delete(categoryToDelete);

            return id;
        }

        /// <summary>
        /// Get one category
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<CategoryResponse> Get(Guid id)
        {
            var catetory = await _categoryRepository.GetByIdAsync(id);

            if (catetory == null)
                throw new Exception("Category Not Found");

            return _mapper.Map<CategoryResponse>(catetory);
        }

        /// <summary>
        /// Search category by keyword
        /// </summary>
        /// <param name="keyWord"></param>
        /// <param name="pager"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<SearchResultValueObject<CategoryResponse>> Search(SearchCategoryViewModel model)
        {
            var query = _categoryRepository.AsQueryable();

            var keyWord = model.KeyWord;

            if (!string.IsNullOrEmpty(keyWord))
            {
                query = query.Where(c => c.Name.Contains(model.KeyWord) && c.IsActive);
            }

            // Get total pages
            var total = await ((IMongoQueryable<Domain.Entities.Category>)query).CountAsync();

            // Pagging
            query = query.Skip(model.Skip).Take(model.Records);

            var result = await ((IMongoQueryable<Domain.Entities.Category>)query).ToListAsync();

            var response = _mapper.Map<List<CategoryResponse>>(result);

            return new SearchResultValueObject<CategoryResponse>(response, total);
        }

        #endregion
    }
}
