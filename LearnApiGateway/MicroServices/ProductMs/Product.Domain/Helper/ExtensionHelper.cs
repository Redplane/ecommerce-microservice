﻿using System;

namespace Product.Domain.Helper
{
    public static class ExtensionHelper
    {
        public static Guid ToGuid(this string input)
        {
            return new Guid(input);
        }
    }
}
