﻿using Product.Domain.Helper;
using Product.Domain.SeedWork;
using System;
using System.Collections.Generic;

namespace Product.Domain.Entities
{
    [CollectionName("Products")]
    public class Product : Entity
    {
        public virtual Guid CategoryId { get; set; }

        public string Sku { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public double Price { get; set; }

        public string Photo { get; set; }

        public int TotalQuantity { get; set; }

        public int AvailableQuantity { get; set; }

        public bool IsActive { get; set; }

        public double CreatedTime { get; set; }

        public double UpdatedTime { get; set; }

        public List<string> Specifications { get; set; }
    }
}
