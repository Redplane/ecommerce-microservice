﻿using Product.Domain.Helper;
using Product.Domain.SeedWork;

namespace Product.Domain.Entities
{
    [CollectionName("Categories")]
    public class Category : Entity
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public double CreatedTime { get; set; }

        public double UpdatedTime { get; set; }

        public bool IsActive { get; set; }
    }
}
