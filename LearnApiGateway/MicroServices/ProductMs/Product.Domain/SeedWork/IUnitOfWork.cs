﻿using MongoDB.Driver;

namespace Product.Domain.SeedWork
{
    public interface IUnitOfWork
    {
        IMongoClient Client { get; }
        IMongoDatabase Database { get; }
        IMongoDatabase CreateNewDatabase();
    }
}
