﻿using MongoDB.Bson.Serialization.Attributes;
using System;

namespace Product.Domain.SeedWork
{
    public abstract class Entity : IEntity
    {
        [BsonId]
        public virtual Guid Id { get; set; }
    }
}
