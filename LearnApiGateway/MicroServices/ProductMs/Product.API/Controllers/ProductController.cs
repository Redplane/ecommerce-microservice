﻿using Microsoft.AspNetCore.Mvc;
using Product.Business.Services.Product;
using Product.Business.ViewModels.Request;
using Product.Business.ViewModels.Response;
using System;
using System.Threading.Tasks;

namespace Product.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductService _productService;

        public ProductController(IProductService productService)
        {
            _productService = productService;
        }

        // GET: api/Category
        [HttpGet("{id}")]
        public async Task<ActionResult> Get(Guid id)
        {
            ProductResponse response = await _productService.Get(id);

            return Ok(response);
        }

        [HttpGet]
        [Route("search")]
        public async Task<ActionResult> Search([FromQuery] SearchProductViewModel model)
        {
            var response = await _productService.Search(model);

            if (response == null)
                return NotFound();

            return Ok(response);
        }

        // POST: api/Product
        [HttpPost]
        public ActionResult Post([FromBody] ProductViewModel viewModel)
        {
            return Ok(_productService.Create(viewModel));
        }

        // PUT: api/Product/5
        [HttpPut("{id}")]
        public ActionResult Put(Guid id, [FromBody] ProductViewModel viewModel)
        {
            return Ok(_productService.Update(id, viewModel));
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
