﻿using Microsoft.AspNetCore.Mvc;
using Product.Business.Services.Category;
using Product.Business.ViewModels.Request;
using Product.Business.ViewModels.Response;
using System;
using System.Threading.Tasks;

namespace Product.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly ICategoryService _categoryService;

        public CategoryController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        // GET: api/Category/5
        [HttpGet("{id}")]
        public async Task<ActionResult> Get(Guid id)
        {
            CategoryResponse response = await _categoryService.Get(id);

            return Ok(response);
        }

        // GET: api/Category?keyWord = 5 & page = 1 & records = 10
        [HttpGet]
        [Route("search")]
        public async Task<ActionResult> Search([FromQuery] SearchCategoryViewModel model)
        {
            var response = await _categoryService.Search(model);

            if (response == null)
                return NotFound();

            return Ok(response);
        }

        // POST: api/Category
        [HttpPost]
        public ActionResult Post([FromBody] CategoryViewModel model)
        {
            return Ok(_categoryService.Create(model));
        }

        // PUT: api/Category/5
        [HttpPut("{id}")]
        public ActionResult Put(Guid id, [FromBody] CategoryViewModel model)
        {
            return Ok(_categoryService.Update(id, model));
        }

        // DELETE: api/Category/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
