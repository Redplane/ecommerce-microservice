﻿using AutoMapper;
using Product.Business.ViewModels.Response;
using Product.Domain.Entities;
using ServicesShared.Services.Implementations;

namespace Product.API.Configs
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Category, CategoryResponse>()
                .ForMember(des => des.CreatedTime, opt => opt.MapFrom(src => new BaseTimeService().UnixToDateTimeUtc(src.CreatedTime)))
                .ForMember(des => des.UpdatedTime, opt => opt.MapFrom(src => new BaseTimeService().UnixToDateTimeUtc(src.UpdatedTime)));

            CreateMap<Domain.Entities.Product, ProductResponse>()
                .ForMember(des => des.CreatedTime, opt => opt.MapFrom(src => new BaseTimeService().UnixToDateTimeUtc(src.CreatedTime)))
                .ForMember(des => des.UpdatedTime, opt => opt.MapFrom(src => new BaseTimeService().UnixToDateTimeUtc(src.UpdatedTime)));
        }
    }
}
