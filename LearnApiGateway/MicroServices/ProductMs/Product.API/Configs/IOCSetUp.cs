﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Bson.Serialization.Conventions;
using Product.Business.Services.Category;
using Product.Business.Services.Product;
using Product.Domain.SeedWork;
using Product.Infrastructure.Repositories;
using ServicesShared.Services.Implementations;
using ServicesShared.Services.Interfaces;

namespace Product.API.Configs
{
    public static class IOCSetUp
    {
        /// <summary>
        /// Run IOC setup.
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="services"></param>
        public static void Run(IConfiguration configuration, IServiceCollection services)
        {
            // Make mongodb save object with camel-cased naming convntion.
            var conventionPack = new ConventionPack { new CamelCaseElementNameConvention() };
            ConventionRegistry.Register("camelCase", conventionPack, t => true);

            services.AddScoped<IUnitOfWork>(provider =>
                new UnitOfWork(configuration.GetConnectionString("DefaultMongoDb")));

            services.AddScoped<IBaseTimeService, BaseTimeService>();

            services.AddScoped<ICategoryService, CategoryService>();
            services.AddScoped<ICategoryRepository, CategoryRepository>();

            services.AddScoped<IProductService, ProductService>();
            services.AddScoped<IProductRepository, ProductRepository>();
        }
    }
}
