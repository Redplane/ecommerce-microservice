﻿namespace OrderDomain.Enums
{
    public enum OrderStatuses
    {
        Pending,
        Paid,
        Rejected
    }
}