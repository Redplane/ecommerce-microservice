﻿namespace OrderDomain.Enums
{
    public enum ShoppingCartStatuses
    {
        Pending,
        Paid,
        Reserved,
        Cancelled
    }
}