﻿using System;
using OrderDomain.Enums;
using OrderDomain.Models.ValueObjects;

namespace OrderDomain.Entities
{
    public class Order
    {
        #region Properties

        public Guid Id { get; private set; }

        public Guid CustomerId { get; private set; }

        public Guid ShoppingCartId { get; private set; }
        
        public OrderStatuses Status { get; set; }

        public ShippingLocationValueObject ShippingAddress { get; set; }

        public double TotalMoney { get; set; }

        public double CreatedTime { get; set; }

        #endregion

        #region Constructor

        public Order(Guid id, Guid customerId, Guid shoppingCartId)
        {
            Id = id;
            CustomerId = customerId;
            ShoppingCartId = shoppingCartId;
        }

        #endregion
    }
}