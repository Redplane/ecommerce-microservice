﻿using System;
using OrderDomain.Enums;
using OrderDomain.Models.ValueObjects;

namespace OrderDomain.Entities
{
    public class ShoppingCart
    {
        #region Properties

        // ReSharper disable once AutoPropertyCanBeMadeGetOnly.Local
        public Guid Id { get; private set; }

        // ReSharper disable once AutoPropertyCanBeMadeGetOnly.Local
        public Guid CustomerId { get; private set; }

        public ShoppingCartItemValueObject[] Items { get; set; }

        public ShoppingCartStatuses Status { get; set; }

        public double CreatedTime { get; set; }

        #endregion

        #region Constructor

        public ShoppingCart(Guid id, Guid customerId)
        {
            Id = id;
            CustomerId = customerId;
        }

        #endregion
    }
}