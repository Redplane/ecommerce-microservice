﻿using System;
using System.Collections.Generic;
using DomainShared.Models;
using OrderDomain.Models;

namespace OrderDomain.Models.ValueObjects
{
    public class ShippingLocationValueObject : ValueObject
    {
        #region Properties

        public double Latitude { get; }

        public double Longitude { get; }

        #endregion

        #region Constructor

        public ShippingLocationValueObject(double latitude, double longitude)
        {
            Latitude = latitude;
            Longitude = longitude;

            if (latitude < -90 || latitude > 90)
                throw new Exception($"{nameof(Latitude)} cannot be either smaller than -90 or greater than 90");

            if (longitude < -180 || longitude > 180)
                throw new Exception($"{nameof(Longitude)} cannot be eitheer smaller than -180 or greater than 180");
        }

        #endregion

        #region Methods

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Longitude;
            yield return Latitude;
        }

        #endregion
    }
}