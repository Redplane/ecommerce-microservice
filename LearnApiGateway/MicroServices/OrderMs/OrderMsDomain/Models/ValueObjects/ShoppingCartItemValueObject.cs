﻿using System;
using System.Collections.Generic;
using DomainShared.Models;
using OrderDomain.Models;

namespace OrderDomain.Models.ValueObjects
{
    public class ShoppingCartItemValueObject : ValueObject
    {
        #region Properties

        public Guid ProductId { get; }

        public double Quantity { get; }

        public double TotalPrice { get; }

        #endregion

        #region Constructor

        public ShoppingCartItemValueObject(Guid productId, double quantity, double totalPrice)
        {
            ProductId = productId;
            Quantity = quantity;
            TotalPrice = totalPrice;
        }

        #endregion

        #region Methods

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return ProductId;
            yield return Quantity;
            yield return TotalPrice;
        }

        #endregion
    }
}