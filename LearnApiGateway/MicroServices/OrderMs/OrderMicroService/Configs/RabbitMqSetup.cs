﻿using System.Collections.Generic;
using System.Text;
using MessageBus;
using MessageBus.Interfaces;
using MessageBus.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace OrderApi.Configs
{
    public class RabbitMqSetup
    {
        #region Methods

        /// <summary>
        /// Run rabbit mq setup.
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="services"></param>
        public static void Run(IConfiguration configuration, IServiceCollection services)
        {
            var options = (Dictionary<string, RabbitMqOption>)configuration.GetSection("rabbitMq")
                .Get(typeof(Dictionary<string, RabbitMqOption>));

            //var rabbitMqService = new Rabbit;

            if (options == null)
                return;

            var rabbitMqBusManager = new RabbitMqManager();

            foreach (var key in options.Keys)
            {
                var rabbitMqOption = options[key];
                var connectionFactory = new ConnectionFactory();
                connectionFactory.HostName = rabbitMqOption.HostName;
                connectionFactory.Password = rabbitMqOption.Password;
                connectionFactory.Uri = rabbitMqOption.Endpoint;

                // Connection initialization.
                var connection = connectionFactory.CreateConnection();
                
                // Get all channels.
                var outgoingChannels = options[key].OutgoingChannels;
                foreach (var channel in outgoingChannels)
                {
                    var outgoingChannel = connection.CreateModel();
                    outgoingChannel.QueueDeclare(channel.Name, channel.Durable, channel.Exclusive, channel.AutoDelete);
                    rabbitMqBusManager.AddOrUpdateOutgoingChannel(channel.Name, outgoingChannel);
                }

                var incomingChannels = options[key].IncomingChannels;
                foreach (var channel in incomingChannels)
                {
                    var incomingChannel = connection.CreateModel();
                    incomingChannel.QueueDeclare(channel.Name, channel.Durable, channel.Exclusive, channel.AutoDelete);
                    var consumer = new EventingBasicConsumer(incomingChannel);
                    consumer.Received += (model, ea) =>
                    {
                        var body = ea.Body;
                        var message = Encoding.UTF8.GetString(body);
                        rabbitMqBusManager.InternalPublish(channel.Name, message);
                    };

                    incomingChannel.BasicConsume(channel.Name,
                        true,
                        consumer);
                    rabbitMqBusManager.AddOrUpdateIncommingChannel(channel.Name, incomingChannel);
                }
            }

            services.AddSingleton<IMessageBusManager<IModel>, RabbitMqManager>(x => rabbitMqBusManager);
        }

        #endregion
    }
}