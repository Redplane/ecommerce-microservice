﻿using FluentValidation;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using OrderApi.Validators;
using OrderApi.ViewModels;
using OrderApi.ViewModels.ShoppingCarts;

namespace OrderApi.Configs
{
    public class FluentValidationSetup
    {
        #region Methods

        /// <summary>
        /// Run fluent validation setup.
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="services"></param>
        public static void Run(IConfiguration configuration, IServiceCollection services)
        {
            // Project validator.
            services.AddScoped<AbstractValidator<SearchShoppingCartViewModel>, SearchShoppingCartValidator>();

            services.AddScoped<AbstractValidator<CheckoutOrderViewModel>, CheckoutOrderValidator>();
        }

        #endregion
    }
}