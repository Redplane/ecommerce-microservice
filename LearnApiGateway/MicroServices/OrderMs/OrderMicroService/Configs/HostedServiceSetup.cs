﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using OrderApi.HostedServices;
using OrderApi.HostedServices.ShoppingCarts;

namespace OrderApi.Configs
{
    public class HostedServiceSetup
    {
        /// <summary>
        /// Run hosted service setup.
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="services"></param>
        public static void Run(IConfiguration configuration, IServiceCollection services)
        {
            // Hosted services registrations.
            services.AddHostedService<ProductReservationDeniedHostedService>();
            services.AddHostedService<ShoppingCartCreditReservedHostedService>();
        }
    }
}