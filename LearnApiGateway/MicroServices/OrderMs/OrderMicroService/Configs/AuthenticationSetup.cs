﻿using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace OrderApi.Configs
{
    public class AuthenticationSetup
    {
        #region Methods

        /// <summary>
        ///     Run authentication setup.
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="services"></param>
        public static void Run(IConfiguration configuration, IServiceCollection services)
        {
            services
                .AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = IdentityServerAuthenticationDefaults.AuthenticationScheme;
                    options.DefaultScheme = IdentityServerAuthenticationDefaults.AuthenticationScheme;
                })
                .AddIdentityServerAuthentication(options =>
                {
                    options.Authority = "http://localhost:62744";
                    options.ApiName = "api1";
                    options.ApiSecret = "secret";
                    options.RequireHttpsMetadata = false;
                    options.SaveToken = true;
                });
        }

        #endregion
    }
}