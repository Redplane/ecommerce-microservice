﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Driver;
using OrderDomain.Entities;

namespace OrderApi.Configs
{
    public class MongoSetup
    {
        #region Methods

        /// <summary>
        /// Run mongo database setup.
        /// </summary>
        public static void Run(IConfiguration configuration, IServiceCollection services)
        {
            // Make mongodb save object with camel-cased naming convntion.
            var conventionPack = new ConventionPack { new CamelCaseElementNameConvention() };
            ConventionRegistry.Register("camelCase", conventionPack, t => true);

            // Resolve order database.
            var mongoDbConnectionString = configuration.GetConnectionString("DefaultMongoDb");
            services.AddScoped<IMongoClient>(options =>new MongoClient(new MongoUrl(mongoDbConnectionString)));
            services.AddScoped(options =>
            {
                var mongoClient = options.GetService<IMongoClient>();
                return mongoClient.GetDatabase("OrderDatabase");
            });
            
            // Add orders collection.
            services.AddScoped(options =>
            {
                // Get mongo client.
                var mongoDatabase = options.GetService<IMongoDatabase>();
                return mongoDatabase.GetCollection<Order>("orders");
            });

            // Add shopping carts collection.
            services.AddScoped(options =>
            {
                // Get mongo client.
                var mongoDatabase = options.GetService<IMongoDatabase>();
                return mongoDatabase.GetCollection<ShoppingCart>("shopping-carts");
            });
        }

        #endregion
    }
}