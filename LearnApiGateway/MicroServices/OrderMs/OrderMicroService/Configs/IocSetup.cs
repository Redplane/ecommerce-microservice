﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using OrderInfrastructure.Factories.Implementations;
using OrderInfrastructure.Factories.Interfaces;
using ServicesShared.Services.Implementations;
using ServicesShared.Services.Interfaces;

namespace OrderApi.Configs
{
    public class IocSetup
    {
        /// <summary>
        /// Run IOC setup.
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="services"></param>
        public static void Run(IConfiguration configuration, IServiceCollection services)
        {
            // Services registration.
            services.AddScoped<IBaseTimeService, BaseTimeService>();

            // Domains registration.
            services.AddScoped<IOrderFactory, OrderFactory>();
            services.AddScoped<IShoppingCartFactory, ShoppingCartFactory>();
        }
    }
}