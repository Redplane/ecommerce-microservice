﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Threading.Tasks;
using FluentValidation;
using IdentityModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Driver;
using OrderApi.ViewModels;
using OrderApi.ViewModels.ShoppingCarts;
using OrderDomain.Enums;
using OrderInfrastructure.Factories.Interfaces;
using ServicesShared.ViewModels;

namespace OrderApi.Controllers
{
    [Route("api/order")]
    public class OrderController : Controller
    {
        #region Constructor

        public OrderController(IOrderFactory orderFactory,
            IShoppingCartFactory shoppingCartFactory,
            IHttpContextAccessor httpContextAccessor,
            AbstractValidator<CheckoutOrderViewModel> checkoutOrderValidator)
        {
            // Factories injection.
            _orderFactory = orderFactory;
            _shoppingCartFactory = shoppingCartFactory;

            // Validators injection.
            _checkoutOrderValidator = checkoutOrderValidator;

            _httpContext = httpContextAccessor.HttpContext;
        }

        #endregion

        #region Properties       

        private readonly IOrderFactory _orderFactory;

        private readonly IShoppingCartFactory _shoppingCartFactory;
        
        private readonly AbstractValidator<CheckoutOrderViewModel> _checkoutOrderValidator;

        private readonly HttpContext _httpContext;

        #endregion

        #region Methods


        /// <summary>
        /// Add shopping cart asynchronously.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("checkout")]
        public async Task<IActionResult> CheckoutAsync([FromBody] CheckoutOrderViewModel model)
        {
            var validationResult = await _checkoutOrderValidator.ValidateAsync(model);
            if (!validationResult.IsValid)
                return BadRequest(validationResult.Errors);

            // Get user information.
            var user = _httpContext.User;
            if (user == null)
                throw new UnauthorizedAccessException();

            if (!Guid.TryParse(user.FindFirst(JwtRegisteredClaimNames.Sub)?.Value, out var userId))
                throw new UnauthorizedAccessException();

            var order = await _orderFactory.CheckoutAsync(userId, model.PurchasedProducts, 
                model.ShippingAddress);
            return Ok(order);
        }
        
        #endregion
    }
}