﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Threading.Tasks;
using FluentValidation;
using IdentityModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OrderApi.ViewModels.ShoppingCarts;
using OrderDomain.Enums;
using OrderInfrastructure.Factories.Interfaces;

namespace OrderApi.Controllers
{
    [Route("api/shopping-cart")]
    public class ShoppingCartController : Controller
    {
        #region Properties
        
        private readonly AbstractValidator<SearchShoppingCartViewModel> _searchShoppingValidator;

        private readonly IShoppingCartFactory _shoppingCartFactory;

        private readonly HttpContext _httpContext;

        #endregion

        #region Constructors

        public ShoppingCartController(
            AbstractValidator<SearchShoppingCartViewModel> searchShoppingCartValidator,
            IShoppingCartFactory shoppingCartFactory,
            IHttpContextAccessor httpContextAccessor)
        {
            // Valdators injection.
            _searchShoppingValidator = searchShoppingCartValidator;

            // Factory injection.
            _shoppingCartFactory = shoppingCartFactory;

            _httpContext = httpContextAccessor.HttpContext;
        }

        #endregion

        #region Methods
        
        /// <summary>
        /// Search shopping cart asynchronously.
        /// </summary>
        /// <param name="conditions"></param>
        /// <returns></returns>
        [HttpPost("search")]
        public async Task<IActionResult> SearchShoppingCartAsync([FromBody] SearchShoppingCartViewModel conditions)
        {
            var validationResult = await _searchShoppingValidator.ValidateAsync(conditions);
            if (!validationResult.IsValid)
                return BadRequest(validationResult.Errors);

            var loadShoppingCartsResult = await _shoppingCartFactory.FindShoppingCartsAsync(conditions.Ids,
                conditions.CustomerIds, conditions.Statuses, conditions.Pager);

            return Ok(loadShoppingCartsResult);
        }

        #endregion
    }
}