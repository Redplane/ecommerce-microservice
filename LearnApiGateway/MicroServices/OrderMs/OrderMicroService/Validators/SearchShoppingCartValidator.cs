﻿using FluentValidation;
using OrderApi.ViewModels.ShoppingCarts;

namespace OrderApi.Validators
{
    public class SearchShoppingCartValidator : AbstractValidator<SearchShoppingCartViewModel>
    {
        #region Constructor

        public SearchShoppingCartValidator()
        {
        }

        #endregion
    }
}