﻿using MessageBus.Constants;
using MessageBus.Interfaces;
using MessageBus.Models;
using MessageBus.Models.Events;
using MessageBus.Services.Implementations;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using OrderInfrastructure.Factories.Interfaces;
using RabbitMQ.Client;

namespace OrderApi.HostedServices.ShoppingCarts
{
    public class ProductSoldHostedService : MessageHostedService<ProductSoldEvent>
    {
        #region Properties

        private readonly IServiceScopeFactory _serviceScopeFactory;

        #endregion

        #region Constructors

        public ProductSoldHostedService(IMessageBusManager<IModel> messageBusManager, 
            IServiceScopeFactory serviceScopeFactory) : base(messageBusManager)
        {
            _serviceScopeFactory = serviceScopeFactory;
        }

        #endregion

        #region Methods

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        /// <param name="message"></param>
        protected override async void OnChannelMessageEmitted(string message)
        {
            if (string.IsNullOrEmpty(message))
                return;

            var mqMessage = JsonConvert.DeserializeObject<MqMessageBase<ProductSoldEvent>>(message);
            if (mqMessage.EventName != MqEventNameConstant.ProductSold)
                return;

            var productSoldEvent = mqMessage.AdditionalData;
            if (productSoldEvent == null)
                return;

            using (var serviceScope = _serviceScopeFactory.CreateScope())
            {
                var serviceProvider = serviceScope.ServiceProvider;

                // Approve order.
                var orderFactory = serviceProvider.GetService<IOrderFactory>();
                await orderFactory.ApproveOrderAsync(productSoldEvent.OrderId);
            }
        }

        #endregion
    }
}