﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MessageBus.Constants;
using MessageBus.Interfaces;
using MessageBus.Models;
using MessageBus.Models.Events;
using MessageBus.Services.Implementations;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using Newtonsoft.Json;
using OrderDomain.Enums;
using OrderInfrastructure.Factories.Interfaces;
using RabbitMQ.Client;
using ServicesShared.Services.Implementations;

namespace OrderApi.HostedServices.ShoppingCarts
{
    public class ProductReservationDeniedHostedService : MessageHostedService<ProductReservationDeniedEvent>
    {
        #region Properties

        private readonly ILogger _logger;

        private readonly IServiceScopeFactory _serviceScopeFactory;

        #endregion

        #region Constructor

        public ProductReservationDeniedHostedService(IMessageBusManager<IModel> messageBusManager,
            ILogger<ProductReservationDeniedHostedService> logger, IServiceScopeFactory serviceScopeFactory) : base(messageBusManager)
        {
            _logger = logger;
            _serviceScopeFactory = serviceScopeFactory;
        }

        #endregion

        #region Methods

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        /// <param name="message"></param>
        protected override void OnChannelMessageEmitted(string message)
        {
            try
            {
                var mqMessage = JsonConvert.DeserializeObject<MqMessageBase<ProductReservationDeniedEvent>>(message);

                if (MqEventNameConstant.ProductReservationDenied != mqMessage.EventName)
                    return;

                // No information specified. Information is ambiguous.
                var additionalData = mqMessage.AdditionalData;
                if (additionalData == null || additionalData.ShoppingCartId == Guid.Empty)
                    return;
                
                using (var serviceScope = _serviceScopeFactory.CreateScope())
                {
                    // Find service provider in scope.
                    var serviceProvider = serviceScope.ServiceProvider;

                    // Find shopping carts database.
                    var shoppingCartFactory = serviceProvider.GetService<IShoppingCartFactory>();
                    shoppingCartFactory.UpdateShoppingCartStatusAsync(additionalData.ShoppingCartId,
                        ShoppingCartStatuses.Cancelled);
                }
            }
            catch (Exception exception)
            {
                // Log the exception and continue running hosted service.
                _logger.LogError(exception, exception.Message);
            }
        }

        #endregion

    }
}