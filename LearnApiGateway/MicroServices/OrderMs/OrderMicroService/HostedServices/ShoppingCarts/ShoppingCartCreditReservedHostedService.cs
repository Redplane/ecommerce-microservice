﻿using System;
using MessageBus.Constants;
using MessageBus.Interfaces;
using MessageBus.Models;
using MessageBus.Models.Events;
using MessageBus.Services.Implementations;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using OrderDomain.Models.ValueObjects;
using OrderInfrastructure.Factories.Interfaces;
using RabbitMQ.Client;

namespace OrderApi.HostedServices.ShoppingCarts
{
    public class ShoppingCartCreditReservedHostedService : MessageHostedService<ShoppingCartCreditReservedEvent>
    {
        #region Properties

        private readonly IServiceScopeFactory _serviceScopeFactory;

        #endregion

        #region Constructors

        public ShoppingCartCreditReservedHostedService(IMessageBusManager<IModel> messageBusManager,
            IServiceScopeFactory serviceScopeFactory) : base(messageBusManager)
        {
            _serviceScopeFactory = serviceScopeFactory;
        }

        #endregion

        #region Methods

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        /// <param name="message"></param>
        protected override void OnChannelMessageEmitted(string message)
        {
            using (var serviceScope = _serviceScopeFactory.CreateScope())
            {
                if (string.IsNullOrEmpty(message))
                    return;

                var mqMessage = JsonConvert.DeserializeObject<MqMessageBase<ShoppingCartCreditReservedEvent>>(message);

                if (MqEventNameConstant.ShoppingCartCreditReserved != mqMessage.EventName)
                    return;

                // No information specified. Information is ambiguous.
                var additionalData = mqMessage.AdditionalData;
                if (additionalData == null || additionalData.ShoppingCartId == Guid.Empty)
                    return;

                // Publish a message about order created event to another micro services.
                var orderCreatedEvent = new OrderCreatedEvent(additionalData.OrderId, additionalData.CustomerId);
                var publishedMessage = new MqMessageBase<OrderCreatedEvent>(MqEventNameConstant.OrderCreated, orderCreatedEvent);
                MessageBusManager.Publish(MqChannelNameConstant.Order, publishedMessage);
            }
        }

        #endregion
    }
}