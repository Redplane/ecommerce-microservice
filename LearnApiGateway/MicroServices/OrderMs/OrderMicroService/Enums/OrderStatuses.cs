﻿namespace OrderApi.Enums
{
    public enum OrderStatuses
    {
        Pending,
        Approved,
        Cancelled
    }
}