﻿using System;
using System.Collections.Generic;
using OrderDomain.Models.ValueObjects;

namespace OrderApi.ViewModels
{
    public class CheckoutOrderViewModel
    {
        #region Properties

        /// <summary>
        /// Shopping cart item.
        /// </summary>
        public Dictionary<Guid, double> PurchasedProducts { get; set; }

        /// <summary>
        /// Shipping address.
        /// </summary>
        public ShippingLocationValueObject ShippingAddress { get; set; }

        #endregion
    }
}