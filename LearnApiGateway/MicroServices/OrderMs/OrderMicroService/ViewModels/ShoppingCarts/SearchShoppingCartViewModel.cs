﻿using System;
using System.Collections.Generic;
using DomainShared.Models;
using OrderDomain.Enums;

namespace OrderApi.ViewModels.ShoppingCarts
{
    public class SearchShoppingCartViewModel
    {
        #region Properties

        public HashSet<Guid> Ids { get; set; }
        
        public HashSet<Guid> CustomerIds { get; set; }

        public HashSet<ShoppingCartStatuses> Statuses { get; set; }

        public PagerValueObject Pager { get; set; }

        #endregion

        #region Constructor

        public SearchShoppingCartViewModel(HashSet<Guid> ids, HashSet<Guid> customerIds, 
            HashSet<ShoppingCartStatuses> statuses, 
            PagerValueObject pager)
        {
            Ids = ids;
            CustomerIds = customerIds;
            Statuses = statuses;
            Pager = pager;
        }

        #endregion
    }
}