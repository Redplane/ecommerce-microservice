﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using MessageBus.Constants;
using MessageBus.Interfaces;
using MessageBus.Models;
using MessageBus.Models.Events;
using Microsoft.EntityFrameworkCore;
using MongoDB.Driver;
using OrderDomain.Entities;
using OrderDomain.Enums;
using OrderDomain.Models.ValueObjects;
using OrderInfrastructure.Factories.Interfaces;
using RabbitMQ.Client;
using ServicesShared.Services.Interfaces;

namespace OrderInfrastructure.Factories.Implementations
{
    public class OrderFactory : IOrderFactory
    {
        #region Constructor

        public OrderFactory(IMessageBusManager<IModel> messageBusManager,
            IMongoCollection<Order> orders,
            IMongoCollection<ShoppingCart> shoppingCarts,
            IMongoClient mongoClient,
            IBaseTimeService baseTimeService)
        {
            _messageBusManager = messageBusManager;

            // Collections injection.
            _orders = orders;
            _shoppingCarts = shoppingCarts;

            // Services injection.
            _mongoClient = mongoClient;
            _baseTimeService = baseTimeService;
        }

        #endregion

        #region Properties

        private readonly IMessageBusManager<IModel> _messageBusManager;

        private readonly IMongoCollection<Order> _orders;

        private readonly IMongoCollection<ShoppingCart> _shoppingCarts;

        private readonly IMongoClient _mongoClient;

        private readonly IBaseTimeService _baseTimeService;

        #endregion

        #region Methods

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <returns></returns>
        public virtual async Task<Order> CheckoutAsync(Guid customerId,
            Dictionary<Guid, double> purchasedProducts,
            ShippingLocationValueObject shippingAddress,
            CancellationToken cancellationToken = default(CancellationToken))
        {

            var clientSession = await _mongoClient.StartSessionAsync(cancellationToken: cancellationToken);
            clientSession.StartTransaction();

            // Get current system unix time.
            var unixTime = _baseTimeService.DateTimeUtcToUnix(DateTime.UtcNow);

            try
            {
                var shoppingCartId = Guid.NewGuid();
                var shoppingCart = new ShoppingCart(Guid.NewGuid(), customerId);
                shoppingCart.Status = ShoppingCartStatuses.Pending;
                shoppingCart.Items = purchasedProducts
                    .Select(x => new ShoppingCartItemValueObject(x.Key, x.Value, 0))
                    .ToArray(); ;
                shoppingCart.CreatedTime = unixTime;
                await _shoppingCarts.InsertOneAsync(shoppingCart, new InsertOneOptions(),  cancellationToken);

                // Initialize order
                var orderId = Guid.NewGuid();
                var order = new Order(orderId, customerId, shoppingCart.Id);
                order.Status = OrderStatuses.Pending;
                order.ShippingAddress = shippingAddress;
                order.TotalMoney = 0;
                order.CreatedTime = _baseTimeService.DateTimeUtcToUnix(DateTime.UtcNow);
                await _orders.InsertOneAsync(order, new InsertOneOptions(), cancellationToken);
                
                //Made it here without error? Let's commit the transaction
                await clientSession.CommitTransactionAsync(cancellationToken);
                
                // Publish message queue event.
                var productReservedEvent = new ProductReservedEvent(purchasedProducts, shoppingCartId, orderId);
                _messageBusManager.Publish(MqChannelNameConstant.Product, productReservedEvent);

                return order;
            }
            catch
            {
                clientSession.AbortTransaction();
                throw;
            }
        }

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="status"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task UpdateOrderStatusAsync(Guid orderId, OrderStatuses status,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            IQueryable<Order> orders = _orders.AsQueryable();
            orders = orders.Where(x => x.Id == orderId);

            // Find first order asynchronously.
            var order = await orders.FirstOrDefaultAsync(cancellationToken);
            if (order == null)
                throw new Exception("Order not found");

            var updateOrderFilter = Builders<Order>.Filter.Eq(x => x.Id, orderId);
            var updateOrderBuilder = Builders<Order>.Update.Set(x => x.Status, status);
            await _orders.UpdateOneAsync(updateOrderFilter, updateOrderBuilder, cancellationToken: cancellationToken);
        }

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task ApproveOrderAsync(Guid orderId, CancellationToken cancellationToken = default(CancellationToken))
        {
            // Find orders.
            IQueryable<Order> orders = _orders.AsQueryable();
            orders =
                orders.Where(x => x.Id == orderId && x.Status == OrderStatuses.Pending);

            // Find first order.
            var order = await orders.FirstOrDefaultAsync(cancellationToken);

            if (order == null)
                throw new Exception("Order is not found");

            var clientSession = await _mongoClient.StartSessionAsync(cancellationToken: cancellationToken);
            clientSession.StartTransaction();

            try
            {
                // Update order.
                var orderFilter = Builders<Order>.Filter.Eq(x => x.Id, order.Id);
                var orderUpdateBuilder = Builders<Order>.Update.Set(x => x.Status, OrderStatuses.Paid);
                await _orders.UpdateOneAsync(orderFilter, orderUpdateBuilder, cancellationToken: cancellationToken);

                // Update shopping cart.
                var shoppingCartFilter = Builders<ShoppingCart>.Filter.Eq(x => x.Id, order.ShoppingCartId);
                var updateShoppingCartBuilder = Builders<ShoppingCart>.Update.Set(x => x.Status, ShoppingCartStatuses.Paid);
                await _shoppingCarts.UpdateOneAsync(shoppingCartFilter, updateShoppingCartBuilder, cancellationToken: cancellationToken);

                //Made it here without error? Let's commit the transaction
                await clientSession.CommitTransactionAsync(cancellationToken);
            }
            catch
            {
                clientSession.AbortTransaction();
                throw;
            }
        }

        #endregion
    }
}