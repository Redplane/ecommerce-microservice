﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using DomainShared.Models;
using MessageBus.Interfaces;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using OrderDomain.Entities;
using OrderDomain.Enums;
using OrderDomain.Models.ValueObjects;
using OrderInfrastructure.Factories.Interfaces;
using RabbitMQ.Client;
using ServicesShared.Services.Interfaces;

namespace OrderInfrastructure.Factories.Implementations
{
    public class ShoppingCartFactory : IShoppingCartFactory
    {
        #region Properties

        private readonly IMessageBusManager<IModel> _messageBusManager;

        private readonly IMongoCollection<ShoppingCart> _shoppingCarts;

        private readonly IBaseTimeService _baseTimeService;

        #endregion

        #region Constructor

        public ShoppingCartFactory(IMessageBusManager<IModel> messageBusManager,
            IMongoCollection<ShoppingCart> shoppingCarts,
            IBaseTimeService baseTimeService)
        {
            _messageBusManager = messageBusManager;
            _shoppingCarts = shoppingCarts;
            _baseTimeService = baseTimeService;
        }

        #endregion

        #region Methods

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        /// <param name="id"></param>
        /// <param name="customerId"></param>
        /// <param name="status"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task<ShoppingCart> FindShoppingCartAsync(Guid id, Guid? customerId, ShoppingCartStatuses? status,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            // Convert shopping carts to queryable list.
            IQueryable<ShoppingCart> shoppingCarts = _shoppingCarts.AsQueryable();
            shoppingCarts = shoppingCarts.Where(x => x.Id == id);

            if (customerId != null)
                shoppingCarts = shoppingCarts.Where(x => x.CustomerId == customerId.Value);

            if (status != null)
                shoppingCarts = shoppingCarts.Where(x => x.Status == status.Value);

            return await ((IMongoQueryable<ShoppingCart>)shoppingCarts).FirstOrDefaultAsync(cancellationToken);
        }

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="customerIds"></param>
        /// <param name="statuses"></param>
        /// <param name="pager"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task<SearchResultValueObject<ShoppingCart>> FindShoppingCartsAsync(HashSet<Guid> ids, HashSet<Guid> customerIds, HashSet<ShoppingCartStatuses> statuses, PagerValueObject pager,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            // Convert shopping carts to queryable list.
            IQueryable<ShoppingCart> shoppingCarts = _shoppingCarts.AsQueryable();

            if (ids != null)
                shoppingCarts = shoppingCarts.Where(x => ids.Contains(x.Id));

            if (customerIds != null)
                shoppingCarts = shoppingCarts.Where(x => customerIds.Contains(x.CustomerId));

            if (statuses != null)
                shoppingCarts = shoppingCarts.Where(x => statuses.Contains(x.Status));

            // Count total of shopping cart.
            var totalShoppingCarts = await ((IMongoQueryable<ShoppingCart>)shoppingCarts).CountAsync();

            // Pager is defined.
            if (pager != null)
            {
                shoppingCarts = shoppingCarts.Skip(pager.FindSkippedRecordsNumber())
                    .Take(pager.Records);
            }

            var loadedShoppingCarts = await ((IMongoQueryable<ShoppingCart>)shoppingCarts).ToListAsync();
            return new SearchResultValueObject<ShoppingCart>(loadedShoppingCarts, totalShoppingCarts);
        }

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="items"></param>
        /// <param name="status"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task<ShoppingCart> AddShoppingCartAsync(Guid customerId, ShoppingCartItemValueObject[] items, ShoppingCartStatuses status,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var shoppingCart = new ShoppingCart(Guid.NewGuid(), customerId);
            shoppingCart.Items = items;
            shoppingCart.Status = status;
            shoppingCart.CreatedTime = _baseTimeService.DateTimeUtcToUnix(DateTime.UtcNow);

            var options = new InsertOneOptions();
            options.BypassDocumentValidation = false;
            await _shoppingCarts.InsertOneAsync(shoppingCart, options, cancellationToken);

            // Publish message to product micro service.


            return shoppingCart;
        }

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        /// <param name="shoppingCartId"></param>
        /// <param name="status"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task UpdateShoppingCartStatusAsync(Guid shoppingCartId, ShoppingCartStatuses status,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var shoppingCartFilter = Builders<ShoppingCart>.Filter.Eq(x => x.Id, shoppingCartId);
            var updateShoppingCartBuilder = Builders<ShoppingCart>.Update.Set(x => x.Status, status);
            await _shoppingCarts.UpdateOneAsync(shoppingCartFilter, updateShoppingCartBuilder, cancellationToken: cancellationToken);
        }

        #endregion
    }
}