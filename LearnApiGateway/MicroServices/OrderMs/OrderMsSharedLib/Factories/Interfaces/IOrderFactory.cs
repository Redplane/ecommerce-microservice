﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using OrderDomain.Entities;
using OrderDomain.Enums;
using OrderDomain.Models.ValueObjects;

namespace OrderInfrastructure.Factories.Interfaces
{
    public interface IOrderFactory
    {
        #region Methods

        /// <summary>
        /// Add order asynchronously.
        /// </summary>
        /// <returns></returns>
        Task<Order> CheckoutAsync(Guid customerId,
            Dictionary<Guid, double> purchasedProducts,
            ShippingLocationValueObject shippingLocation,
            CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        /// Find order and update its status.
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="status"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task UpdateOrderStatusAsync(Guid orderId, OrderStatuses status, CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        /// Approve order asynchronously.
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task ApproveOrderAsync(Guid orderId, CancellationToken cancellationToken = default(CancellationToken));

        #endregion
    }
}