﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using DomainShared.Models;
using OrderDomain.Entities;
using OrderDomain.Enums;
using OrderDomain.Models.ValueObjects;

namespace OrderInfrastructure.Factories.Interfaces
{
    public interface IShoppingCartFactory
    {
        #region Methods

        /// <summary>
        /// Find shopping cart asynchronously.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="customerId"></param>
        /// <param name="status"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<ShoppingCart> FindShoppingCartAsync(Guid id, Guid? customerId, ShoppingCartStatuses? status,
            CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        /// Find shopping cart asynchronously
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="customerIds"></param>
        /// <param name="statuses"></param>
        /// <param name="pager"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<SearchResultValueObject<ShoppingCart>> FindShoppingCartsAsync(HashSet<Guid> ids,
            HashSet<Guid> customerIds, HashSet<ShoppingCartStatuses> statuses, PagerValueObject pager,
            CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        /// Add shopping cart asynchronously.
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="items"></param>
        /// <param name="status"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<ShoppingCart> AddShoppingCartAsync(Guid customerId, ShoppingCartItemValueObject[] items,
            ShoppingCartStatuses status, CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        /// Update shopping cart asynchronously.
        /// </summary>
        /// <param name="shoppingCartId"></param>
        /// <param name="status"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task UpdateShoppingCartStatusAsync(Guid shoppingCartId, ShoppingCartStatuses status,
            CancellationToken cancellationToken = default(CancellationToken));

        #endregion
    }
}