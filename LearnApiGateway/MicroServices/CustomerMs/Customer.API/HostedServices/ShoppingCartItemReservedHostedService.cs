﻿using System;
using CustomerApi.Application.Commands;
using MediatR;
using MessageBus.Constants;
using MessageBus.Interfaces;
using MessageBus.Models;
using MessageBus.Models.Events;
using MessageBus.Services.Implementations;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using RabbitMQ.Client;

namespace CustomerApi.HostedServices
{
    public class ShoppingCartItemReservedHostedService : MessageHostedService<ShoppingCartItemReservedEvent>
    {
        #region Constructor

        public ShoppingCartItemReservedHostedService(IMessageBusManager<IModel> messageBusManager,
            IServiceScopeFactory serviceScopeFactory) : base(messageBusManager)
        {
            _messageBusManager = messageBusManager;
            _serviceScopeFactory = serviceScopeFactory;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="message"></param>
        protected override async void OnChannelMessageEmitted(string message)
        {
            if (string.IsNullOrEmpty(message))
                return;

            var mqMessage = JsonConvert.DeserializeObject<MqMessageBase<ShoppingCartItemReservedEvent>>(message);
            if (mqMessage.EventName != MqEventNameConstant.ShoppingCartItemReserved)
                return;

            using (var serviceScope = _serviceScopeFactory.CreateScope())
            {
                var mediatorService = serviceScope.ServiceProvider.GetService<IMediator>();

                var additionalData = mqMessage.AdditionalData;

                if (additionalData == null)
                    return;

                // Update BalanceValueObject Of RemainingBalance BalanceValueObject
                var availableBalanceCommand = new UpdateAvailableBalanceCommand
                {
                    CustomerId = additionalData.CustomerId,
                    Total = additionalData.TotalPrice
                };

                var isMoneyUpdated = await mediatorService.Send(availableBalanceCommand);

                if (isMoneyUpdated)
                {
                    // Broadcast shopping cart credit reservation message.
                    var shoppingCartCreditReservedEvent =
                        new ShoppingCartCreditReservedEvent(additionalData.CustomerId, additionalData.ShoppingCartId, additionalData.OrderId);

                    _messageBusManager.Publish(MqChannelNameConstant.Order, shoppingCartCreditReservedEvent);
                }
                else
                {
                    var productInsufficientBalanceEvent =
                        new ProductBalanceInsufficientEvent(additionalData.CustomerId, additionalData.ShoppingCartId);
                    
                    _messageBusManager.Publish(MqChannelNameConstant.Product, productInsufficientBalanceEvent);
                }
            }
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Message bus manager to exchange real-time messages.
        /// </summary>
        private readonly IMessageBusManager<IModel> _messageBusManager;

        private readonly IServiceScopeFactory _serviceScopeFactory;

        /// <summary>
        ///     Subscription of message listener.
        ///     This should be disposed when service stops.
        /// </summary>
        private IDisposable _messageListenerSubscription;

        #endregion
    }
}