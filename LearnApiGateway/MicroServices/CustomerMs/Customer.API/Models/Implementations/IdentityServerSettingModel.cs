﻿namespace CustomerApi.Models.Implementations
{
    public class IdentityServerSettingModel
    {
        #region Properties
        
        public string Authority { get; set; }

        public string ApiSecret { get; set; }

        public string ApiName { get; set; }

        public bool RequireHttpsMetaData { get; set; }

        public bool SaveToken { get; set; }

        #endregion
    }
}