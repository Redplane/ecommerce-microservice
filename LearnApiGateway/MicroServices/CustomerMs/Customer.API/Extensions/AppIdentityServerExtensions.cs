﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using IdentityServer4;
using IdentityServer4.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;

namespace CustomerApi.Extensions
{
    public static class AppIdentityServerExtensions
    {
        public static void UseIdentityServerDataSeed(this IApplicationBuilder app)
        {
            // Get application services list.
            var applicationServices = app.ApplicationServices;
            using (var serviceScope = applicationServices.CreateScope())
            {
                // Service provider.
                var serviceProvider = serviceScope.ServiceProvider;

                // Mongo client.
                var mongoClient = serviceProvider.GetService<IMongoClient>();

                // Start database session and transaction.
                var session = mongoClient.StartSession();
                session.StartTransaction();

                // Get application logger.
                var logger = serviceProvider.GetService<ILogger<Startup>>();

                try
                {
                    // Whether collection has been created or not.
                    var hasClientSeeded = false;
                    var hasIdentityResourceSeeded = false;
                    var hasApiResourceSeeded = false;

                    // Insert identity resources.
                    var seedIdentityResourcesTask = SeedIdentityResourcesAsync(serviceProvider);

                    // Insert api resources
                    var seedApiResourcesTask = SeedApiResourcesAsync(serviceProvider);

                    // Insert clients.
                    var seedClientsTask = SeedClientsAsync(serviceProvider);

                    // List of tasks that must be completed.
                    Task.WhenAll(seedIdentityResourcesTask, seedApiResourcesTask, seedClientsTask)
                        .Wait();

                    hasIdentityResourceSeeded = seedIdentityResourcesTask.Result;
                    hasApiResourceSeeded = seedApiResourcesTask.Result;
                    hasClientSeeded = seedClientsTask.Result;

                    if (hasIdentityResourceSeeded)
                        logger.LogInformation("Identity has been seeded");

                    if (hasApiResourceSeeded)
                        logger.LogInformation("Api resource has been seeded");

                    if (hasClientSeeded)
                        logger.LogInformation("Client has been seeded");
                }
                catch (Exception exception)
                {
                    logger.LogError(exception.Message);
                }
            }
        }

        /// <summary>
        ///     Seed client asynchronously.
        /// </summary>
        /// <param name="applicationServices"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        private static async Task<bool> SeedClientsAsync(IServiceProvider applicationServices,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var clients = applicationServices.GetService<IMongoCollection<Client>>();
            // No client is found.
            if (clients.CountDocuments(FilterDefinition<Client>.Empty) > 0)
                return false;

            var predefinedClients = LoadClients();
            if (predefinedClients == null || !predefinedClients.Any())
                return false;

            await clients.InsertManyAsync(predefinedClients, null, cancellationToken);

            return true;
        }

        /// <summary>
        ///     Seed identity resource.
        /// </summary>
        /// <returns></returns>
        private static async Task<bool> SeedIdentityResourcesAsync(IServiceProvider applicationServices,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var identityResources = applicationServices.GetService<IMongoCollection<IdentityResource>>();

            // Identity resource is found.
            if (identityResources.CountDocuments(FilterDefinition<IdentityResource>.Empty) > 0)
                return false;

            // List of tasks that must be done.
            var insertIdentityResourceTasks = new List<Task>();
            insertIdentityResourceTasks.Add(identityResources.InsertOneAsync(new IdentityResources.OpenId(), null,
                cancellationToken));
            insertIdentityResourceTasks.Add(identityResources.InsertOneAsync(new IdentityResources.Profile(), null,
                cancellationToken));

            await Task.WhenAll(insertIdentityResourceTasks.ToArray());
            return true;
        }

        /// <summary>
        ///     Seed api resource asynchronously.
        /// </summary>
        /// <param name="applicationServices"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        private static async Task<bool> SeedApiResourcesAsync(IServiceProvider applicationServices,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var apiResources = applicationServices.GetService<IMongoCollection<ApiResource>>();

            // No client is found.
            if (apiResources.CountDocuments(FilterDefinition<ApiResource>.Empty) > 0)
                return false;

            var predefinedApiResources = LoadApiResources();
            if (predefinedApiResources == null || !predefinedApiResources.Any())
                return false;

            await apiResources.InsertManyAsync(predefinedApiResources, null, cancellationToken);
            return true;
        }

        /// <summary>
        ///     Load pre-defined api resources.
        /// </summary>
        /// <returns></returns>
        private static List<ApiResource> LoadApiResources()
        {
            return new List<ApiResource>();
        }

        /// <summary>
        ///     Load pre-defined clients.
        /// </summary>
        /// <returns></returns>
        private static List<Client> LoadClients()
        {
            var clients = new List<Client>();

            var resourceOwnerPasswordClient = new Client();
            resourceOwnerPasswordClient.ClientId = "ro.client";
            resourceOwnerPasswordClient.AllowedGrantTypes = new List<string>();
            resourceOwnerPasswordClient.AllowedGrantTypes.Add(GrantType.ResourceOwnerPassword);
            resourceOwnerPasswordClient.ClientSecrets = new List<Secret>();
            resourceOwnerPasswordClient.ClientSecrets.Add(new Secret("secret".Sha256()));
            resourceOwnerPasswordClient.AllowedScopes = new List<string>();
            resourceOwnerPasswordClient.AllowedScopes.Add(IdentityServerConstants.StandardScopes.Profile);
            resourceOwnerPasswordClient.AllowOfflineAccess = true;
            resourceOwnerPasswordClient.RefreshTokenExpiration = TokenExpiration.Sliding;
            resourceOwnerPasswordClient.RefreshTokenUsage = TokenUsage.ReUse;
            resourceOwnerPasswordClient.SlidingRefreshTokenLifetime = 3600;
            clients.Add(resourceOwnerPasswordClient);

            var authorizationCodeClient = new Client();
            authorizationCodeClient.ClientId = "mvc";
            authorizationCodeClient.ClientName = "MVC Client";
            authorizationCodeClient.AllowedGrantTypes = GrantTypes.Hybrid;
            authorizationCodeClient.AllowedGrantTypes.Add("Google");
            authorizationCodeClient.AllowedGrantTypes.Add("Facebook");
            authorizationCodeClient.ClientSecrets = new List<Secret>();
            authorizationCodeClient.ClientSecrets.Add(new Secret("secret".Sha256()));
            authorizationCodeClient.RedirectUris = new List<string>();
            authorizationCodeClient.RedirectUris.Add("http://localhost:4300");
            authorizationCodeClient.RedirectUris.Add("https://oidcdebugger.com/debug");
            authorizationCodeClient.RedirectUris.Add("ionic-sso://token");
            authorizationCodeClient.PostLogoutRedirectUris = new List<string>();
            authorizationCodeClient.PostLogoutRedirectUris.Add("http://localhost:5002/signout-callback-oidc");
            authorizationCodeClient.RequirePkce = true;
            authorizationCodeClient.AllowedScopes = new List<string>();
            authorizationCodeClient.AllowedScopes.Add(IdentityServerConstants.StandardScopes.OpenId);
            authorizationCodeClient.AllowedScopes.Add(IdentityServerConstants.StandardScopes.Profile);
            authorizationCodeClient.AllowedScopes.Add("api1");
            authorizationCodeClient.AllowOfflineAccess = true;
            authorizationCodeClient.RequireConsent = false;
            authorizationCodeClient.AccessTokenType = AccessTokenType.Reference;
            authorizationCodeClient.AllowAccessTokensViaBrowser = true;
            clients.Add(authorizationCodeClient);

            return clients;
        }
    }
}