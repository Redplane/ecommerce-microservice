﻿using System;
using System.Reflection;
using CustomerApi.Application.Commands;
using CustomerApi.Application.Handlers;
using CustomerApi.Application.Pipelines.Behaviours;
using CustomerApi.Application.Validations;
using CustomerApi.AuthenticationHandlers.Requirements;
using CustomerApi.Configs;
using CustomerApi.Extensions;
using FluentValidation.AspNetCore;
using IdentityServer4.AccessTokenValidation;
using MediatR;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace CustomerApi
{
    public class Startup
    {
        #region Properties

        public IConfiguration Configuration { get; }

        #endregion

        #region Methods

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        /// <summary>
        ///     This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            // Setup rabbit mq.
            RabbitMqSetup.Run(Configuration, services);

            // Setup database.
            DatabaseSetup.Run(services, Configuration);

            // Cors setup.
            CorsSetup.Run(services, Configuration);

            // Add mediatr.
            services.AddMediatR(typeof(Startup).GetTypeInfo().Assembly);

            // Auto validate command
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(RequestValidationBehavior<,>));

            // Run ioc setup.
            IocSetup.Run(services, Configuration);

            // Setup authentication methods.
            AuthenticationSetup.Run(services, Configuration);
            

            services
                .AddMvc(options =>
                {
                    ////only allow authenticated users
                    var policy = new AuthorizationPolicyBuilder()
                        .RequireAuthenticatedUser()
                        .AddAuthenticationSchemes(IdentityServerAuthenticationDefaults.AuthenticationScheme,
                            CookieAuthenticationDefaults.AuthenticationScheme)
#if !ALLOW_ANONYMOUS
                        .AddRequirements(new SolidUserRequirement())
#endif
                        .Build();

                    options.Filters.Add(new AuthorizeFilter(policy));
                })
                .AddFluentValidation(options =>
                    options.RegisterValidatorsFromAssembly(typeof(AddCustomerCommandValidator).Assembly))
                .AddJsonOptions(x =>
                {
                    var camelCasePropertyNamesContractResolver = new CamelCasePropertyNamesContractResolver();
                    x.SerializerSettings.ContractResolver = camelCasePropertyNamesContractResolver;
                    x.SerializerSettings.DefaultValueHandling = DefaultValueHandling.Ignore;
                })
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        /// <summary>
        ///     This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();
            else
                app.UseHsts();

            // Seed authentication database.
            //AuthenticationSetup.Seed(app);

            app.UseStaticFiles();

            app.UseCors("AllowAll");
            app.UseHttpsRedirection();

            // Enable identity server.
            app.UseIdentityServer();

            // Enable authentication.
            app.UseAuthentication();

            // Seed default data to mongo db.
            app.UseIdentityServerDataSeed();

            // Enable swagger documentation.
            //app.UseSwagger();
            //app.UseSwaggerUi3();

#if USE_IN_MEMORY_SQL
            var serviceScope = app.ApplicationServices.CreateScope();
            var customerDbContext = serviceScope.ServiceProvider.GetService<CustomerDbContext>();
            customerDbContext.Database.EnsureCreated();
#endif
            // Enable mvc pipeline.
            app
                .UseMvcWithDefaultRoute();
        }

        #endregion
    }
}