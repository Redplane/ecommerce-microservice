﻿using MediatR;
using System;
using System.Runtime.Serialization;
using CustomerDomain.Entities;
using CustomerDomain.Models;

namespace CustomerApi.Application.Commands
{
    public class AddCustomerCommand : IRequest<User>
    {
        public BalanceValueObject Balance { get; set; }
    }
}
