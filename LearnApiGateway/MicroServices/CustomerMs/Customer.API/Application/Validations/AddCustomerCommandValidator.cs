﻿using CustomerApi.Application.Commands;
using FluentValidation;

namespace CustomerApi.Application.Validations
{
    public class AddCustomerCommandValidator : AbstractValidator<AddCustomerCommand>
    {
        public AddCustomerCommandValidator()
        {
            RuleFor(x => x.Balance)
                .NotEmpty();
        }
    }
}
