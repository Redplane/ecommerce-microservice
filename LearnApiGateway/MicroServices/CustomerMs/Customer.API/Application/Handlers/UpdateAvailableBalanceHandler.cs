﻿using CustomerApi.Application.Commands;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using CustomerInfrastructure.Factories.Interfaces;

namespace CustomerApi.Application.Handlers
{
    public class UpdateAvailableBalanceHandler : IRequestHandler<UpdateAvailableBalanceCommand, bool>
    {
        private readonly IMediator _mediator;
        private readonly IUserFactory _customerFactory;

        public UpdateAvailableBalanceHandler(IMediator mediator,
            IUserFactory customerFactory)
        {
            _mediator = mediator;
            _customerFactory = customerFactory;
        }

        public async Task<bool> Handle(UpdateAvailableBalanceCommand request, CancellationToken cancellationToken)
        {
            return await _customerFactory.UpdateAvailableBalance(request.CustomerId, request.Total);
        }
    }
}
