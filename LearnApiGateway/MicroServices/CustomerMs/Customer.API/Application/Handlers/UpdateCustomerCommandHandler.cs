﻿using System.Threading;
using System.Threading.Tasks;
using CustomerApi.Application.Commands;
using CustomerInfrastructure.Factories.Interfaces;
using MediatR;

namespace CustomerApi.Application.Handlers
{
    public class UpdateCustomerCommandHandler : IRequestHandler<UpdateCustomerCommand, bool>
    {
        private readonly IMediator _mediator;
        private readonly IUserFactory _customerFactory;

        /// <summary>
        /// Inject DI 
        /// </summary>
        public UpdateCustomerCommandHandler(IMediator mediator,
            IUserFactory customerFactory)
        {
            _mediator = mediator;
            _customerFactory = customerFactory;
        }

        public async Task<bool> Handle(UpdateCustomerCommand request, CancellationToken cancellationToken)
        {
            return true;
        }
    }
}
