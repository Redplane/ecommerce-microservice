﻿using CustomerApi.Application.Commands;
using CustomerDomain.Entities;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using CustomerInfrastructure.Factories.Interfaces;

namespace CustomerApi.Application.Handlers
{
    /// <summary>
    /// This class for implement create a new customer
    /// </summary>
    public class AddCustomerCommandHandler : IRequestHandler<AddCustomerCommand, User>
    {
        #region Fields
        /// <summary>
        /// The Factory For Make CRUD Customer
        /// </summary>
        private readonly IUserFactory _customerFactory;

        /// <summary>
        /// The Abstract Mediator used for processing with command handle
        /// </summary>
        private readonly IMediator _mediator;
        #endregion

        #region Contructor

        /// <summary>
        ///     Inject ICustomerFactory and IMediator to contructor
        /// </summary>
        public AddCustomerCommandHandler(IMediator mediator,
            IUserFactory customerFactory)
        {
            _mediator = mediator;
            _customerFactory = customerFactory;
        }
        
        #endregion

        #region Methods
        /// <summary>
        /// Handle function for create new customer
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public  Task<User> Handle(AddCustomerCommand request, CancellationToken cancellationToken)
        {
            //// Init customer object with request input
            //var newCustomer = new User(Guid.NewGuid(), );

            //// Create new customer
            //var result = await _customerFactory.CreateCustomer(newCustomer);

            //// After Customer created return the Id of new customer
            //return result;
            return Task.FromResult(new User(Guid.NewGuid(), "username"));
        }
        #endregion
    }
}