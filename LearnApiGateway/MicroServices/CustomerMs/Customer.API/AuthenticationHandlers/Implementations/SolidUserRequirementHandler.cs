﻿using System;
using System.Threading.Tasks;
using CustomerApi.AuthenticationHandlers.Requirements;
using CustomerInfrastructure.Factories.Interfaces;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;

namespace CustomerApi.AuthenticationHandlers.Implementations
{
    public class SolidUserRequirementHandler : AuthorizationHandler<SolidUserRequirement>
    {
        #region Properties

        private readonly IUserFactory _userFactory;

        private readonly HttpContext _httpContext;
        
        #endregion

        #region Constructor

        public SolidUserRequirementHandler(IUserFactory userFactory, HttpContext httpContext)
        {
            _userFactory = userFactory;
            _httpContext = httpContext;
        }

        #endregion

        #region Methods

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        /// <param name="context"></param>
        /// <param name="requirement"></param>
        /// <returns></returns>
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, SolidUserRequirement requirement)
        {
            // Convert authorization filter context into authorization filter context.
            var authorizationFilterContext = (AuthorizationFilterContext)context.Resource;

            var httpUser = context.User;
            var a = 1;
            //// Get user access token.
            //var accessToken = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");

            //// Decode access token.
            //var claims = _appProfileService.DecodeJwt(accessToken);
            //if (claims == null || claims.Length < 1)
            //{
            //    context.Fail();
            //    return;
            //}

            //// Id is not in claims.
            //var claim = claims.FirstOrDefault(x => x.Type == ClaimConstants.UserId);
            //if (claim == null)
            //{
            //    context.Fail();
            //    return;
            //}

            //// Find user id claim.


            //// Get user unique identifier.
            //if (!Guid.TryParse(claim.Value, out var uuid))
            //{
            //    context.Fail();
            //    return;
            //}

            //// Find user by using id.
            //var user = await _userFactory.GetUserByIdAsync(uuid);
            //if (user == null)
            //{
            //    context.Fail();
            //    return;
            //}
            //// Do user profile mapping.
            //var userModel = _mapper.Map<UserViewModel>(user);
            //_appProfileService.SetProfile(userModel);
            context.Succeed(requirement);
            return Task.CompletedTask;
        }

        #endregion


    }
}