﻿using System.Threading.Tasks;
using CustomerApi.Application.Commands;
using CustomerApi.ViewModels;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CustomerApi.Controllers
{
    [Route("api/customer")]
    [AllowAnonymous]
    public class CustomerController : Controller
    {

        #region Properties

        private readonly IMediator _mediator;


        #endregion

        #region Constructor

        public CustomerController(IMediator mediator)
        {
            _mediator = mediator;
        }

        #endregion

        #region Methods
        
        /// <summary>
        /// Load customer asynchronously.
        /// </summary>
        /// <returns></returns>
        [HttpPost("search")]
        public virtual async Task<IActionResult> LoadCustomers([FromBody] LoadCustomerViewModel condition)
        {
            return Ok();
        }

        [HttpPost("")]
        public async Task<IActionResult> Post([FromBody] AddCustomerCommand command)
        {
            var result = await _mediator.Send(command);

            return Ok();
        }

        #endregion

    }
}