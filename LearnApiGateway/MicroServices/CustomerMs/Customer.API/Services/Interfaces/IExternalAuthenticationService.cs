﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace CustomerApi.Services.Interfaces
{
    public interface IExternalAuthenticationService
    {
        #region Methods

        /// <summary>
        /// Convert access token to facebook profile claim asynchronously.
        /// </summary>
        /// <returns></returns>
        Task<Dictionary<string, string>> AccessTokenToFacebookClaimsAsync(string accessToken, List<string> fields,
            CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        /// Convert id token to google claims list.
        /// </summary>
        /// <returns></returns>
        Task<Dictionary<string, string>> IdTokenToGoogleClaimAsync(string idToken, CancellationToken cancellationToken = default(CancellationToken));

        #endregion
    }
}