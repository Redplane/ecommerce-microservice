﻿using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using CustomerApi.Services.Interfaces;
using CustomerDomain.Enums;
using CustomerDomain.Models;
using CustomerInfrastructure.Aggregates;
using CustomerInfrastructure.Factories.Interfaces;
using CustomerInfrastructure.Services.Interfaces;
using IdentityModel;
using IdentityServer4.Models;
using IdentityServer4.Validation;
using Microsoft.AspNetCore.Http;

namespace CustomerApi.Services.Implementations.Authentications.ExtentionGrants
{
    public class FacebookExtensionGrant : IExtensionGrantValidator
    {
        #region Constructor

        public FacebookExtensionGrant(IExternalAuthenticationService externalAuthenticationService,
            IUserFactory userFactory,
            IBaseTimeService baseTimeService,
            IHttpContextAccessor httpContextAccessor)
        {
            _externalAuthenticationService = externalAuthenticationService;
            _userFactory = userFactory;
            _httpContext = httpContextAccessor.HttpContext;
            _baseTimeService = baseTimeService;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Exchange facebook access token with identity server 4 access token.
        ///     Account will be created automatically if it is not available.
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public virtual async Task ValidateAsync(ExtensionGrantValidationContext context)
        {
            // Get access token that has been passed in request body.
            var accessToken = context.Request.Raw.Get("access_token");

            if (string.IsNullOrEmpty(accessToken))
            {
                context.Result = new GrantValidationResult(TokenRequestErrors.InvalidRequest, "access_token_missing");
                return;
            }

            // Exchange access token with list of claims returned by facebook.
            // Get id token to claim.
            var requestedFields = new[] {"id", "name", "email", "birthday"};
            var claims =
                await _externalAuthenticationService.AccessTokenToFacebookClaimsAsync(accessToken,
                    requestedFields.ToList());

            if (!claims.TryGetValue(JwtClaimTypes.Id, out var id) || string.IsNullOrEmpty(id))
            {
                context.Result = new GrantValidationResult(TokenRequestErrors.InvalidRequest, "token_invalid");
                return;
            }

            // Find facebook user by using username.
            var user = await _userFactory.FindUserAsync(id, null, Providers.Facebook, null);
            if (user == null)
            {
                claims.TryGetValue(JwtClaimTypes.Name, out var name);
                claims.TryGetValue("birthday", out var birthdate);
                var birthday =
                    _baseTimeService.DateTimeUtcToUnix(birthdate, "MM/dd/yyyy");

                // Register user if user is not available.
                user = await _userFactory.AddUserAsync(id, null,
                    name,
                    birthday,
                    new BalanceValueObject(0, 0),
                    Providers.Facebook, CustomerStatuses.Active);
            }

            // User cannot be created.
            if (user == null || user.Status != CustomerStatuses.Active)
            {
                context.Result = new GrantValidationResult(TokenRequestErrors.UnauthorizedClient, "invalid_client");
                return;
            }

            var userCredential = new UserCredential(user);
            _httpContext.Items[ClaimTypes.UserData] = userCredential;

            context.Result = new GrantValidationResult(id, GrantType, claims.Select(x => new Claim(x.Key, x.Value)));
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Grant type.
        /// </summary>
        public string GrantType => "Facebook";

        private readonly IExternalAuthenticationService _externalAuthenticationService;

        /// <summary>
        ///     Handle user operations.
        /// </summary>
        private readonly IUserFactory _userFactory;

        private readonly HttpContext _httpContext;

        private readonly IBaseTimeService _baseTimeService;

        #endregion
    }
}