﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using CustomerDomain.Enums;
using CustomerInfrastructure.Aggregates;
using CustomerInfrastructure.Factories.Interfaces;
using IdentityModel;
using IdentityServer4.Models;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace CustomerApi.Services.Implementations.Authentications
{
    public class ProfileService : IProfileService
    {
        #region Constructor

        public ProfileService(IUserFactory userFactory, IHttpContextAccessor httpContextAccessor)
        {
            _httpContext = httpContextAccessor.HttpContext;
            _userFactory = userFactory;
        }

        #endregion

        #region Properties
        
        private readonly HttpContext _httpContext;

        private readonly IUserFactory _userFactory;

        #endregion

        #region Methods

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public Task GetProfileDataAsync(ProfileDataRequestContext context)
        {
            // Get user which is attached to http context.
            var userCredential = (UserCredential) _httpContext.Items[ClaimTypes.UserData];

            if (userCredential == null)
                throw new Exception("No user credential has been found in the incoming http request.");

            context.IssuedClaims = userCredential.GetClaims().ToList();
            return Task.CompletedTask;
        }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public virtual async Task IsActiveAsync(IsActiveContext context)
        {
            // Get subject from context (set in ResourceOwnerPasswordValidator.ValidateAsync),
            var username = context.Subject.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Name || x.Type == ClaimTypes.Name || x.Type == JwtClaimTypes.Subject)?.Value;
            if (string.IsNullOrEmpty(username))
                return;

            var userCredential = (UserCredential) _httpContext.Items[ClaimTypes.UserData];
            if (userCredential == null)
            {
                // Find active user by username.
                var user = await _userFactory.FindUserAsync(username, null, null, CustomerStatuses.Active);

                // Active user is not found.
                if (user == null)
                    return;

                _httpContext.Items[ClaimTypes.UserData] = new UserCredential(user);
            }

            context.IsActive = true;
        }

        #endregion
    }
}