﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using CustomerDomain.Enums;
using CustomerInfrastructure.Aggregates;
using CustomerInfrastructure.Factories.Interfaces;
using IdentityModel;
using IdentityServer4.Models;
using IdentityServer4.Validation;
using Microsoft.AspNetCore.Http;

namespace CustomerApi.Services.Implementations.Authentications
{
    public class ResourceOwnerPasswordValidator : IResourceOwnerPasswordValidator
    {
        #region Constructor

        public ResourceOwnerPasswordValidator(
            IUserFactory userFactory,
            IHttpContextAccessor httpContextAccessor)
        {
            _userFactory = userFactory;
            _httpContext = httpContextAccessor.HttpContext;
        }

        #endregion

        #region Properties
        
        private readonly HttpContext _httpContext;

        private readonly IUserFactory _userFactory;
        #endregion

        #region Methods

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public virtual async Task ValidateAsync(ResourceOwnerPasswordValidationContext context)
        {
            // Hash the password.
            var username = context.UserName;
            var password = context.Password;
            
            // Find the user in the system with defined username and password.
            var user = await _userFactory.FindUserAsync(username, password, Providers.Basic, CustomerStatuses.Active);

            // No user is found.
            if (user == null)
            {
                context.Result =
                    new GrantValidationResult(TokenRequestErrors.InvalidGrant, "invalid_credential");
                return;
            }

            var userCredential = new UserCredential(user);
            context.Result = new GrantValidationResult(
                user.Id.ToString("D"),
                OidcConstants.AuthenticationMethods.Password,
                userCredential.GetClaims());

            _httpContext.Items[ClaimTypes.UserData] = userCredential;
        }

        #endregion
    }
}