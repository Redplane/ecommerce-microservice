﻿using System.Reflection;
using CustomerApi.Models.Implementations;
using CustomerInfrastructure.Constants;
using CustomerInfrastructure.DbContexts;
using IdentityServer4.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Driver;

namespace CustomerApi.Configs
{
    public class DatabaseSetup
    {
        public static void Run(IServiceCollection services, IConfiguration configuration)
        {
#if USE_MYSQL
            services.AddDbContext<RelationshipDbContext>(optionsBuilder => optionsBuilder.UseMySql(Configuration.GetConnectionString("MySqlConnectionString"), b =>
            {
                b.MigrationsAssembly(Assembly.GetExecutingAssembly().GetName().Name);
                b.ServerVersion(new Version(5, 7, 25), ServerType.MySql);
            }));
#elif USE_SQL
            services.AddDbContext<CustomerDbContext>(optionsBuilder => optionsBuilder
                .UseSqlServer(configuration.GetConnectionString("customerMs"),
                    b => b.MigrationsAssembly(Assembly.GetExecutingAssembly().GetName().Name)));
#elif USE_IN_MEMORY_SQL
            services.AddDbContext<CustomerDbContext>(optionsBuilder => optionsBuilder
                .UseInMemoryDatabase("connectionStrings"));
#endif

            BsonClassMap.RegisterClassMap<Client>(cm =>
            {
                cm.AutoMap();
                cm.SetIgnoreExtraElements(true);
            });
            BsonClassMap.RegisterClassMap<IdentityResource>(cm =>
            {
                cm.AutoMap();
                cm.SetIgnoreExtraElements(true);
            });
            BsonClassMap.RegisterClassMap<ApiResource>(cm =>
            {
                cm.AutoMap();
                cm.SetIgnoreExtraElements(true);
            });
            BsonClassMap.RegisterClassMap<PersistedGrant>(cm =>
            {
                cm.AutoMap();
                cm.SetIgnoreExtraElements(true);
            });

            // Make mongodb save object with camel-cased naming convntion.
            var conventionPack = new ConventionPack();
            conventionPack.Add(new CamelCaseElementNameConvention());
            conventionPack.Add(new IgnoreExtraElementsConvention(true));
            conventionPack.AddClassMapConvention("LenientDiscriminator", m => m.SetDiscriminatorIsRequired(false));
            ConventionRegistry.Register("camelCase", conventionPack, t => true);

            // Resolve order database.
            var mongoDbConnectionString = configuration.GetConnectionString("is4-customer-ms");
            services.AddScoped<IMongoClient>(options => new MongoClient(new MongoUrl(mongoDbConnectionString)));
            services.AddScoped(options =>
            {
                var mongoClient = options.GetService<IMongoClient>();
                return mongoClient.GetDatabase("customer_ms_identity");
            });

            // Add identity resources collection.
            services.AddScoped(options =>
            {
                // Get mongo client.
                var mongoDatabase = options.GetService<IMongoDatabase>();
                return mongoDatabase.GetCollection<IdentityResource>(
                    IdentityServerCollectionConstants.IdentityResources);
            });


            // Add api resources collection.
            services.AddScoped(options =>
            {
                // Get mongo client.
                var mongoDatabase = options.GetService<IMongoDatabase>();
                return mongoDatabase.GetCollection<ApiResource>(IdentityServerCollectionConstants.ApiResources);
            });

            // Add clients collection.
            services.AddScoped(options =>
            {
                // Get mongo client.
                var mongoDatabase = options.GetService<IMongoDatabase>();
                return mongoDatabase.GetCollection<Client>(IdentityServerCollectionConstants.Clients);
            });

            // Add persisted grants collection.
            services.AddScoped(options =>
            {
                // Get mongo client.
                var mongoDatabase = options.GetService<IMongoDatabase>();
                return mongoDatabase.GetCollection<PersistedGrant>(IdentityServerCollectionConstants.PersistedGrants);
            });
        }
    }
}