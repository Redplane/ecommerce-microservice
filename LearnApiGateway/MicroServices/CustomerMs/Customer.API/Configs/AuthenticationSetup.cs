﻿using System.Collections.Generic;
using CustomerApi.Constants;
using CustomerApi.Models;
using CustomerApi.Models.Implementations;
using CustomerApi.Services.Implementations.Authentications;
using CustomerApi.Services.Implementations.Authentications.ExtentionGrants;
using CustomerInfrastructure.Factories.Implements;
using IdentityServer4;
using IdentityServer4.Models;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace CustomerApi.Configs
{
    public class AuthenticationSetup
    {
        #region Methods

        public static void Run(IServiceCollection services, IConfiguration configuration)
        {
            // Get identity server settings.
            var identityServerSettings = new IdentityServerSettingModel();
            configuration.GetSection(AppSettingConstants.IdentityServerSettings).Bind(identityServerSettings);

            services
                .AddIdentityServer()
                .AddClientStore<ClientFactory>()
                .AddResourceStore<ResourceFactory>()
                .AddPersistedGrantStore<PersistedGrantFactory>()
                .AddProfileService<ProfileService>()
                .AddResourceOwnerValidator<ResourceOwnerPasswordValidator>()
                .AddExtensionGrantValidator<GoogleExtensionGrant>()
                .AddExtensionGrantValidator<FacebookExtensionGrant>()
                .AddDeveloperSigningCredential();

            // Add jwt validation.
            services.AddAuthentication()
                .AddIdentityServerAuthentication(options =>
                {
                    options.Authority = identityServerSettings.Authority;
                    options.ApiName = identityServerSettings.ApiName;
                    options.ApiSecret = identityServerSettings.ApiSecret;
                    options.RequireHttpsMetadata = identityServerSettings.RequireHttpsMetaData;
                    options.SaveToken = identityServerSettings.SaveToken;
                })
                .AddGoogle("Google", options =>
                {
                    options.SignInScheme = IdentityServerConstants.ExternalCookieAuthenticationScheme;

                    options.ClientId = "323676358406-ikvol20relacv3mn5popdi79e5m759pc.apps.googleusercontent.com";
                    options.ClientSecret = "68pGK3guMhv_bdJKQOznblSi";
                })
                .AddFacebook("Facebook", options =>
                {
                    options.SignInScheme = IdentityServerConstants.ExternalCookieAuthenticationScheme;

                    options.ClientId = "1984309301812750";
                    options.ClientSecret = "683c6592465c8c1940356f42e710dfe3";
                });
        }

        #endregion

        #region Methods


        #endregion
    }
}