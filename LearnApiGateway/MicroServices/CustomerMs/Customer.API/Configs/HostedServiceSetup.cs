﻿using CustomerApi.HostedServices;
using Microsoft.Extensions.DependencyInjection;

namespace CustomerApi.Configs
{
    public class HostedServiceSetup
    {
        public static void Run(IServiceCollection services)
        {
            // Add hosted services.
            services.AddHostedService<ShoppingCartItemReservedHostedService>();
        }
    }
}