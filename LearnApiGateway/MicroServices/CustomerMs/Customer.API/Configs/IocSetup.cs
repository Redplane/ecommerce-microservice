﻿using CustomerApi.Services.Implementations.Authentications;
using CustomerApi.Services.Implementations.Authentications.ExtentionGrants;
using CustomerApi.Services.Interfaces;
using CustomerInfrastructure.Factories.Implements;
using CustomerInfrastructure.Factories.Interfaces;
using CustomerInfrastructure.Services.Implementations;
using CustomerInfrastructure.Services.Interfaces;
using IdentityServer4.Stores;
using IdentityServer4.Validation;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace CustomerApi.Configs
{
    public class IocSetup
    {
        /// <summary>
        /// Run Ioc setup.
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        public static void Run(IServiceCollection services, IConfiguration configuration)
        {
            // Register configuration.
            services.AddSingleton(configuration);

            // Register services.
            services.AddScoped<IBaseEncryptionService, BaseEncryptionService>();
            services.AddScoped<IBaseTimeService, BaseTimeService>();
            services.AddScoped<IExternalAuthenticationService, ExternalAuthenticationService>();

            // Register identity stores (factories)
            services.AddScoped<IClientStore, ClientFactory>();
            services.AddScoped<IResourceStore, ResourceFactory>();
            services.AddScoped<IPersistedGrantStore, PersistedGrantFactory>();
            
            // Add http client injectors.
            services.AddHttpClient();

            // Add http client to external authentication service.
            services.AddHttpClient< IExternalAuthenticationService, ExternalAuthenticationService >();
            
            services.AddHttpContextAccessor();

            // Register factory.
            services.AddScoped<IUserFactory, UserFactory>();

            // Register grant.
            services.AddScoped<GoogleExtensionGrant>();
            services.AddScoped<FacebookExtensionGrant>();
        }
    }
}