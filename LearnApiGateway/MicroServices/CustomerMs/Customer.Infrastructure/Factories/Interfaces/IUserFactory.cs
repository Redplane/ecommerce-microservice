﻿using System;
using System.Threading;
using System.Threading.Tasks;
using CustomerDomain.Entities;
using CustomerDomain.Enums;
using CustomerDomain.Models;

namespace CustomerInfrastructure.Factories.Interfaces
{
    public interface IUserFactory
    {
        #region Methods

        /// <summary>
        /// Add user into system asynchronously.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="fullName"></param>
        /// <param name="birthday"></param>
        /// <param name="balance"></param>
        /// <param name="provider"></param>
        /// <param name="status"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<User> AddUserAsync(string username, string password, string fullName, double? birthday,
            BalanceValueObject balance, Providers provider, CustomerStatuses status, 
            CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        /// Find user by using specific condition.
        /// </summary>
        /// <param name="username">Username belongs to user (required)</param>
        /// <param name="password">Original password(optional)</param>
        /// <param name="provider">(optional)</param>
        /// <param name="status">(optional)</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<User> FindUserAsync(string username, string password, Providers? provider, CustomerStatuses? status,
            CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        /// Find customer by using customer id.
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        Task<User> GetCustomer(Guid customerId);

        Task<Guid> CreateCustomer(User customer);

        Task UpdateCustomer(User customer);

        Task<bool> UpdateAvailableBalance(Guid customerId, double total);

        Task<bool> UpdateCurrentBalance(Guid customerId, double orderTotal);

        Task<Guid> DeleteCustomer(Guid customerId);

        #endregion
    }
}