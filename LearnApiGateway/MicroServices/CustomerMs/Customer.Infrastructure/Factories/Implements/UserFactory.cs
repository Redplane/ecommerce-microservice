﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CustomerDomain.Entities;
using CustomerDomain.Enums;
using CustomerDomain.Models;
using CustomerInfrastructure.DbContexts;
using CustomerInfrastructure.Factories.Interfaces;
using CustomerInfrastructure.Services.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace CustomerInfrastructure.Factories.Implements
{
    public class UserFactory : IUserFactory
    {
        #region Fields

        /// <summary>
        /// Customer micro-service database context.
        /// </summary>
        private readonly CustomerDbContext _dbContext;

        /// <summary>
        /// Service which is for data encryption | decryption.
        /// </summary>
        private readonly IBaseEncryptionService _baseEncryptionService;

        /// <summary>
        /// Service which is for handling system time.
        /// </summary>
        private readonly IBaseTimeService _baseTimeService;

        #endregion

        #region Contructor

        public UserFactory(IBaseEncryptionService baseEncryptionService, 
            IBaseTimeService baseTimeService,
            CustomerDbContext dbContext)
        {
            _dbContext = dbContext;
            _baseEncryptionService = baseEncryptionService;
            _baseTimeService = baseTimeService;
        }

        #endregion

        #region Methods

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="fullName"></param>
        /// <param name="birthday"></param>
        /// <param name="balance"></param>
        /// <param name="provider"></param>
        /// <param name="status"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task<User> AddUserAsync(string username, string password, string fullName, double? birthday, BalanceValueObject balance,
            Providers provider, CustomerStatuses status, CancellationToken cancellationToken = default(CancellationToken))
        {
            var user = new User(Guid.NewGuid(), username);

            if (!string.IsNullOrEmpty(password))
                user.HashedPassword = _baseEncryptionService.Md5Hash(password);

            user.FullName = fullName;
            user.Birthday = birthday;
            user.Balance = balance;
            user.Provider = provider;
            user.Status = status;

            // Update user joined time by getting current time.
            user.JoinedTime = _baseTimeService.DateTimeUtcToUnix(DateTime.UtcNow);

            var addedUser = _dbContext.Users.Add(user);
            await _dbContext.SaveChangesAsync(cancellationToken);

            return addedUser.Entity;
        }

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="provider"></param>
        /// <param name="status"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<User> FindUserAsync(string username, string password, Providers? provider, CustomerStatuses? status,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var users = _dbContext.Users.AsQueryable();

            // Find user by using username (case insensitive)
            users = users.Where(x => x.Username.Equals(username));

            if (!string.IsNullOrEmpty(password))
            {
                var hashedPassword = _baseEncryptionService.Md5Hash(password);
                users = users.Where(x =>
                    x.HashedPassword.Equals(hashedPassword, StringComparison.InvariantCultureIgnoreCase));
            }

            if (provider != null)
                users = users.Where(x => x.Provider == provider.Value);

            if (status != null)
                users = users.Where(x => x.Status == status.Value);

            return await users.FirstOrDefaultAsync(cancellationToken);
        }

        public async Task<User> GetCustomer(Guid customerId)
        {
            return await _dbContext.Users.FirstOrDefaultAsync(c => c.Id == customerId);
        }

        public async Task<Guid> CreateCustomer(User customer)
        {
            _dbContext.Users.Add(customer);

            await _dbContext.SaveChangesAsync();

            return customer.Id;
        }

        public Task UpdateCustomer(User customer)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> UpdateAvailableBalance(Guid customerId, double total)
        {
            // Find current customer
            var customer = await _dbContext.Users.FindAsync(customerId);

            if (customer == null)
            {
                throw new Exception("Customer Not Existed In Application");
            }

            // Checking If Total BalanceValueObject Greater Than AvailableBalance Then Block Transaction
            if (total > customer.Balance.AvailableBalance)
                return false;

            // Assign current money
            var currentMoney = customer.Balance.RemainingBalance;
            // ReCaculated available money
            var availableMoney = customer.Balance.AvailableBalance - total;

            var updateMoney = new BalanceValueObject(currentMoney, availableMoney);

            customer.Balance = updateMoney;

            // Update customer balanceValueObject 
            _dbContext.Users.Update(customer);

            // Save commited data
            await _dbContext.SaveChangesAsync();

            return true;
        }

        public async Task<bool> UpdateCurrentBalance(Guid customerId, double orderTotal)
        {
            // Find current customer
            var currentCustomer = await _dbContext.Users.FindAsync(customerId);

            if (currentCustomer == null)
            {
                throw new Exception("Customer Not Existed In Application");
            }

            // Validate RemainingBalance BalanceValueObject Amount Reserved 
            if (orderTotal > currentCustomer.Balance.RemainingBalance)
                return false;

            // Update RemainingBalance BalanceValueObject
            var changed = new BalanceValueObject(currentCustomer.Balance.RemainingBalance - orderTotal, currentCustomer.Balance.AvailableBalance);

            currentCustomer.Balance = changed;

            _dbContext.Users.Update(currentCustomer);

            await _dbContext.SaveChangesAsync();

            return true;
        }

        public async Task<Guid> DeleteCustomer(Guid customerId)
        {
            var customerDelete = await _dbContext.Users.FindAsync(customerId);

            if (customerDelete == null)
            {
                throw new Exception("Customer Not Existed In Application");
            }

            _dbContext.Users.Remove(customerDelete);
            await _dbContext.SaveChangesAsync();

            return customerId;
        }

        #endregion
    }
}