﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IdentityModel;
using IdentityServer4.Models;
using IdentityServer4.Stores;
using MongoDB.Driver;
using MongoDB.Driver.Linq;

namespace CustomerInfrastructure.Factories.Implements
{
    public class ResourceFactory : IResourceStore
    {
        #region Properties

        private readonly IMongoCollection<IdentityResource> _identityResources;

        private readonly IMongoCollection<ApiResource> _apiResources;
        

        #endregion

        #region Constructor

        public ResourceFactory(IMongoCollection<IdentityResource> identityResources, IMongoCollection<ApiResource> apiResources)
        {
            _identityResources = identityResources;
            _apiResources = apiResources;
        }

        #endregion

        #region Methods

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public async Task<ApiResource> FindApiResourceAsync(string name)
        {
            var apiResources = _apiResources.AsQueryable();
            return await apiResources.Where(x => x.Name == name).FirstOrDefaultAsync();
        }

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        /// <param name="scopeNames"></param>
        /// <returns></returns>
        public Task<IEnumerable<ApiResource>> FindApiResourcesByScopeAsync(IEnumerable<string> scopeNames)
        {
            var apiResources = _apiResources.AsQueryable();
            apiResources = apiResources.Where(x => scopeNames.Contains(x.Name));

            return Task.FromResult(apiResources.AsEnumerable());
        }

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        /// <param name="scopeNames"></param>
        /// <returns></returns>
        public Task<IEnumerable<IdentityResource>> FindIdentityResourcesByScopeAsync(IEnumerable<string> scopeNames)
        {
            var identityResources = _identityResources.AsQueryable();
            identityResources = identityResources.Where(x => scopeNames.Contains(x.Name));

            return Task.FromResult(identityResources.AsEnumerable());
        }

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        /// <returns></returns>
        public async Task<Resources> GetAllResourcesAsync()
        {
            var apiResources = await _apiResources.AsQueryable().ToListAsync();
            var identityResources = await _identityResources.AsQueryable().ToListAsync();

            return new Resources(identityResources, apiResources);
        }

        #endregion
    }
}