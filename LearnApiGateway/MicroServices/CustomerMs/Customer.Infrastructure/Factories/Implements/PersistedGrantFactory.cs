﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IdentityServer4.Models;
using IdentityServer4.Stores;
using Microsoft.EntityFrameworkCore;
using MongoDB.Driver;

namespace CustomerInfrastructure.Factories.Implements
{
    public class PersistedGrantFactory : IPersistedGrantStore
    {
        #region Properties

        private readonly IMongoCollection<PersistedGrant> _persistedGrants;

        #endregion

        #region Constructors

        public PersistedGrantFactory(IMongoCollection<PersistedGrant> persistedGrants)
        {
            _persistedGrants = persistedGrants;
        }

        #endregion

        #region Methods

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        /// <param name="grant"></param>
        /// <returns></returns>
        public async Task StoreAsync(PersistedGrant grant)
        {
            await _persistedGrants.InsertOneAsync(grant);
        }

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public async Task<PersistedGrant> GetAsync(string key)
        {
            var persistedGrants = _persistedGrants.AsQueryable();
            return await persistedGrants.Where(i => i.Key == key)
                .FirstOrDefaultAsync();
        }

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        /// <param name="subjectId"></param>
        /// <returns></returns>
        public Task<IEnumerable<PersistedGrant>> GetAllAsync(string subjectId)
        {
            var persistedGrants = _persistedGrants.AsQueryable();
            var result = persistedGrants.Where(i => i.SubjectId == subjectId);
            return Task.FromResult(result.AsEnumerable());
        }

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public Task RemoveAsync(string key)
        {
            var persistedGrantFilterDefinition = Builders<PersistedGrant>
                .Filter
                .Where(x => x.Key.ToLower() == key.ToLower());

            return _persistedGrants.DeleteManyAsync(persistedGrantFilterDefinition);
        }

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        /// <param name="subjectId"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public Task RemoveAllAsync(string subjectId, string clientId)
        {
            var lowercasedSubjectId = subjectId.ToLower();
            var lowercasedClientId = clientId.ToLower();

            var persistedGrantFilterDefinition = Builders<PersistedGrant>
                .Filter
                .Where(x => x.SubjectId.ToLower() == lowercasedSubjectId && x.ClientId == lowercasedClientId);

            return _persistedGrants.DeleteManyAsync(persistedGrantFilterDefinition);
        }

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        /// <param name="subjectId"></param>
        /// <param name="clientId"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public Task RemoveAllAsync(string subjectId, string clientId, string type)
        {
            var lowercasedSubjectId = subjectId.ToLower();
            var lowercasedClientId = clientId.ToLower();
            var lowercasedType = type.ToLower();

            var persistedGrantFilterDefinition = Builders<PersistedGrant>
                .Filter
                .Where(x => x.SubjectId.ToLower() == lowercasedSubjectId && x.ClientId == lowercasedClientId && x.Type == lowercasedType);

            return _persistedGrants.DeleteManyAsync(persistedGrantFilterDefinition);
        }

        #endregion
    }
}