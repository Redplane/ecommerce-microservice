﻿using CustomerDomain.Entities;
using CustomerDomain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CustomerInfrastructure.EntityConfigurations
{
    /// <summary>
    ///     Configuration schema of customer
    /// </summary>
    public class UserEntityTypeConfiguration : IEntityTypeConfiguration<User>
    {
        #region Methods

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="builder"></param>
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasKey(c => c.Id);

            // Balance value object persisted as owned entity in EF Core 2.0
            builder.OwnsOne(o => o.Balance)
                .Property(x => x.RemainingBalance)
                .HasColumnName(nameof(BalanceValueObject.RemainingBalance));

            builder.OwnsOne(o => o.Balance)
                .Property(x => x.AvailableBalance)
                .HasColumnName(nameof(BalanceValueObject.AvailableBalance));
        }

#endregion
    }
}