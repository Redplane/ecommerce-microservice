﻿using System;
using System.Globalization;
using CustomerInfrastructure.Services.Interfaces;

namespace CustomerInfrastructure.Services.Implementations
{
    public class BaseTimeService : IBaseTimeService
    {
        /// <summary>
        ///     Calculate the unix time from UTC DateTime.
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public double DateTimeUtcToUnix(DateTime dateTime)
        {
            return (dateTime - _utcDateTime).TotalMilliseconds;
        }

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        /// <param name="dateTime"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public double? DateTimeUtcToUnix(string dateTime, string format)
        {
            if (!DateTime.TryParseExact(dateTime, format, null, DateTimeStyles.None,
                out var birthdate))
                return null;


            return DateTimeUtcToUnix(birthdate);
        }

        /// <summary>
        ///     Convert unix time to UTC time.
        /// </summary>
        /// <param name="unixTime"></param>
        /// <returns></returns>
        public DateTime UnixToDateTimeUtc(double unixTime)
        {
            return _utcDateTime.AddMilliseconds(unixTime);
        }

        #region Properties

        private readonly DateTime _utcDateTime;

        public BaseTimeService()
        {
            _utcDateTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        }

        #endregion
    }
}