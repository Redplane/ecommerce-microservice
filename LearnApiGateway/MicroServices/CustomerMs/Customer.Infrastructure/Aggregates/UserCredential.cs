﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using CustomerDomain.Entities;
using IdentityModel;

namespace CustomerInfrastructure.Aggregates
{
    public class UserCredential
    {
        #region Constructor

        public UserCredential(User user)
        {
            _user = user;
        }

        #endregion

        #region Properties

        /// <summary>
        /// User information from database.
        /// </summary>
        private readonly User _user;

        #endregion

        #region Methods

        /// <summary>
        /// Get list of available claims that current user can have.
        /// </summary>
        public IList<Claim> GetClaims()
        {
            if (_user == null)
                throw new Exception("No user information has been attached into this identity credential");

            var claims = new List<Claim>();
            claims.Add(new Claim(JwtClaimTypes.Subject, _user.Id.ToString("D"), ClaimValueTypes.String));
            claims.Add(new Claim("username", _user.Username, ClaimValueTypes.String));
            
            // Birthday is valid.
            if (_user.Birthday != null)
                claims.Add(new Claim(JwtClaimTypes.BirthDate, $"{_user.Birthday}", ClaimValueTypes.Double));

            // Email is valid.
            if (!string.IsNullOrEmpty(_user.Email))
                claims.Add(new Claim(JwtClaimTypes.Email, $"{_user.Email}", ClaimValueTypes.String));
            
            claims.Add(new Claim("joinedTime", $"{_user.JoinedTime}", ClaimValueTypes.Double));
            claims.Add(new Claim("lastModifiedtime", $"{_user.LastModifiedTime}", ClaimValueTypes.Double));
            return claims;
        }

        /// <summary>
        /// Get user entity from credential.
        /// </summary>
        /// <returns></returns>
        public User ToUser()
        {
            return _user;
        }

        #endregion

    }
}