﻿using CustomerDomain.Entities;
using CustomerInfrastructure.EntityConfigurations;
using Microsoft.EntityFrameworkCore;

namespace CustomerInfrastructure.DbContexts
{
    /// <summary>
    ///     Customer Sql Db Context
    /// </summary>
    public class CustomerDbContext : DbContext
    {
        #region Constructor

        /// <summary>
        ///     Initialize db context with connection string pass
        /// </summary>
        public CustomerDbContext(DbContextOptions<CustomerDbContext> dbContextOptions) : base(
            dbContextOptions)
        {
        }

        #endregion

        #region Data set

        /// <summary>
        ///     Customer table
        /// </summary>
        public virtual DbSet<User> Users { get; set; }
        
        #endregion

        #region Methods

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(UserEntityTypeConfiguration).Assembly);
        }

        #endregion
    }
}