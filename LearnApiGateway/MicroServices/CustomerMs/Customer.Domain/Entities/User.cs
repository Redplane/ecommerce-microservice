﻿using System;
using CustomerDomain.Enums;
using CustomerDomain.Models;

namespace CustomerDomain.Entities
{
    public class User
    {
        #region Constructor

        public User(Guid id, string username)
        {
            Id = id;
            Username = username;
        }

        #endregion

        #region Properties

        // ReSharper disable once AutoPropertyCanBeMadeGetOnly.Local
        public Guid Id { get; private set; }

        // ReSharper disable once AutoPropertyCanBeMadeGetOnly.Local
        public string Username { get; private set; }

        public string Email { get; set; }

        public string HashedPassword { get; set; }

        public double? Birthday { get; set; }

        /// <summary>
        ///     How much money user is having.
        /// </summary>
        public BalanceValueObject Balance { get; set; }

        public string FullName { get; set; }

        public Providers Provider { get; set; }

        public CustomerStatuses Status { get; set; }

        public double JoinedTime { get; set; }

        public double? LastModifiedTime { get; set; }

        #endregion
    }
}