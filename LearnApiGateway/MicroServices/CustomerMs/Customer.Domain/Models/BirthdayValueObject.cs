﻿using System.Collections.Generic;
using DomainShared.Models;

namespace CustomerDomain.Models
{
    public class BirthdayValueObject : ValueObject
    {
        #region Properties

        // ReSharper disable once AutoPropertyCanBeMadeGetOnly.Local
        public double? Birthday { get; private set; }

        #endregion

        #region Constructors

        public BirthdayValueObject(double? birthday)
        {
            Birthday = birthday;
        }

        #endregion

        #region Methods

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return null;
        }

        #endregion



    }
}