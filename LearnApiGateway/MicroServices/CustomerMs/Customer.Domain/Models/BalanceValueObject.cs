﻿using System;
using System.Collections.Generic;
using DomainShared.Models;

namespace CustomerDomain.Models
{
    public class BalanceValueObject : ValueObject
    {
        #region Constructor

        public BalanceValueObject(double remainingBalance, double availableBalance)
        {
            if (remainingBalance < availableBalance)
                throw new Exception($"{nameof(remainingBalance)} cannot be smaller than {nameof(availableBalance)}");

            RemainingBalance = remainingBalance;
            AvailableBalance = availableBalance;
        }

        #endregion

        #region Properties

        /// <summary>
        ///     The remainingBalance amount money
        /// </summary>
        // ReSharper disable once AutoPropertyCanBeMadeGetOnly.Local
        public double RemainingBalance { get; private set; }

        /// <summary>
        ///     The availableBalance amount money
        /// </summary>
        // ReSharper disable once AutoPropertyCanBeMadeGetOnly.Local
        public double AvailableBalance { get; private set; }

        #endregion

        #region Methods

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return RemainingBalance;
            yield return AvailableBalance;
        }


        #endregion
    }
}