﻿namespace CustomerDomain.Enums
{
    public enum CustomerStatuses
    {
        Disabled,
        Pending,
        Active
    }
}