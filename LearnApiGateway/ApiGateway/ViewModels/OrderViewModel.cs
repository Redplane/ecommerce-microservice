﻿using System;

namespace ApiGateway.ViewModels
{
    public class OrderViewModel
    {
        #region Properties

        public Guid Id { get; set; }

        public double RequestedProductQuantity { get; set; }
        
        #endregion
    }
}