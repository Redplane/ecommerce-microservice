﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Ocelot.Middleware;
using Ocelot.Middleware.Multiplexer;
using Ocelot.Request.Middleware;

namespace ApiGateway.Aggregators
{
    public class LoadCustomerOrdersAggregator : IDefinedAggregator
    {
        #region Methods

        /// <summary>
        /// Load customer orders.
        /// </summary>
        /// <param name="responses"></param>
        /// <returns></returns>
        public Task<DownstreamResponse> Aggregate(List<DownstreamContext> responses)
        {
            throw new NotImplementedException();
        }

        #endregion


    }
}