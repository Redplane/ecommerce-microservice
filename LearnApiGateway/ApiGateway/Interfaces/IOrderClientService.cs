﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ApiGateway.ViewModels;

namespace ApiGateway.Interfaces
{
    public interface IOrderClientService
    {
        #region Properties

        /// <summary>
        /// Load order asynchronously.
        /// </summary>
        /// <returns></returns>
        Task<List<OrderViewModel>> LoadOrdersAsync();

        #endregion
    }
}