﻿using ApiGateway.Aggregators;
using ApiGateway.Handlers;
using ApiGateway.Interfaces;
using ApiGateway.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Ocelot.DependencyInjection;
using Ocelot.Middleware;

namespace ApiGateway
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<BaseHttpMessageHandler>();
            services
                .AddHttpContextAccessor();

            services
                .AddHttpClient<IOrderClientService, OrderClientService>()
                .AddHttpMessageHandler<BaseHttpMessageHandler>();
            
            // Add ocelot to pipeline.
            //services
            //    .AddOcelot()
            //    .AddTransientDefinedAggregator<LoadCustomerOrdersAggregator>(); ;

            services
                .AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();
            else
                app.UseHsts();

            app.UseHttpsRedirection();
            app.UseMvc();

            // Use ocelot.
            //app.UseOcelot()
            //    .Wait();
        }
    }
}