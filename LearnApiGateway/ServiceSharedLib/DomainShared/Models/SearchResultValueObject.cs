﻿using System.Collections.Generic;

namespace DomainShared.Models
{
    public class SearchResultValueObject<T> : ValueObject
    {
        #region Properties

        public IList<T> Records { get; }

        public long TotalRecords { get; }

        #endregion

        #region Constructors

        public SearchResultValueObject(IList<T> records, long totalRecords)
        {
            Records = records;
            TotalRecords = totalRecords;
        }

        #endregion

        #region Methods
        
        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Records;
            yield return TotalRecords;
        }

        #endregion
    }
}