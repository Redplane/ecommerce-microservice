﻿using System;

namespace DomainShared.Models
{
    public class PagerValueObject
    {
        #region Properties

        public int Page { get; }

        public int Records { get; }

        #endregion

        #region Constructor

        public PagerValueObject(int page, int records)
        {
            Page = page;
            Records = records;

            if (page < 1)
                throw new Exception($"{nameof(Page)} cannot be smaller than 1");

            if (records < 0)
                throw new Exception($"{nameof(Records)} cannot be smaller than 0");
        }

        #endregion

        #region Methods

        public int FindSkippedRecordsNumber()
        {
            var pageIndex = Page - 1;
            if (pageIndex < 0)
                pageIndex = 0;

            return pageIndex * Records;
        }

        #endregion
    }
}