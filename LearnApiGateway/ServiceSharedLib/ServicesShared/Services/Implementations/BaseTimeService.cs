﻿using ServicesShared.Services.Interfaces;
using System;

namespace ServicesShared.Services.Implementations
{
    public class BaseTimeService : IBaseTimeService
    {
        #region Methods

        /// <summary>
        ///     Calculate the unix time from UTC DateTime.
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public double DateTimeUtcToUnix(DateTime dateTime)
        {
            return (dateTime - _utcDateTime).TotalMilliseconds;
        }

        /// <summary>
        ///     Convert unix time to UTC time.
        /// </summary>
        /// <param name="unixTime"></param>
        /// <returns></returns>
        public DateTime UnixToDateTimeUtc(double unixTime)
        {
            return _utcDateTime.AddMilliseconds(unixTime);
        }

        #endregion

        #region Properties

        private readonly DateTime _utcDateTime;

        #endregion

        #region Constructor

        public BaseTimeService()
        {
            _utcDateTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        }

        #endregion
    }
}