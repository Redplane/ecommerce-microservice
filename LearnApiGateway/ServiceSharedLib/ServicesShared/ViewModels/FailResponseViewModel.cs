﻿namespace ServicesShared.ViewModels
{
    public class FailResponseViewModel
    {
        #region Properties

        public string Message { get; set; }

        #endregion

        #region Constructors

        public FailResponseViewModel()
        {
        }

        public FailResponseViewModel(string message)
        {
            Message = message;
        }

        #endregion
    }
}