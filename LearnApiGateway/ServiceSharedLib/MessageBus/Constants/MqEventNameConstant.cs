﻿namespace MessageBus.Constants
{
    public class MqEventNameConstant
    {
        #region Properties

        public const string OrderCreated = "ORDER_CREATED";

        /// <summary>
        /// Event which is raised by Customer service that credit has been reserved for an order successfully.
        /// </summary>
        public const string CreditReserved = "CREDIT_RESERVED";

        /// <summary>
        /// Event which is raised by Customer service that credit is not sufficient for order.
        /// </summary>
        public const string CreditLimitExceed = "CREDIT_LIMIT_EXCEEDED";

        /// <summary>
        /// Emitted when products are refused their reservation.
        /// </summary>
        public const string ProductReservationDenied = "PRODUCT_RESERVATION_DENIED";

        /// <summary>
        /// Emitted when shopping cart credit reserved when balance is enough.
        /// </summary>
        public const string ShoppingCartCreditReserved = "SHOPPING_CART_CREDIT_RESERVED";

        /// <summary>
        /// Emitted when product micro-service successfully reserves shopping cart items.
        /// </summary>
        public const string ShoppingCartItemReserved = "SHOPPING_CART_ITEM_RESERVED";

        /// <summary>
        /// Emitted when customer doesnt have enough money for reserving shopping product.
        /// </summary>
        public const string ProductInsufficientBalance = "PRODUCT_BALANCE_INSUFFICIENT";

        /// <summary>
        /// Emitted when product is sold successfully.
        /// </summary>
        public const string ProductSold = "PRODUCT_SOLD";

        /// <summary>
        /// Emitted when product is reserved.
        /// </summary>
        public const string ProductReserved = "PRODUCT_RESERVED";

        #endregion
    }
}