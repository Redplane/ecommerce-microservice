﻿namespace MessageBus.Constants
{
    public class MqChannelNameConstant
    {
        #region Propeties

        /// <summary>
        /// Channel which for receiving message from Customer service.
        /// </summary>
        public const string Customer = "channel-ms-customer";

        /// <summary>
        /// Channel for exchanging mesage from Order service.
        /// </summary>
        public const string Order = "channel-ms-order";

        /// <summary>
        /// Channel for exchaing message from Product service.
        /// </summary>
        public const string Product = "channel-ms-product";


        #endregion
    }
}