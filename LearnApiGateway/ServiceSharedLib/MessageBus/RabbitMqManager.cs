﻿using System.Collections.Concurrent;
using System.Reactive.Subjects;
using System.Text;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace MessageBus
{
    public class RabbitMqManager : MessageBusManagerBase<IModel>
    {
        #region Properties

        /// <summary>
        /// Mapping between rabbit mq channel with subject.
        /// </summary>
        private readonly ConcurrentDictionary<IModel, ISubject<string>> _channelToSubject;

        #endregion

        #region Constructor

        public RabbitMqManager()
        {
            _channelToSubject = new ConcurrentDictionary<IModel, ISubject<string>>();
        }

        #endregion

        #region Methods

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        /// <typeparam name="TMessage"></typeparam>
        /// <param name="channelName"></param>
        /// <param name="message"></param>
        public override void Publish<TMessage>(string channelName, TMessage message)
        {
            // Get channel by name.
            var channel = FindOutgoingChannel(channelName);
            if (channel == null)
                return;

            var serializedMessage = JsonConvert.SerializeObject(message);
            var encodedMessage = Encoding.UTF8.GetBytes(serializedMessage);
            channel.BasicPublish("", channelName, null, encodedMessage);
        }

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        /// <param name="name"></param>
        /// <param name="incomingChannel"></param>
        public override void AddOrUpdateIncommingChannel(string name, IModel incomingChannel)
        {
            var subject = new BehaviorSubject<string>(null);
            _channelToSubject.AddOrUpdate(incomingChannel, subject, (model, originalSubject) => subject);
            base.AddOrUpdateIncommingChannel(name, incomingChannel);
        }

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        /// <typeparam name="TMessage"></typeparam>
        /// <param name="channelName"></param>
        /// <param name="message"></param>
        public override void InternalPublish<TMessage>(string channelName, TMessage message)
        {
            var incomingChannel = FindIncomingChannel(channelName);
            if (incomingChannel == null)
                return;

            if (!_channelToSubject.TryGetValue(incomingChannel, out var subject))
                return;
            
            string serializedMessage = null;
            if ((message is string szMessage))
                serializedMessage = szMessage;
            else
                serializedMessage = JsonConvert.SerializeObject(message);

            subject.OnNext(serializedMessage);
        }

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public override ISubject<string> HookIncomingChannel(string name)
        {
            // Find channel by using name.
            var channel = FindIncomingChannel(name);
            if (channel == null)
                return null;

            if (!_channelToSubject.TryGetValue(channel, out var subject))
                return null;

            return subject;
        }

        #endregion
    }
}