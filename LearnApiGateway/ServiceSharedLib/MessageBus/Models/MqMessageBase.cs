﻿namespace MessageBus.Models
{
    public class MqMessageBase<T>
    {
        #region Properties

        /// <summary>
        /// Name of message event.
        /// </summary>
        public string EventName { get; set; }
        
        /// <summary>
        /// Additional data.
        /// </summary>
        public T AdditionalData { get; set; }

        #endregion

        #region Constructor

        public MqMessageBase()
        {
            
        }

        public MqMessageBase(string eventName, T additionalData)
        {
            EventName = eventName;
            AdditionalData = additionalData;
        }

        #endregion
    }
}