﻿namespace MessageBus.Models
{
    public class RabbitMqChannelOption
    {
        #region Properties

        public string Name { get; set; }

        public bool Durable { get; set; }

        public bool Exclusive { get; set; }

        public bool AutoDelete { get; set; }
        
        #endregion
    }
}