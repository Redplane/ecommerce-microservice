﻿using System;
using System.Collections.Generic;
using MessageBus.Interfaces;

namespace MessageBus.Models
{
    public class RabbitMqOption : IMqOptions
    {
        #region Properties

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        public Uri Endpoint { get; set; }

        /// <summary>
        /// The host to connect to.
        /// </summary>
        public string HostName { get; set; }

        /// <summary>
        /// Password to sign into the host.
        /// </summary>
        public string Password { get; set; }
        
        public List<RabbitMqChannelOption> OutgoingChannels { get; set; }

        public List<RabbitMqChannelOption> IncomingChannels { get; set; }

        #endregion

        #region Constructor

        public RabbitMqOption()
        {
        }

        public RabbitMqOption(Uri endpoint)
        {
            Endpoint = endpoint;
        }

        public RabbitMqOption(Uri endpoint, string hostName) : this(endpoint)
        {
            HostName = hostName;
        }

        public RabbitMqOption(Uri endpoint, string hostName, string password) : this(endpoint, hostName)
        {
            Password = password;
        }

        #endregion
    }
}