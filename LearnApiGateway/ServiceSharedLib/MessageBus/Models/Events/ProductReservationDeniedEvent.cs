﻿using System;

namespace MessageBus.Models.Events
{
    public class ProductReservationDeniedEvent
    {
        #region Properties

        /// <summary>
        /// Id of shopping cart whose products are refused to be reserved.
        /// </summary>
        public Guid ShoppingCartId { get; set; }

        #endregion
    }
}