﻿using System;

namespace MessageBus.Models.Events
{
    public class ShoppingCartCreditReservedEvent
    {
        #region Properties

        /// <summary>
        /// Id of customer who order belongs to.
        /// </summary>
        public Guid CustomerId { get; set; }

        /// <summary>
        /// Id of shopping cart that need to belonged to an order.
        /// </summary>
        public Guid ShoppingCartId { get; set; }

        public Guid OrderId { get; set; }

        #endregion

        #region Constructor

        public ShoppingCartCreditReservedEvent(Guid customerId, Guid shoppingCartId, Guid orderId)
        {
            CustomerId = customerId;
            ShoppingCartId = shoppingCartId;
            OrderId = orderId;
        }

        #endregion
    }
}