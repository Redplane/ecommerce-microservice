﻿using System;

namespace MessageBus.Models.Events
{
    public class OrderCreatedEvent
    {
        #region Properties

        public Guid OrderId { get; set; }

        public Guid CustomerId { get; set; }
        
        #endregion

        #region Constructor

        public OrderCreatedEvent()
        {
            
        }
        
        public OrderCreatedEvent(Guid orderId, Guid customerId)
        {
            OrderId = orderId;
            CustomerId = customerId;
        }

        #endregion
    }
}