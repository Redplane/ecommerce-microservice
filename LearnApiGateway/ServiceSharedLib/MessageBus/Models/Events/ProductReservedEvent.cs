﻿using System;
using System.Collections.Generic;

namespace MessageBus.Models.Events
{
    public class ProductReservedEvent
    {
        #region Properties
        
        public Dictionary<Guid, double> PurchasedProducts { get; set; }

        public Guid ShoppingCartId { get; set; }

        public Guid OrderId { get; set; }

        #endregion

        #region Constructors

        public ProductReservedEvent(Dictionary<Guid, double> purchasedProducts, Guid shoppingCartId, Guid orderId)
        {
            PurchasedProducts = purchasedProducts;
            ShoppingCartId = shoppingCartId;
            OrderId = orderId;
        }

        #endregion
    }
}