﻿using System;

namespace MessageBus.Models.Events
{
    public class ProductBalanceInsufficientEvent
    {
        #region Properties

        public Guid CustomerId { get; set; }

        public Guid ShoppingCartId { get; set; }

        #endregion

        #region Methods

        public ProductBalanceInsufficientEvent(Guid customerId, Guid shoppingCartId)
        {
            CustomerId = customerId;
            ShoppingCartId = shoppingCartId;
        }

        #endregion
    }
}