﻿using System;

namespace MessageBus.Models.Events
{
    public class ProductSoldEvent
    {
        #region Properties

        public Guid CustomerId { get; set; }

        public Guid OrderId { get; set; }

        public Guid ShoppingCartId { get; set; }

        #endregion

        #region Constructors

        public ProductSoldEvent(Guid customerId, Guid orderId, Guid shoppingCartId)
        {
            CustomerId = customerId;
            ShoppingCartId = shoppingCartId;
            OrderId = orderId;
        }

        #endregion
    }
}