﻿using System;

namespace MessageBus.Models.Events
{
    public class ShoppingCartItemReservedEvent
    {
        #region Properties
        
        public Guid CustomerId { get; set; }

        public Guid ShoppingCartId { get; set; }

        public double TotalPrice { get; set; }

        public Guid OrderId { get; set; }

        #endregion
    }
}