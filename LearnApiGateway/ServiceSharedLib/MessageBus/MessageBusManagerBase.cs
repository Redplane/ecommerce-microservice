﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Reactive.Subjects;
using MessageBus.Interfaces;
using MessageBus.Models;

namespace MessageBus
{
    public class MessageBusManagerBase<TChannel> : IMessageBusManager<TChannel>
    {
        #region Properties

        /// <summary>
        /// Mapping between message channel name and its instance.
        /// </summary>
        private readonly ConcurrentDictionary<string, TChannel> _incomingChannels;

        /// <summary>
        /// Mapping between message channel name and its instance.
        /// </summary>
        private readonly ConcurrentDictionary<string, TChannel> _outgoingChanels;

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        public ISubject<AddedMqEvent<TChannel>> OnOutgoingChannelCreated { get; }

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        public ISubject<AddedMqEvent<TChannel>> OnIncomingChannelCreated { get; }

        #endregion

        #region Constructor

        public MessageBusManagerBase()
        {
            _incomingChannels = new ConcurrentDictionary<string, TChannel>();
            _outgoingChanels = new ConcurrentDictionary<string, TChannel>();

            OnOutgoingChannelCreated = new BehaviorSubject<AddedMqEvent<TChannel>>(null);
            OnIncomingChannelCreated = new BehaviorSubject<AddedMqEvent<TChannel>>(null);
        }

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        /// <param name="name"></param>
        /// <param name="incomingChannel"></param>
        public virtual void AddOrUpdateIncommingChannel(string name, TChannel incomingChannel)
        {
            _incomingChannels.AddOrUpdate(name, incomingChannel, (channelName, originalChannel) => incomingChannel);
            var addedMqEvent = new AddedMqEvent<TChannel>(name, incomingChannel);
            OnIncomingChannelCreated.OnNext(addedMqEvent);
        }

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        /// <param name="name"></param>
        /// <param name="outgoingChannel"></param>
        public virtual void AddOrUpdateOutgoingChannel(string name, TChannel outgoingChannel)
        {
            _outgoingChanels.AddOrUpdate(name, outgoingChannel, (channelName, channel) => outgoingChannel);
            var addedMqEvent = new AddedMqEvent<TChannel>(name, outgoingChannel);
            OnOutgoingChannelCreated.OnNext(addedMqEvent);
        }

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        /// <typeparam name="TMessage"></typeparam>
        /// <param name="channelName"></param>
        /// <param name="message"></param>
        public virtual void InternalPublish<TMessage>(string channelName, TMessage message)
        {
            throw new NotImplementedException();
        }
        
        /// <summary>
        /// <inheritdoc />
        /// </summary>
        /// <typeparam name="TMessage"></typeparam>
        /// <param name="channelName"></param>
        /// <param name="message"></param>
        public virtual void Publish<TMessage>(string channelName, TMessage message)
        {
            throw new System.NotImplementedException();
        }
        
        /// <summary>
        /// Find incomming channel.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        protected virtual TChannel FindIncomingChannel(string name)
        {
            if (_incomingChannels == null || !_incomingChannels.ContainsKey(name))
                return default(TChannel);

            if (!_incomingChannels.TryGetValue(name, out var incomingChannel))
                return default(TChannel);

            return incomingChannel;
        }

        /// <summary>
        /// Find outgoing channel.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        protected virtual TChannel FindOutgoingChannel(string name)
        {
            if (_outgoingChanels == null || !_outgoingChanels.ContainsKey(name))
                return default(TChannel);

            if (!_outgoingChanels.TryGetValue(name, out var outgoingChannel))
                return default(TChannel);

            return outgoingChannel;
        }

        /// <summary>
        /// Hook to an incoming channel to listen to broadcasted message.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public virtual ISubject<string> HookIncomingChannel(string name)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Methods

        #endregion
    }
}