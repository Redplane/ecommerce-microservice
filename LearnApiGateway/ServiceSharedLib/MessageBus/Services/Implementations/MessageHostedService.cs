﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MessageBus.Constants;
using MessageBus.Interfaces;
using MessageBus.Models;
using Microsoft.Extensions.Hosting;
using RabbitMQ.Client;

namespace MessageBus.Services.Implementations
{
    public abstract class MessageHostedService<TEvent> : IHostedService
    {
        #region Constructor

        protected MessageHostedService(IMessageBusManager<IModel> messageBusManager)
        {
            MessageBusManager = messageBusManager;
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Message bus manager.
        /// </summary>
        protected readonly IMessageBusManager<IModel> MessageBusManager;

        /// <summary>
        ///     Subscription that listens to message received event.
        /// </summary>
        private IDisposable _messageReceivedSubscription;

        /// <summary>
        ///     Subscription that listens to incoming channel created event.
        /// </summary>
        private IDisposable _listenIncomingChannelCreatedSubscription;

        #endregion

        #region Methods

        /// <summary>
        ///     Start listening customer credit reserved event.
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual Task StartAsync(CancellationToken cancellationToken)
        {
            _listenIncomingChannelCreatedSubscription =
                MessageBusManager.OnIncomingChannelCreated.Subscribe(OnIncomingChannelCreated);

            return Task.CompletedTask;
        }

        /// <summary>
        ///     Stop listening customer credit reserved event.
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual Task StopAsync(CancellationToken cancellationToken)
        {
            _listenIncomingChannelCreatedSubscription?.Dispose();
            _messageReceivedSubscription?.Dispose();

            return Task.CompletedTask;
        }

        #endregion

        #region Events

        /// <summary>
        ///     Called when incoming channel is created.
        /// </summary>
        /// <param name="addedMqEvent"></param>
        protected virtual void OnIncomingChannelCreated(AddedMqEvent<IModel> addedMqEvent)
        {
            if (addedMqEvent == null)
                return;

            if (addedMqEvent.ChannelName != MqChannelNameConstant.Customer)
                return;

            _messageReceivedSubscription = MessageBusManager
                .HookIncomingChannel(addedMqEvent.ChannelName)
                .Subscribe(OnChannelMessageEmitted);
        }

        /// <summary>
        ///     On customer channel message is emitted.
        /// </summary>
        /// <param name="message"></param>
        protected abstract void OnChannelMessageEmitted(string message);

        #endregion
    }
}