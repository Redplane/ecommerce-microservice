﻿using System;

namespace MessageBus.Interfaces
{
    public interface IMqOptions
    {
        #region Methods
        
        /// <summary>
        /// Endpoint of message queue host.
        /// </summary>
        Uri Endpoint { get; set; }
        
        #endregion
    }
}