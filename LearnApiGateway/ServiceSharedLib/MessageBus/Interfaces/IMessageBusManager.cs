﻿using System.Reactive.Subjects;
using MessageBus.Models;

namespace MessageBus.Interfaces
{
    public interface IMessageBusManager<TChannel>
    {
        #region Methods

        /// <summary>
        /// Add an outgoing incomingChannel.
        /// </summary>
        /// <typeparam name="TChannel"></typeparam>
        /// <param name="name"></param>
        /// <param name="outgoingChannel"></param>
        void AddOrUpdateOutgoingChannel(string name, TChannel outgoingChannel);

        /// <summary>
        /// Add an incomming incomingChannel.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="incomingChannel"></param>
        void AddOrUpdateIncommingChannel(string name, TChannel incomingChannel);

        /// <summary>
        /// Publish a message to a specific mq name.
        /// </summary>
        /// <typeparam name="TMessage"></typeparam>
        /// <param name="channelName"></param>
        /// <param name="message"></param>
        void Publish<TMessage>(string channelName, TMessage message);

        /// <summary>
        /// Trigger a message internally.
        /// This method is for emitting a message to HookInCommmingChannel method to catch.
        /// 
        /// </summary>
        /// <typeparam name="TMessage"></typeparam>
        /// <param name="channelName"></param>
        /// <param name="message"></param>
        void InternalPublish<TMessage>(string channelName, TMessage message);

        /// <summary>
        /// Raise an event when outgoing channel is created.
        /// </summary>
        /// <returns></returns>
        ISubject<AddedMqEvent<TChannel>> OnOutgoingChannelCreated { get; }

        /// <summary>
        /// Raised an event when incoming channel is created.
        /// </summary>
        /// <returns></returns>
        ISubject<AddedMqEvent<TChannel>> OnIncomingChannelCreated { get; }

        /// <summary>
        /// Hook to incoming channel.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        ISubject<string> HookIncomingChannel(string name);


        #endregion
    }
}