﻿namespace SharedMq.Models
{
    public class AddedMqEvent<TChannel>
    {
        #region Properties

        public string ChannelName { get; set; }

        public TChannel ChannelInstance { get; set; }

        #endregion

        #region Constructor

        public AddedMqEvent()
        {
        }

        public AddedMqEvent(string channelName, TChannel channelInstance)
        {
            ChannelName = channelName;
            ChannelInstance = channelInstance;
        }

        #endregion
    }
}