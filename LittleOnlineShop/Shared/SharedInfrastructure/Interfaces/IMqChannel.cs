namespace SharedInfrastructure.Interfaces
{
    public interface IMqChannel
    {
        #region Properties
        
        /// <summary>
        /// Name of message queue channel.
        /// </summary>
        string Name { get; }
        
        #endregion
    }
}