﻿namespace SharedInfrastructure.Constants
{
    public class MqChannelNameConstant
    {
        #region Propeties

        /// <summary>
        /// Channel which for receiving message from Customer service.
        /// </summary>
        public const string Customer = "channel-customer";

        /// <summary>
        /// Channel for exchanging mesage from Order service.
        /// </summary>
        public const string Order = "channel-order";

        /// <summary>
        /// Channel for exchaing message from Product service.
        /// </summary>
        public const string Product = "channel-product";


        #endregion
    }
}