﻿namespace SharedInfrastructure.Constants
{
    public class MqEventNameConstant
    {
        #region Properties

        /// <summary>
        /// Emitted when balance has been deducted successfully.
        /// </summary>
        public const string SuccessfulBalanceDeduction = "SUCCESSFUL_BALANCE_DEDUCTION_EVENT";

        /// <summary>
        /// Emitted when balance has been deducted unsuccessfully.
        /// </summary>
        public const string FailedBalanceDeduction = "FAILED_BALANCE_DEDUCTION_EVENT";
        
        /// <summary>
        /// Emitted when product is reserved.
        /// </summary>
        public const string ProductBasketReservation = "PRODUCT_BASKET_RESERVED_EVENT";

        /// <summary>
        /// Emitted when product baskets have been checked out.
        /// </summary>
        public const string ProductBasketCheckOut = "PRODUCT_BASKET_CHECKED_OUT_EVENT";

        /// <summary>
        /// Emitted when product basket reservation cancellation.
        /// </summary>
        public const string ProductBasketReservationCancellation = "PRODUCT_BASKET_RESERVATION_CANCELLATION_EVENT";

        #endregion
    }
}