﻿namespace SharedInfrastructure.Constants
{
    public class SharedHttpMessageCodeConstants
    {
        #region Properties

        public const string InvalidGrant = "invalid_grant";

        public const string InvalidClaims = "invalid_claims";

        public const string InvalidSubject = "invalid_subject";

        public const string InvalidClient = "invalid_client";

        /// <summary>
        ///     Message code which responses to user when server has an error.
        /// </summary>
        public const string InternalServerError = "internal_server_error";

        public const string InvalidRequest = "invalid_request";

        #endregion
    }
}