﻿using System;
using System.Collections.Generic;
using System.Net;

namespace SharedInfrastructure.Exceptions
{
    public class HttpResponseException : Exception
    {
        #region Constructor

        public HttpResponseException(HttpStatusCode statusCode, string message) : base(message)
        {
            StatusCode = statusCode;
        }

        public HttpResponseException(HttpStatusCode statusCode, string message,
            Dictionary<string, object> additionalData) : base(message)
        {
            StatusCode = statusCode;
            AdditionalData = additionalData;
        }

        #endregion

        #region Properties

        public HttpStatusCode StatusCode { get; }

        public Dictionary<string, object> AdditionalData { get; }

        #endregion
    }
}