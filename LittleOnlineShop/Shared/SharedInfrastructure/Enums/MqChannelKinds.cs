namespace SharedInfrastructure.Enums
{
    public enum MqChannelKinds
    {
        Incoming,
        Outgoing
    }
}