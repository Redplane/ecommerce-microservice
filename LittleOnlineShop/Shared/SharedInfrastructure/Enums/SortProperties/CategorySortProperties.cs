﻿namespace SharedInfrastructure.Enums.SortProperties
{
    public enum CategorySortProperties
    {
        Name,
        CreatedTime,
        LastModifiedTime
    }
}