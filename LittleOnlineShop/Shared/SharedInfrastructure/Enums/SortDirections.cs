﻿namespace SharedInfrastructure.Enums
{
    public enum SortDirections
    {
        None,
        Ascending,
        Descending
    }
}