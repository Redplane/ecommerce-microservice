﻿using System.Collections.Generic;
using SharedDomain.Models;

namespace SharedInfrastructure.Models
{
    public class EditableFieldValueObject<T> : ValueObject
    {
        #region Constructor

        public EditableFieldValueObject(T value, bool hasModified)
        {
            Value = value;
            HasModified = hasModified;
        }

        #endregion

        #region Methods

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Value;
            yield return HasModified;
        }

        #endregion

        #region Properties

        public T Value { get; }

        public bool HasModified { get; }

        #endregion
    }
}