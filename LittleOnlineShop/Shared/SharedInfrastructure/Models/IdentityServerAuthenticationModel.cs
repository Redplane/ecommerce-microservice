﻿using IdentityServer4.AccessTokenValidation;

namespace SharedInfrastructure.Models
{
    public class IdentityServerAuthenticationModel
    {
        #region Properties

        public string Authority { get; set; }

        public string ApiSecret { get; set; }

        public string ApiName { get; set; }

        public bool RequireHttpsMetaData { get; set; }

        public bool SaveToken { get; set; }

        public SupportedTokens SupportedToken { get; set; }

        #endregion
    }
}