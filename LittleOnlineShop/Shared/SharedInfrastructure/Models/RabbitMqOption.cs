﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SharedInfrastructure.Models
{
    public class RabbitMqOption
    {
        #region Properties

        public string Endpoint { get; set; }
        
        public List<RabbitMqChannelOption> Channels { get; set; }

        #endregion

        #region Constructor

        #endregion
        
        #region Methods

        /// <summary>
        /// Find channel options from settings.
        /// </summary>
        /// <param name="channelName"></param>
        public virtual RabbitMqChannelOption FindChannelOption(string channelName)
        {
            return Channels
                ?.FirstOrDefault(channel => channel.Name.Equals(channelName, StringComparison.InvariantCultureIgnoreCase));
        }
        
        #endregion
    }
}