﻿using SharedInfrastructure.Enums;
using SharedInfrastructure.Interfaces;

namespace SharedInfrastructure.Models
{
    public class RabbitMqChannelOption : IMqChannel
    {
        #region Properties

        /// <summary>
        /// Name of channel.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Whether channel should be durable or not.
        /// </summary>
        public bool Durable { get; set; }

        /// <summary>
        /// Whether channel should be exclusive or not.
        /// </summary>
        public bool Exclusive { get; set; }

        /// <summary>
        /// Should channel should be deleted automatically or not.
        /// </summary>
        public bool AutoDelete { get; set; }
        
        /// <summary>
        /// Kind of message channel.
        /// </summary>
        public MqChannelKinds Kind { get; set; }
        
        #endregion
    }
}