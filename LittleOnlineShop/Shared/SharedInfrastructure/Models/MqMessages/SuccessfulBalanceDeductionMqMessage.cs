using System;
using SharedInfrastructure.Constants;

namespace SharedInfrastructure.Models.MqMessages
{
    public class SuccessfulBalanceDeductionMqMessage : MqMessageBase
    {
        #region Properties
        
        /// <summary>
        /// Product basket to be reserved.
        /// </summary>
        public Guid ProductBasketId { get; }
        
        /// <summary>
        /// Id of customer whose balance has been deducted.
        /// </summary>
        public Guid CustomerId { get; }

        #endregion
        
        #region Constructor
        
        public SuccessfulBalanceDeductionMqMessage(Guid productBasketId, Guid customerId) : base(MqEventNameConstant.SuccessfulBalanceDeduction)
        {
            ProductBasketId = productBasketId;
            CustomerId = customerId;
        }
        
        #endregion
    }
}