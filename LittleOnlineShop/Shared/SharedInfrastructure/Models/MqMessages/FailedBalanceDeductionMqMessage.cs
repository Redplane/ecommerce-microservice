using System;
using SharedInfrastructure.Constants;

namespace SharedInfrastructure.Models.MqMessages
{
    public class FailedBalanceDeductionMqMessage : MqMessageBase
    {
        #region Properties
        
        public Guid ProductBasketId { get; }
        
        public Guid CustomerId { get; }
        
        #endregion
        
        #region Constructor
        
        public FailedBalanceDeductionMqMessage(Guid productBasketId, Guid customerId) : base(MqEventNameConstant.FailedBalanceDeduction)
        {
            ProductBasketId = productBasketId;
            CustomerId = customerId;
        }
        
        #endregion
    }
}