using System;
using SharedInfrastructure.Constants;

namespace SharedInfrastructure.Models.MqMessages
{
    public class ProductBasketCheckedOutMqMessage : MqMessageBase
    {
        #region Properties
        
        public Guid CustomerId { get; }
        
        public Guid ProductBasketId { get; }
        
        public decimal TotalMoney { get; }

        #endregion
        
        #region Constructor
        
        public ProductBasketCheckedOutMqMessage(Guid customerId, Guid productBasketId, decimal totalMoney) : base(MqEventNameConstant.ProductBasketCheckOut)
        {
            CustomerId = customerId;
            ProductBasketId = productBasketId;
            TotalMoney = totalMoney;
        }
        
        #endregion
    }
}