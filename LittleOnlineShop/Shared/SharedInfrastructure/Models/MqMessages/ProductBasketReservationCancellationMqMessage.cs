using System;
using SharedInfrastructure.Constants;

namespace SharedInfrastructure.Models.MqMessages
{
    public class ProductBasketReservationCancellationMqMessage : MqMessageBase
    {
        #region Properties
        
        public Guid ProductBasketId { get; }
        
        public Guid CustomerId { get; }
        
        public decimal TotalMoney { get; }
        
        #endregion
        
        #region Constructor

        public ProductBasketReservationCancellationMqMessage(Guid productBasketId, Guid customerId, decimal totalMoney) : base(MqEventNameConstant.ProductBasketReservationCancellation)
        {
            ProductBasketId = productBasketId;
            CustomerId = customerId;
            TotalMoney = totalMoney;
        }

        #endregion
    }
}