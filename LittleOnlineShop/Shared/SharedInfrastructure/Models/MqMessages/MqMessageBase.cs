using Newtonsoft.Json;

namespace SharedInfrastructure.Models.MqMessages
{
    public class MqMessageBase
    {
        #region Properties
        
        /// <summary>
        /// Event name bond to the message.
        /// </summary>
        public string EventName { get; }
        
        #endregion
        
        #region Constructor

        public MqMessageBase(string eventName)
        {
            EventName = eventName;
        }

        #endregion
        
        #region Methods

        /// <summary>
        /// Serialize the message to json string.
        /// </summary>
        /// <returns></returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }
        
        #endregion
    }
}