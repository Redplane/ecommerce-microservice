using System;
using SharedInfrastructure.Constants;

namespace SharedInfrastructure.Models.MqMessages
{
    public class ProductReservedMqMessage : MqMessageBase
    {
        #region Properties

        /// <summary>
        /// Id of product basket that has been reserved.
        /// </summary>
        public Guid ProductBasketId { get; }
        
        /// <summary>
        /// Total money all products in basket cost.
        /// </summary>
        public decimal TotalMoney { get; }
        
        #endregion
        
        #region Constructor

        public ProductReservedMqMessage(Guid productBasketId, decimal totalMoney) : base(MqEventNameConstant.ProductBasketReservation)
        {
            ProductBasketId = productBasketId;
            TotalMoney = totalMoney;
        }

        #endregion
    }
}