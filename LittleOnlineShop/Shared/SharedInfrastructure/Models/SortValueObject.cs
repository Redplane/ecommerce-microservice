﻿using System.Collections.Generic;
using SharedDomain.Models;
using SharedInfrastructure.Enums;

namespace SharedInfrastructure.Models
{
    public class SortValueObject<T> : ValueObject
    {
        #region Constructors

        public SortValueObject(T property, SortDirections directions)
        {
            Property = property;
            Directions = directions;
        }

        #endregion

        #region Methods

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Property;
            yield return Directions;
        }

        #endregion

        #region Properties

        // ReSharper disable once UnusedAutoPropertyAccessor.Local
        public T Property { get; }

        // ReSharper disable once UnusedAutoPropertyAccessor.Local
        public SortDirections Directions { get; }

        #endregion
    }
}