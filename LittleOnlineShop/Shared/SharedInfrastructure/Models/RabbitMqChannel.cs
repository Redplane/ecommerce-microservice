using RabbitMQ.Client;

namespace SharedInfrastructure.Models
{
    public class RabbitMqChannel : RabbitMqChannelOption
    {
        #region Properties

        /// <summary>
        /// Channel which has been established between client & rabbit mq queue.
        /// </summary>
        public IModel Channel { get;}
        
        #endregion
        
        #region Constructor

        public RabbitMqChannel(IModel channel, RabbitMqChannelOption option)
        {
            Name = option.Name;
            Durable = option.Durable;
            Exclusive = option.Exclusive;
            AutoDelete = option.AutoDelete;
            Kind = option.Kind;
            Channel = channel;
        }

        #endregion
    }
}