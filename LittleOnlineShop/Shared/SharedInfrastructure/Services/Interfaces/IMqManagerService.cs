using System.Threading;
using System.Threading.Tasks;
using SharedInfrastructure.Interfaces;
using SharedInfrastructure.Models.MqMessages;

namespace SharedInfrastructure.Services.Interfaces
{
    public interface IMqManagerService
    {
        #region Methods

        /// <summary>
        /// Publish mq message to external message queue service asynchronously.
        /// </summary>
        /// <param name="channelName"></param>
        /// <param name="mqMessage"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task PublishMqMessageAsync(string channelName, MqMessageBase mqMessage,
            CancellationToken cancellationToken = default);

        /// <summary>
        /// Add an external message queue channel into manager service asynchronously.
        /// </summary>
        /// <param name="mqChannel"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task AddMqChannelAsync(IMqChannel mqChannel, CancellationToken cancellationToken = default);

        /// <summary>
        /// Find message queue channel using channel name.
        /// </summary>
        /// <param name="channelName"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<IMqChannel[]> FindMqChannelsAsync(string channelName, CancellationToken cancellationToken = default);

        #endregion
    }
}