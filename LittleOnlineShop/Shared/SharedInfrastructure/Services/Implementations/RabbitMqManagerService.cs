using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RabbitMQ.Client;
using SharedInfrastructure.Interfaces;
using SharedInfrastructure.Models;
using SharedInfrastructure.Models.MqMessages;

namespace SharedInfrastructure.Services.Implementations
{
    public class RabbitMqManagerService : MqManagerService
    {
        #region Constructor

        public RabbitMqManagerService()
        {
        }

        public RabbitMqManagerService(IList<IMqChannel> channels) : base(channels)
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        /// <param name="mqChannel"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        protected override Task PublishMqMessageThroughMqChannelAsync(IMqChannel mqChannel, MqMessageBase message)
        {
            // Channel is invalid.
            if (mqChannel == null)
                return Task.CompletedTask;

            // Channel is not a rabbit mq channel.
            if (!(mqChannel is RabbitMqChannel rabbitMqChannel))
                return Task.CompletedTask;

            // Channel model is invalid.
            var channelModel = rabbitMqChannel.Channel;
            if (channelModel == null)
                return Task.CompletedTask;

            var serializedMessage = Encoding.UTF8.GetBytes(message.ToJson());
            channelModel.BasicPublish("", mqChannel.Name, null, serializedMessage);

            return Task.CompletedTask;
        }

        #endregion
    }
}