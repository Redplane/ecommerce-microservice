using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using SharedInfrastructure.Interfaces;
using SharedInfrastructure.Models.MqMessages;
using SharedInfrastructure.Services.Interfaces;

namespace SharedInfrastructure.Services.Implementations
{
    public abstract class MqManagerService : IMqManagerService
    {
        #region Properties

        /// <summary>
        /// Mapping between channel name and message queue options.
        /// </summary>
        private readonly LinkedList<IMqChannel> _addedChannels;
        
        #endregion
        
        #region Constructor

        protected MqManagerService()
        {
            _addedChannels = new LinkedList<IMqChannel>();
        }

        protected MqManagerService(IList<IMqChannel> channels) : this()
        {
            if (channels == null)
                throw new ArgumentException($"{nameof(channels)} cannot be null.");

            _addedChannels = new LinkedList<IMqChannel>(channels);
        }

        #endregion
        
        #region Methods
        
        /// <summary>
        /// <inheritdoc />
        /// </summary>
        /// <param name="channelName"></param>
        /// <param name="message"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public virtual async Task PublishMqMessageAsync(string channelName, MqMessageBase message,
            CancellationToken cancellationToken = default)
        {
            // Find the channel using specific name.
            var mqChannels = await FindMqChannelsAsync(channelName, cancellationToken);
            if (mqChannels == null)
                throw new Exception($"Channel {channelName} is not found.");

            // List of tasks to be completed.
            var messagePublishTasks = new LinkedList<Task>();
            
            // Go through every channels have been found to send message through them.
            foreach (var mqChannel in mqChannels)
            {
                var messagePublishTask = PublishMqMessageThroughMqChannelAsync(mqChannel, message);
                messagePublishTasks.AddLast(messagePublishTask);
            }

            await Task.WhenAll(messagePublishTasks.ToArray());
        }

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        /// <param name="mqChannel"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public virtual Task AddMqChannelAsync(IMqChannel mqChannel, CancellationToken cancellationToken = default)
        {
            // Prevent another thread from accessing the variable.
            lock (_addedChannels)
            {
                // Check whether channel has already been added before.
                var channel = _addedChannels.FirstOrDefault(x => x.Name.Equals(mqChannel.Name, StringComparison.InvariantCultureIgnoreCase));
                
                // Remove the existing channel.
                if (channel != null)
                    _addedChannels.Remove(channel);

                _addedChannels.AddLast(mqChannel);
            }

            return Task.CompletedTask;
        }

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        /// <param name="channelName"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public virtual Task<IMqChannel[]> FindMqChannelsAsync(string channelName, CancellationToken cancellationToken = default)
        {
            // Find the channels whose name match with the one in search condition.
            // ReSharper disable once InconsistentlySynchronizedField
            var channels = _addedChannels
                .Where(x => x.Name.Equals(channelName, StringComparison.InvariantCultureIgnoreCase));
            return Task.FromResult(channels.ToArray());
        }

        /// <summary>
        /// Publish mq message through a specific message queue channel.
        /// By default, this method is not implemented because there is nothing to decide which kind of message queue to be used.
        /// </summary>
        /// <param name="mqChannel"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        protected abstract Task PublishMqMessageThroughMqChannelAsync(IMqChannel mqChannel, MqMessageBase message);

        #endregion
    }
}