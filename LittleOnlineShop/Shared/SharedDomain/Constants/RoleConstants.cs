﻿namespace SharedDomain.Constants
{
    public class RoleConstants
    {
        #region Properties

        public const string Admin = "Admin";

        public const string Member = "Member";

        public static string[] ValidRoles => new[] {Admin, Member};

        #endregion
    }
}