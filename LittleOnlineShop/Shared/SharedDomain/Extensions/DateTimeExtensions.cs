﻿using System;
using System.Globalization;

namespace SharedDomain.Extensions
{
    public static class DateTimeExtensions
    {
        #region Properties

        // ReSharper disable once InconsistentNaming
        private static readonly DateTime _utcDateTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        #endregion

        /// <summary>
        ///     Calculate the unix time from UTC DateTime.
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static double ToUnixTime(this DateTime dateTime)
        {
            return (dateTime - _utcDateTime).TotalMilliseconds;
        }

        /// <summary>
        ///     Calculate date time to unix time.
        /// </summary>
        /// <param name="dateTime"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public static double? ToUnixTime(this string dateTime, string format)
        {
            if (!DateTime.TryParseExact(dateTime, format, null, DateTimeStyles.None,
                out var birthdate))
                return null;


            return ToUnixTime(birthdate);
        }

        /// <summary>
        ///     Convert unix time to UTC time.
        /// </summary>
        /// <param name="unixTime"></param>
        /// <returns></returns>
        public static DateTime ToUnixDateTime(this double unixTime)
        {
            return _utcDateTime.AddMilliseconds(unixTime);
        }
    }
}