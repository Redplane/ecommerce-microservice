﻿namespace SharedDomain.Enums
{
    public enum MasterItemAvailabilities
    {
        Unavailable,
        Available
    }
}