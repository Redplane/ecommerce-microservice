﻿using System;
using SharedDomain.Enums;

namespace SharedDomain.Interfaces
{
    public interface IEntity
    {
        #region Properties

        /// <summary>
        ///     Id of entity.
        /// </summary>
        Guid Id { get; }

        /// <summary>
        ///     Entity availability.
        /// </summary>
        MasterItemAvailabilities Availability { get; set; }

        /// <summary>
        ///     Time when entity was created.
        /// </summary>
        double CreatedTime { get; set; }

        /// <summary>
        ///     Time when entity was lastly modified.
        /// </summary>
        double? LastModifiedTime { get; set; }

        #endregion
    }
}