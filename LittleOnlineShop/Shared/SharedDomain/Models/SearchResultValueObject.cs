﻿using System.Collections.Generic;
using System.Linq;

namespace SharedDomain.Models
{
    public class SearchResultValueObject<T> : ValueObject
    {
        #region Constructors

        public SearchResultValueObject(IList<T> records, long totalRecords)
        {
            Records = records;
            TotalRecords = totalRecords;
        }

        #endregion

        #region Properties

        public IList<T> Records { get; }

        public long TotalRecords { get; }

        #endregion

        #region Methods

        public T FirstOrDefault()
        {
            if (Records == null)
                return default;

            return Records.FirstOrDefault();
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Records;
            yield return TotalRecords;
        }

        #endregion
    }
}