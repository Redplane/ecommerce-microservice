﻿using System.Collections.Generic;

namespace SharedDomain.Models
{
    public class ImageSizeValueObject : ValueObject
    {
        #region Constructor

        public ImageSizeValueObject(int width, int height)
        {
            Height = height;
            Width = width;
        }

        #endregion

        #region Methods

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Height;
            yield return Width;
        }

        #endregion

        #region Properties

        // ReSharper disable once AutoPropertyCanBeMadeGetOnly.Local
        public int Height { get; private set; }

        // ReSharper disable once AutoPropertyCanBeMadeGetOnly.Local
        public int Width { get; private set; }

        #endregion
    }
}