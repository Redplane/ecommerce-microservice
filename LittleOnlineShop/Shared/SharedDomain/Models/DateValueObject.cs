﻿using System;
using System.Collections.Generic;

namespace SharedDomain.Models
{
    public class DateValueObject : ValueObject
    {
        #region Constructor

        public DateValueObject(int year, int month, int day)
        {
            if (year < 1)
                year = 1;

            if (!IsValidMonth(month))
                month = DefaultMonth;

            if (!IsValidDay(year, month, day))
                day = DefaultDay;

            Year = year;
            Month = month;
            Day = day;
        }

        #endregion

        #region Properties

        // ReSharper disable once AutoPropertyCanBeMadeGetOnly.Local
        public int Year { get; private set; }

        // ReSharper disable once AutoPropertyCanBeMadeGetOnly.Local
        public int Month { get; private set; }

        // ReSharper disable once AutoPropertyCanBeMadeGetOnly.Local
        public int Day { get; private set; }

        private const int DefaultDay = 1;

        private const int DefaultMonth = 1;

        private const int DefaultYear = 1;

        #endregion

        #region Methods

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Year;
            yield return Month;
            yield return Day;
        }

        /// <summary>
        ///     Whether month is valid or not.
        /// </summary>
        /// <param name="month"></param>
        /// <returns></returns>
        protected bool IsValidMonth(int month)
        {
            return 1 <= month && month <= 12;
        }

        /// <summary>
        ///     Whether day is valid or not.
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <param name="day"></param>
        /// <returns></returns>
        protected bool IsValidDay(int? year, int? month, int day)
        {
            if (day < 1)
                return false;

            if (month == null)
                return 1 <= day && day <= 31;

            switch (month.Value)
            {
                case 1:
                case 3:
                case 5:
                case 7:
                case 8:
                case 10:
                case 12:
                    return day <= 31;

                case 2:

                    // No year is defined.
                    if (year == null)
                        return day <= 29;

                    // Leap year.
                    if (year % 4 == 0)
                        return day <= 29;

                    return day <= 28;

                default:
                    return day <= 30;
            }
        }

        /// <summary>
        ///     Serialize object to string.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return new DateTime(Year, Month, Day).ToString("yyyy-MM-dd");
        }

        #endregion

        #region Operators overriden

        public static implicit operator DateTime(DateValueObject date)
        {
            if (date == null)
                return DateTime.Now.ToLocalTime();

            return new DateTime(date.Year, date.Month, date.Day);
        }

        public static implicit operator DateValueObject(DateTime dateTime)
        {
            return new DateValueObject(dateTime.Year, dateTime.Month, dateTime.Day);
        }

        public static implicit operator DateValueObject(string value)
        {
            if (!DateTime.TryParse(value, out var dateTime))
                return null;

            return new DateValueObject(dateTime.Year, dateTime.Month, dateTime.Day);
        }

        #endregion
    }
}