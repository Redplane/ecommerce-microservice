﻿using System.Collections.Generic;

namespace SharedDomain.Models
{
    public class PagerValueObject : ValueObject
    {
        #region Constructor

        public PagerValueObject(int page, int records)
        {
            Page = page;
            Records = records;

            if (page < 1)
                Page = 1;

            if (records < 0)
                Records = 0;
        }

        #endregion

        #region Properties

        public int Page { get; }

        public int Records { get; }

        #endregion

        #region Methods

        public int GetSkippedRecords()
        {
            var pIndex = Page - 1;
            if (pIndex < 0)
                pIndex = 0;

            return pIndex * Records;
        }

        /// <summary>
        ///     Whether item should be queried from source or not.
        /// </summary>
        /// <returns></returns>
        public bool ShouldItemQueried()
        {
            if (Records < 1)
                return false;

            return true;
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Page;
            yield return Records;
        }

        #endregion
    }
}