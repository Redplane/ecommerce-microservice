﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Filters;
using SharedDomain.Constants;
using SharedMicroService.AuthenticationHandlers.Requirements;
using SharedMicroService.Extensions;

namespace SharedMicroService.AuthenticationHandlers.Implementations
{
    public class RoleRequirementHandler : AuthorizationHandler<RoleRequirement>
    {
        #region Methods

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, RoleRequirement requirement)
        {
            // Convert authorization filter context into authorization filter context.
            var authorizationFilterContext = (AuthorizationFilterContext) context.Resource;

            // Get http context.
            var httpContext = authorizationFilterContext.HttpContext;

            // Decode access token.
            var claims = httpContext.User.Claims.ToList();
            if (!claims.Any())
            {
                context.MarkRequirementAsFailed(requirement);
                return Task.CompletedTask;
            }

            // Id is not in claims.
            var roleClaim = claims.FirstOrDefault(x => x.Type == JwtClaimTypeConstants.Role);
            if (roleClaim == null || string.IsNullOrEmpty(roleClaim.Value))
            {
                context.MarkRequirementAsFailed(requirement);
                return Task.CompletedTask;
            }

            // No role matched.
            if (!requirement.Roles.Any(
                role => role.Equals(roleClaim.Value, StringComparison.InvariantCultureIgnoreCase)))
            {
                context.MarkRequirementAsFailed(requirement);
                return Task.CompletedTask;
            }

            context.Succeed(requirement);
            return Task.CompletedTask;
        }

        #endregion

        #region Constructor

        #endregion
    }
}