﻿using Microsoft.AspNetCore.Authorization;

namespace SharedMicroService.AuthenticationHandlers.Requirements
{
    public class RoleRequirement : IAuthorizationRequirement
    {
        #region Accessors

        /// <summary>
        ///     List of roles that are allowed to access this system.
        /// </summary>
        public string[] Roles { get; }

        #endregion

        #region Properties

        #endregion

        #region Constructor

        public RoleRequirement(string[] roles)
        {
            Roles = roles;
        }

        public RoleRequirement(string role)
        {
            Roles = new[] {role};
        }

        #endregion
    }
}