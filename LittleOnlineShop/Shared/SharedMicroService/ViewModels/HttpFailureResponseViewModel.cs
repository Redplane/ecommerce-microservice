﻿using System.Collections.Generic;

namespace SharedMicroService.ViewModels
{
    public class HttpFailureResponseViewModel
    {
        #region Constructor

        public HttpFailureResponseViewModel(string message)
        {
            Message = message;
        }

        #endregion

        #region Properties

        public string Message { get; set; }

        public Dictionary<string, object> AdditionalData { get; set; }

        #endregion
    }
}