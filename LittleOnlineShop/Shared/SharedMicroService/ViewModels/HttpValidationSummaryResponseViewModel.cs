﻿namespace SharedMicroService.ViewModels
{
    public class HttpValidationSummaryResponseViewModel
    {
        #region Constructors

        public HttpValidationSummaryResponseViewModel(string[] messages)
        {
            Messages = messages;
        }

        #endregion

        #region Properties

        public string[] Messages { get; }

        #endregion
    }
}