﻿namespace SharedMicroService.Constants
{
    public class PolicyConstants
    {
        #region Properties

        /// <summary>
        ///     User must be admin.
        /// </summary>
        public const string IsAdmin = nameof(IsAdmin);

        /// <summary>
        ///     User must be member.
        /// </summary>
        public const string IsMember = nameof(IsMember);

        #endregion
    }
}