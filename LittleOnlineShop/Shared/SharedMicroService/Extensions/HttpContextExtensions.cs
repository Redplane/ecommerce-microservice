﻿using System;
using System.Net;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using SharedInfrastructure.Constants;
using SharedInfrastructure.Models;

namespace SharedMicroService.Extensions
{
    public static class HttpContextExtensions
    {
        #region Methods

        /// <summary>
        ///     Find user profile that attached to HttpContext.
        /// </summary>
        /// <returns></returns>
        public static UserCredential FindProfile(this HttpContext httpContext, bool throwException = false)
        {
            if (httpContext == null)
            {
                if (throwException)
                    throw new UnauthorizedAccessException(SharedHttpMessageCodeConstants.InvalidGrant);

                return null;
            }

            var items = httpContext.Items;

            var info = items?[ClaimTypes.UserData];
            if (info == null || !(info is UserCredential userCredential))
            {
                if (throwException)
                    throw new UnauthorizedAccessException(SharedHttpMessageCodeConstants.InvalidGrant);

                return null;
            }


            return userCredential;
        }

        /// <summary>
        ///     Set profile to HttpContext.
        /// </summary>
        /// <param name="httpContext"></param>
        /// <param name="userCredential"></param>
        public static void SetProfile(this HttpContext httpContext, UserCredential userCredential)
        {
            httpContext.Items[ClaimTypes.UserData] = userCredential;
        }

        /// <summary>
        ///     Find client IP Address.
        /// </summary>
        /// <param name="httpContext"></param>
        /// <returns></returns>
        // ReSharper disable once InconsistentNaming
        public static string FindClientIPAddress(this HttpContext httpContext)
        {
            var httpRequest = httpContext.Request;
            if (httpRequest == null)
                return null;

            var headers = httpRequest.Headers;
            var headerNamesList = new[]
            {
                "HTTP_CLIENT_IP", "HTTP_X_FORWARDED_FOR", "HTTP_X_FORWARDED", "HTTP_FORWARDED_FOR", "HTTP_FORWARDED",
                "REMOTE_ADDR"
            };

            string ipAddress = null;

            foreach (var headerName in headerNamesList)
            {
                if (!headers.ContainsKey(headerName))
                    continue;

                ipAddress = headers[headerName];
                if (string.IsNullOrWhiteSpace(ipAddress) || !IPAddress.TryParse(ipAddress, out var parsedIpAddress))
                    ipAddress = null;
            }

            if (ipAddress == null)
                return "UNKNOWN";

            return ipAddress;
        }

        #endregion
    }
}