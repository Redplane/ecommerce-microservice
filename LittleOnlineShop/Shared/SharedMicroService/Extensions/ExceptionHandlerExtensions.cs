﻿using System.Linq;
using System.Net;
using FluentValidation;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using SharedInfrastructure.Constants;
using SharedInfrastructure.Exceptions;
using SharedMicroService.ViewModels;

namespace SharedMicroService.Extensions
{
    public static class ExceptionHandlerExtension
    {
        public static void UseApiExceptionHandler(this IApplicationBuilder app)
        {
            // Use exception handler for errors handling.
            app.UseExceptionHandler(options =>
            {
                options.Run(
                    async context =>
                    {
                        // Get logger.

                        // Mark the response status as Internal server error.
                        context.Response.StatusCode = (int) HttpStatusCode.InternalServerError;
                        context.Response.ContentType = "application/json";
                        var exceptionHandlerFeature = context.Features.Get<IExceptionHandlerFeature>();

                        // No exception handler feature has been found.
                        if (exceptionHandlerFeature == null || exceptionHandlerFeature.Error == null)
                            return;

                        // Initialize response asynchronously.
                        var contractResolver = new DefaultContractResolver();
                        contractResolver.NamingStrategy = new CamelCaseNamingStrategy
                        {
                            ProcessDictionaryKeys = true
                        };

                        var jsonSerializerSettings = new JsonSerializerSettings();
                        jsonSerializerSettings.ContractResolver = contractResolver;
                        jsonSerializerSettings.DefaultValueHandling = DefaultValueHandling.Ignore;

                        // Get the thrown exception.
                        var exception = exceptionHandlerFeature.Error;

                        // Initialize api response.
                        string szApiResponse;

                        // Http response exception.
                        if (exception is HttpResponseException httpResponseException)
                        {
                            var httpFailureResponse = new HttpFailureResponseViewModel(httpResponseException.Message);
                            httpFailureResponse.AdditionalData = httpResponseException.AdditionalData;
                            szApiResponse = JsonConvert.SerializeObject(httpFailureResponse, jsonSerializerSettings);
                            context.Response.StatusCode = (int) httpResponseException.StatusCode;
                        }
                        else if (exception is ValidationException validationException)
                        {
                            var messages = validationException.Errors.Select(x => x.ErrorMessage).ToArray();
                            var httpValidationSummaryResponse = new HttpValidationSummaryResponseViewModel(messages);
                            szApiResponse =
                                JsonConvert.SerializeObject(httpValidationSummaryResponse, jsonSerializerSettings);
                            context.Response.StatusCode = (int) HttpStatusCode.BadRequest;
                        }
                        else
                        {
                            var httpFailureResponse =
                                new HttpFailureResponseViewModel(SharedHttpMessageCodeConstants.InternalServerError);
                            szApiResponse = JsonConvert.SerializeObject(httpFailureResponse, jsonSerializerSettings);
                            context.Response.StatusCode = (int) HttpStatusCode.InternalServerError;
                        }

                        await context.Response.WriteAsync(szApiResponse).ConfigureAwait(false);
                    });
            });
        }
    }
}