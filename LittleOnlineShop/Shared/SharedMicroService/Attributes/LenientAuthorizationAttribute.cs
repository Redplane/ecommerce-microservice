﻿using System;
using Microsoft.AspNetCore.Mvc.Filters;

namespace SharedMicroService.Attributes
{
    public class LenientAuthorizationAttribute : Attribute, IFilterMetadata
    {
    }
}