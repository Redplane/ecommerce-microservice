﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace SharedMicroService.Services.Interfaces
{
    public interface IPhotoService
    {
        #region Methods

        /// <summary>
        ///     Upload photo asynchronously.
        /// </summary>
        /// <param name="bytes"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<string> UploadAsync(byte[] bytes, CancellationToken cancellationToken = default);

        /// <summary>
        ///     Upload photos asynchronously.
        /// </summary>
        /// <returns></returns>
        Task<string[]> UploadAsync(IFormFile[] bytes, CancellationToken cancellationToken = default);

        #endregion
    }
}