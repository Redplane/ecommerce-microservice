﻿using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Net.Http.Headers;

namespace ApiGateway.Handlers
{
    public class BaseHttpMessageHandler : DelegatingHandler
    {
        #region Properties

        private readonly IHttpContextAccessor _httpContextAccessor;

        #endregion

        #region Constructor

        public BaseHttpMessageHandler(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        #endregion

        #region Methods

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        /// <param name="httpRequestMessage"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage httpRequestMessage, CancellationToken cancellationToken)
        {
            // Get authorization from incoming request.
            var httpContext = _httpContextAccessor.HttpContext;
            if (httpContext == null)
                return base.SendAsync(httpRequestMessage, cancellationToken);

            var httpRequest = httpContext.Request;
            if (httpRequest == null)
                return base.SendAsync(httpRequestMessage, cancellationToken);

            // Get authorization header.
            var authorizationHeaders = httpRequest.Headers[HeaderNames.Authorization];
            if ( authorizationHeaders.Count < 1)
                return null;

            var authorizationHeader = authorizationHeaders.FirstOrDefault();
            return base.SendAsync(httpRequestMessage, cancellationToken);
        }

        #endregion
    }
}