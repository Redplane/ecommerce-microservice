﻿using System.Threading.Tasks;
using ApiGateway.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace ApiGateway.Controllers
{
    [Route("api/order")]
    [ApiController]
    public class OrderGatewayController : ControllerBase
    {
        #region Properties

        private readonly IOrderClientService _orderClientService;

        #endregion

        #region Constructor

        public OrderGatewayController(IOrderClientService orderClientService)
        {
            _orderClientService = orderClientService;
        }

        #endregion

        #region Methods

        [HttpPost("load-orders")]
        public async Task<IActionResult> LoadOrdersAsync()
        {
            var orders = await _orderClientService
                .LoadOrdersAsync();

            return Ok(orders);
        }

        #endregion
    }
}