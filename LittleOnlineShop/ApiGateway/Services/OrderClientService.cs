﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using ApiGateway.Extensions;
using ApiGateway.Interfaces;
using ApiGateway.ViewModels;

namespace ApiGateway.Services
{
    public class OrderClientService : IOrderClientService
    {
        #region Methods

        private readonly HttpClient _httpClient;

        #endregion

        #region Constructor

        public OrderClientService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        #endregion

        #region Methods

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        /// <returns></returns>
        public virtual async Task<List<OrderViewModel>> LoadOrdersAsync()
        {
            var loadOrdersResponse = await _httpClient
                .GetAsync("https://api.jsonbin.io/b/5c4928be04ce8017ee29d526");

            if (loadOrdersResponse == null)
                return null;

            var httpContent = loadOrdersResponse.Content;
            if (httpContent == null)
                return null;

            return await httpContent.ReadAsAsync<List<OrderViewModel>>();
        }

        #endregion


    }
}