﻿using ApiGateway.Aggregators;
using ApiGateway.Handlers;
using ApiGateway.Interfaces;
using ApiGateway.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Ocelot.DependencyInjection;
using Ocelot.Middleware;

namespace ApiGateway
{
    public class Startup
    {
        #region Properties

        public IConfiguration Configuration { get; }
        
        #endregion

        #region Constructor

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        #endregion

        #region Methods

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<BaseHttpMessageHandler>();
            services
                .AddHttpContextAccessor();

            services
                .AddHttpClient<IOrderClientService, OrderClientService>()
                .AddHttpMessageHandler<BaseHttpMessageHandler>();

            // Add ocelot to pipeline.
            services.AddOcelot()
                .AddTransientDefinedAggregator<LoadCustomerOrdersAggregator>(); ;

            services
                .AddControllers()
                .SetCompatibilityVersion(CompatibilityVersion.Version_3_0);
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();
            else
                app.UseHsts();

            app.UseHttpsRedirection();

            // Use routing middleware.
            app.UseRouting();

            app
                .UseEndpoints(endpointBuilder => endpointBuilder.MapControllers());

            // Use ocelot.
            app.UseOcelot()
                .Wait();
        }

        #endregion
    }
}