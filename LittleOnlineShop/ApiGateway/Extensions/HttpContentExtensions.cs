using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ApiGateway.Extensions
{
    public static class HttpContentExtensions
    {
        #region Methods

        public static async Task<T> ReadAsAsync<T>(this HttpContent httpContent)
        {
            if (httpContent == null)
                return default;

            var content = await httpContent.ReadAsStringAsync();
            if (string.IsNullOrWhiteSpace(content))
                return default;

            return JsonConvert.DeserializeObject<T>(content);
        }
        
        #endregion
    }
}