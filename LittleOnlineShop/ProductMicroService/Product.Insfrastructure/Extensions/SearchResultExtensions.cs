﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using SharedDomain.Models;

namespace ProductInfrastructure.Extensions
{
    public static class SearchResultExtensions
    {
        /// <summary>
        ///     Search result using specific conditions.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="pager"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public static async Task<SearchResultValueObject<T>> ToSearchResultAsync<T>(this IMongoQueryable<T> source,
            PagerValueObject pager, CancellationToken cancellationToken = default)
        {
            // Count total records.
            var totalRecords = await source.CountAsync(cancellationToken);

            var items = new List<T>();

            // Do pagination with one extra item.
            if (pager != null)
            {
                if (pager.ShouldItemQueried())
                    items = await source.Skip(pager.GetSkippedRecords())
                        .Take(pager.Records + 1)
                        .ToListAsync(cancellationToken);

                return new SearchResultValueObject<T>(items.Take(pager.Records).ToArray(), totalRecords);
            }

            items = await source.ToListAsync(cancellationToken);
            return new SearchResultValueObject<T>(items.ToArray(), totalRecords);
        }

        /// <summary>
        ///     Find and build projected search result from database.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TProjection"></typeparam>
        /// <param name="source"></param>
        /// <param name="pager"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public static async Task<SearchResultValueObject<TProjection>> ToSearchResultAsync<T, TProjection>(
            this IFindFluent<T, TProjection> source,
            PagerValueObject pager, CancellationToken cancellationToken = default)
        {
            // Count total records.
            var totalRecords = await source.CountDocumentsAsync(cancellationToken);

            var items = new List<TProjection>();

            // Do pagination with one extra item.
            if (pager != null)
            {
                if (pager.ShouldItemQueried())
                    items = await source.Skip(pager.GetSkippedRecords())
                        .Limit(pager.Records + 1)
                        .ToListAsync(cancellationToken);


                // Initialize pager result.
                return new SearchResultValueObject<TProjection>(items.Take(pager.Records).ToArray(), totalRecords);
            }

            items = await source.ToListAsync(cancellationToken);
            return new SearchResultValueObject<TProjection>(items.ToArray(), totalRecords);
        }
    }
}