﻿using System;
using System.Linq.Expressions;
using MongoDB.Driver;
using ProductInfrastructure.Enums;
using ProductInfrastructure.Models;

namespace ProductInfrastructure.Extensions
{
    public static class NumericSearchExtensions
    {
        /// <summary>
        ///     Search for text base on search mode.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="builder"></param>
        /// <param name="field"></param>
        /// <param name="comparision"></param>
        /// <returns></returns>
        public static FilterDefinition<T> SearchNumeric<T>(this FilterDefinitionBuilder<T> builder,
            Expression<Func<T, object>> field, NumericRangeComparisionModel<double> comparision)
        {
            var filterDefinition = FilterDefinition<T>.Empty;
            filterDefinition &= SearchNumeric(builder, field, comparision.Min);
            filterDefinition &= SearchNumeric(builder, field, comparision.Max);

            return filterDefinition;
        }


        /// <summary>
        ///     Search a record by comparing its property value to a specific value.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="builder"></param>
        /// <param name="field"></param>
        /// <param name="numericComparision"></param>
        /// <returns></returns>
        public static FilterDefinition<T> SearchNumeric<T>(this FilterDefinitionBuilder<T> builder,
            Expression<Func<T, object>> field, NumericComparisionModel<double> numericComparision)
        {
            if (numericComparision == null)
                return FilterDefinition<T>.Empty;

            switch (numericComparision.Mode)
            {
                case NumericComparisionModes.LessThan:
                    return builder.Lt(field, numericComparision.Value);

                case NumericComparisionModes.LessThanOrEqualTo:
                    return builder.Lte(field, numericComparision.Value);

                case NumericComparisionModes.EqualTo:
                    return builder.Eq(field, numericComparision.Value);

                case NumericComparisionModes.GreaterThanOrEqualTo:
                    return builder.Gte(field, numericComparision.Value);

                case NumericComparisionModes.GreaterThan:
                    return builder.Gt(field, numericComparision.Value);
            }

            return FilterDefinition<T>.Empty;
        }

        /// <summary>
        ///     Search a record by comparing its property value to a specific value.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="builder"></param>
        /// <param name="field"></param>
        /// <param name="numericComparision"></param>
        /// <returns></returns>
        public static FilterDefinition<T> SearchNumeric<T>(this FilterDefinitionBuilder<T> builder,
            FieldDefinition<T, double> field, NumericComparisionModel<double> numericComparision)
        {
            if (numericComparision == null)
                return FilterDefinition<T>.Empty;

            switch (numericComparision.Mode)
            {
                case NumericComparisionModes.LessThan:
                    return builder.Lt(field, numericComparision.Value);

                case NumericComparisionModes.LessThanOrEqualTo:
                    return builder.Lte(field, numericComparision.Value);

                case NumericComparisionModes.EqualTo:
                    return builder.Eq(field, numericComparision.Value);

                case NumericComparisionModes.GreaterThanOrEqualTo:
                    return builder.Gte(field, numericComparision.Value);

                case NumericComparisionModes.GreaterThan:
                    return builder.Gt(field, numericComparision.Value);
            }

            return FilterDefinition<T>.Empty;
        }
    }
}