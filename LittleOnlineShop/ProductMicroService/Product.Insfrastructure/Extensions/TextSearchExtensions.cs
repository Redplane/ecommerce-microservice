﻿using System;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using MongoDB.Bson;
using MongoDB.Driver;
using ProductInfrastructure.Enums;
using ProductInfrastructure.Models;

namespace ProductInfrastructure.Extensions
{
    public static class TextSearchExtensions
    {
        /// <summary>
        ///     Search for text base on search mode.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="builder"></param>
        /// <param name="field"></param>
        /// <param name="textSearch"></param>
        /// <returns></returns>
        public static FilterDefinition<T> SearchText<T>(this FilterDefinitionBuilder<T> builder,
            Expression<Func<T, object>> field, TextSearchModel textSearch)
        {
            if (textSearch == null || string.IsNullOrWhiteSpace(textSearch.Text))
                return Builders<T>.Filter.Empty;

            switch (textSearch.Mode)
            {
                default:
                    return Builders<T>.Filter.Regex(field,
                        new BsonRegularExpression(textSearch.Text, "i"));

                case TextSearchModes.StartsWith:
                    return builder.Regex(field,
                        new BsonRegularExpression(new Regex($"^({textSearch.Text})")));

                case TextSearchModes.EndsWith:
                    return builder.Regex(field,
                        new BsonRegularExpression(new Regex($"({textSearch.Text})$")));

                case TextSearchModes.InsensitiveEqual:
                    return builder.Regex(field,
                        new BsonRegularExpression(new Regex($"^({textSearch.Text})$", RegexOptions.IgnoreCase)));

                case TextSearchModes.Equal:
                    return builder.Regex(field,
                        new BsonRegularExpression(new Regex($"^({textSearch.Text})$")));
            }
        }

        /// <summary>
        ///     Search for text base on search mode.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="builder"></param>
        /// <param name="field"></param>
        /// <param name="textSearch"></param>
        /// <returns></returns>
        public static FilterDefinition<T> SearchText<T>(this FilterDefinitionBuilder<T> builder,
            FieldDefinition<T> field, TextSearchModel textSearch)
        {
            if (textSearch == null || string.IsNullOrWhiteSpace(textSearch.Text))
                return Builders<T>.Filter.Empty;

            switch (textSearch.Mode)
            {
                default:
                    return Builders<T>.Filter.Regex(field,
                        new BsonRegularExpression(textSearch.Text, "i"));

                case TextSearchModes.StartsWith:
                    return builder.Regex(field,
                        new BsonRegularExpression(new Regex($"^({textSearch.Text})")));

                case TextSearchModes.EndsWith:
                    return builder.Regex(field,
                        new BsonRegularExpression(new Regex($"({textSearch.Text})$")));

                case TextSearchModes.InsensitiveEqual:
                    return builder.Regex(field,
                        new BsonRegularExpression(new Regex($"^({textSearch.Text})$", RegexOptions.IgnoreCase)));

                case TextSearchModes.Equal:
                    return builder.Regex(field,
                        new BsonRegularExpression(new Regex($"^({textSearch.Text})$")));
            }
        }
    }
}