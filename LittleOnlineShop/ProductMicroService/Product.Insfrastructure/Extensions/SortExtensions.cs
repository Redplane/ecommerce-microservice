﻿using System;
using System.Linq.Expressions;
using MongoDB.Driver;
using SharedInfrastructure.Enums;
using SharedInfrastructure.Models;

namespace ProductInfrastructure.Extensions
{
    public static class SortExtensions
    {
        #region Methods

        /// <summary>
        ///     Search for text base on search mode.
        /// </summary>
        /// <typeparam name="TDocument"></typeparam>
        /// <typeparam name="TProjection"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="documents"></param>
        /// <param name="field"></param>
        /// <param name="sort"></param>
        /// <returns></returns>
        public static IFindFluent<TDocument, TProjection> AdvanceSort<TDocument, TProjection, TProperty>(
            this IFindFluent<TDocument, TProjection> documents,
            Expression<Func<TDocument, object>> field, SortValueObject<TProperty> sort) where TProperty : Enum
        {
            if (sort == null)
                return documents;

            var sortDefinitionBuilder = new SortDefinitionBuilder<TDocument>();
            switch (sort.Directions)
            {
                case SortDirections.Ascending:
                    var ascendingSortDefinition = sortDefinitionBuilder.Ascending(field);
                    return documents.Sort(ascendingSortDefinition);

                case SortDirections.Descending:
                    var descendingSortDefinition = sortDefinitionBuilder.Descending(field);
                    return documents.Sort(descendingSortDefinition);
            }

            return documents;
        }

        #endregion
    }
}