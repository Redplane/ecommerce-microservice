﻿namespace ProductInfrastructure.Enums
{
    public enum NumericComparisionModes
    {
        LessThan,
        LessThanOrEqualTo,
        EqualTo,
        GreaterThanOrEqualTo,
        GreaterThan
    }
}