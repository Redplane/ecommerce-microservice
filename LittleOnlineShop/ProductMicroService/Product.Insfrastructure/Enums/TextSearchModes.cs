﻿namespace ProductInfrastructure.Enums
{
    public enum TextSearchModes
    {
        Contains,
        Equal,
        InsensitiveEqual,
        StartsWith,
        EndsWith
    }
}