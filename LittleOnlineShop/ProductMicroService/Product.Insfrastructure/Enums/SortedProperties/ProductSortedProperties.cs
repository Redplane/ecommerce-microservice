﻿namespace ProductInfrastructure.Enums.SortedProperties
{
    public enum ProductSortedProperties
    {
        Sku,
        Name,
        Price,
        CreatedTime,
        LastModifiedTime
    }
}