﻿namespace ProductInfrastructure.Constants
{
    public class HttpResponseMessageCodeConstants
    {
        #region Properties

        /// <summary>
        ///     Category is duplicated.
        /// </summary>
        public const string CategoryDuplicated = "category_duplicated";

        /// <summary>
        ///     Product sku duplicated.
        /// </summary>
        public const string ProductSkuDuplicated = "product_sku_duplicated";

        /// <summary>
        ///     Product is duplicated.
        /// </summary>
        public const string ProductDuplicated = "product_duplicated";

        public const string ProductNotFound = "product_not_found";

        public const string NotEnoughProductForReservation = "not_enough_product_for_reservation";

        /// <summary>
        ///     Reserved product not found.
        /// </summary>
        public const string ReservedProductNotFound = "reserved_product_not_found";

        /// <summary>
        ///     At least one product category doesn't exist.
        /// </summary>
        public const string AtLeastOneProductCategoryNotExists = "at_least_one_product_category_not_exists";

        /// <summary>
        ///     Product photo is not found.
        /// </summary>
        public const string ProductPhotoNotFound = "product_photo_not_found";

        /// <summary>
        ///     Filtered property name is not supported.
        /// </summary>
        public const string NotSupportedFilteredPropertyName = "filtered_property_name_not_supported";

        /// <summary>
        /// Message which will be sent to client when user cannot access to protected api resource.
        /// </summary>
        public const string InvalidGrant = "invalid_grant";

        /// <summary>
        /// No pending product basket is found.
        /// </summary>
        public const string PendingProductBasketNotFound = "pending_product_basket_not_found";

        #endregion
    }
}