﻿namespace ProductInfrastructure.Constants
{
    public class ValidatorMessageCodeConstants
    {
        #region Properties

        /// <summary>
        ///     Product id is invalid.
        /// </summary>
        public const string InvalidProductId = "invalid_product_id";

        /// <summary>
        ///     Quantity is invalid.
        /// </summary>
        public const string InvalidQuantity = "invalid_quantity";

        #endregion
    }
}