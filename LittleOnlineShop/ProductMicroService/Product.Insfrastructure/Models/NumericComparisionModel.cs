﻿using ProductInfrastructure.Enums;

namespace ProductInfrastructure.Models
{
    public class NumericComparisionModel<T>
    {
        #region Constructor

        public NumericComparisionModel(T value, NumericComparisionModes mode)
        {
            Value = value;
            Mode = mode;
        }

        #endregion

        #region Properties

        public T Value { get; }

        public NumericComparisionModes Mode { get; }

        #endregion
    }
}