﻿using ProductInfrastructure.Enums;

namespace ProductInfrastructure.Models
{
    public class TextSearchModel
    {
        #region Methods

        public bool ShouldResultSearched()
        {
            if (string.IsNullOrWhiteSpace(Text))
                return false;

            return true;
        }

        #endregion

        #region Properties

        public TextSearchModes Mode { get; set; }

        public string Text { get; set; }

        #endregion

        #region Constructors

        public TextSearchModel()
        {
        }

        public TextSearchModel(TextSearchModes mode, string text)
        {
            Mode = mode;
            Text = text;
        }

        #endregion
    }
}