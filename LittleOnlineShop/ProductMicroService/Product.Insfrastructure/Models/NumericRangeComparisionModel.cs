﻿namespace ProductInfrastructure.Models
{
    public class NumericRangeComparisionModel<T>
    {
        #region Constructor

        public NumericRangeComparisionModel(NumericComparisionModel<T> min, NumericComparisionModel<T> max)
        {
            Min = min;
            Max = max;
        }

        #endregion

        #region Properties

        public NumericComparisionModel<T> Min { get; }

        public NumericComparisionModel<T> Max { get; }

        #endregion
    }
}