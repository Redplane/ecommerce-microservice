﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using ProductDomain.Models.Entities;
using SharedDomain.Enums;
using SharedDomain.Models;
using SharedInfrastructure.Enums.SortProperties;
using SharedInfrastructure.Models;

namespace ProductInfrastructure.Factories.Interfaces
{
    public interface ICategoryFactory
    {
        #region Methods

        /// <summary>
        ///     Add category into system.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="description"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<Category> AddCategoryAsync(string name, string description, CancellationToken cancellationToken = default);

        /// <summary>
        ///     Find categories by ids asynchronously.
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<List<Category>> FindCategoriesByIdsAsync(HashSet<Guid> ids, CancellationToken cancellationToken = default);

        /// <summary>
        ///     Find categories asynchronously.
        /// </summary>
        /// <returns></returns>
        Task<SearchResultValueObject<Category>> FindCategoriesAsync(HashSet<Guid> ids,
            MasterItemAvailabilities? availability,
            SortValueObject<CategorySortProperties> sort, PagerValueObject pager = default,
            CancellationToken cancellationToken = default);

        #endregion
    }
}