﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using ProductDomain.Models;
using ProductDomain.Models.Entities;
using ProductInfrastructure.Enums.SortedProperties;
using ProductInfrastructure.Models;
using SharedDomain.Enums;
using SharedDomain.Models;
using SharedInfrastructure.Models;

namespace ProductInfrastructure.Factories.Interfaces
{
    public interface IProductFactory
    {
        #region Methods

        /// <summary>
        ///     Add product into system asynchronously.
        /// </summary>
        /// <param name="categoryIds"></param>
        /// <param name="sku"></param>
        /// <param name="name"></param>
        /// <param name="description"></param>
        /// <param name="specifications"></param>
        /// <param name="price"></param>
        /// <param name="availability"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<Product> AddProductAsync(HashSet<Guid> categoryIds, string sku, string name,
            string description, HashSet<ProductSpecificationValueObject> specifications, decimal price,
            MasterItemAvailabilities availability = MasterItemAvailabilities.Available,
            CancellationToken cancellationToken = default);

        /// <summary>
        ///     Edit a product using specific information.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="categoryIds"></param>
        /// <param name="sku"></param>
        /// <param name="name"></param>
        /// <param name="description"></param>
        /// <param name="specifications"></param>
        /// <param name="price"></param>
        /// <param name="availability"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<Product> EditProductAsync(Guid id, EditableFieldValueObject<HashSet<Guid>> categoryIds,
            EditableFieldValueObject<string> sku,
            EditableFieldValueObject<string> name,
            EditableFieldValueObject<string> description,
            EditableFieldValueObject<ProductSpecificationValueObject[]> specifications,
            EditableFieldValueObject<decimal> price,
            EditableFieldValueObject<MasterItemAvailabilities> availability,
            CancellationToken cancellationToken = default);

        /// <summary>
        ///     Delete a specific product.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<Product> DeleteProductAsync(Guid id, CancellationToken cancellationToken = default);

        /// <summary>
        ///     Add product photo asynchronously.
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="relativeUrl"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<Photo> AddProductPhotoAsync(Guid productId, string relativeUrl,
            CancellationToken cancellationToken = default);

        /// <summary>
        ///     Edit product photo asynchronously.
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="photoId"></param>
        /// <param name="relativeUrl"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<Product> EditProductPhotoAsync(Guid productId, Guid photoId, EditableFieldValueObject<string> relativeUrl,
            CancellationToken cancellationToken = default);

        /// <summary>
        ///     Delete product photo asynchronously.
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="photoId"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<Product> DeleteProductPhotoAsync(Guid productId, Guid? photoId,
            CancellationToken cancellationToken = default);

        /// <summary>
        ///     Find products asynchronously.
        /// </summary>
        /// <returns></returns>
        Task<SearchResultValueObject<Product>> FindProductsAsync(
            HashSet<Guid> ids,
            HashSet<Guid> categoryIds = default, MasterItemAvailabilities? availabilities = default,
            NumericRangeComparisionModel<double> price = default,
            SortValueObject<ProductSortedProperties> sort = default,
            PagerValueObject pager = default, CancellationToken cancellationToken = default);

        /// <summary>
        ///     Add a product into basket asynchronously.
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="item"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<ProductBasket> UpdateProductBasketAsync(Guid customerId, ProductBasketItemValueObject item,
            CancellationToken cancellationToken = default);

        /// <summary>
        /// Checkout the pending product basket.
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<ProductBasket> CheckoutProductBasketAsync(Guid customerId, CancellationToken cancellationToken = default);

        /// <summary>
        /// Find the checked out product basket of a specific customer to reserve it.
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="productBasketId"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<ProductBasket> ReserveCheckedOutProductBasketAsync(Guid customerId, Guid productBasketId,
            CancellationToken cancellationToken = default);

        /// <summary>
        /// Find the reserved product basket and cancel it.
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="productBasketId"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<ProductBasket> CancelCheckedOutProductBasketAsync(Guid customerId, Guid productBasketId,
            CancellationToken cancellationToken);

        /// <summary>
        /// Delete the pending product basket.
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<ProductBasket> DeletePendingProductBasketAsync(Guid customerId, CancellationToken cancellationToken = default);

        #endregion
    }
}