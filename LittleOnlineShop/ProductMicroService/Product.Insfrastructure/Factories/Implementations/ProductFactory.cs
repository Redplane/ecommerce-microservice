﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using ProductDomain.Enums;
using ProductDomain.Models;
using ProductDomain.Models.Entities;
using ProductInfrastructure.Constants;
using ProductInfrastructure.Enums.SortedProperties;
using ProductInfrastructure.Extensions;
using ProductInfrastructure.Factories.Interfaces;
using ProductInfrastructure.Models;
using SharedDomain.Enums;
using SharedDomain.Extensions;
using SharedDomain.Models;
using SharedInfrastructure.Enums;
using SharedInfrastructure.Exceptions;
using SharedInfrastructure.Models;

namespace ProductInfrastructure.Factories.Implementations
{
    public class ProductFactory : IProductFactory
    {
        #region Constructor

        public ProductFactory(IMongoClient dbClient, IMongoCollection<Product> products,
            IMongoCollection<ProductBasket> productBaskets)
        {
            _dbClient = dbClient;
            _products = products;
            _productBaskets = productBaskets;
        }

        #endregion

        #region Properties

        private readonly IMongoClient _dbClient;

        private readonly IMongoCollection<Product> _products;

        private readonly IMongoCollection<ProductBasket> _productBaskets;

        #endregion

        #region Methods

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="categoryIds"></param>
        /// <param name="sku"></param>
        /// <param name="name"></param>
        /// <param name="description"></param>
        /// <param name="specifications"></param>
        /// <param name="price"></param>
        /// <param name="availability"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task<Product> AddProductAsync(HashSet<Guid> categoryIds, string sku, string name,
            string description, HashSet<ProductSpecificationValueObject> specifications,
            decimal price, MasterItemAvailabilities availability = MasterItemAvailabilities.Available,
            CancellationToken cancellationToken = default)
        {
            // Check whether there is any product with the same sku or not.
            var findDuplicateSkuProductFilterDefinition = Builders<Product>.Filter
                .Regex(x => x.Sku, new BsonRegularExpression(new Regex($"^{sku}$", RegexOptions.IgnoreCase)));

            // Find the first product with the same sku.
            var duplicateSkuProduct = await _products.Find(findDuplicateSkuProductFilterDefinition)
                .Limit(1)
                .FirstOrDefaultAsync(cancellationToken);

            // Duplicate sku product is found.
            if (duplicateSkuProduct != null)
                throw new HttpResponseException(HttpStatusCode.Conflict,
                    HttpResponseMessageCodeConstants.ProductDuplicated);

            // Get the current system time.
            var unixTime = DateTime.UtcNow.ToUnixTime();

            var product = new Product(Guid.NewGuid());
            product.CategoryIds = categoryIds.ToArray();
            product.Sku = sku;
            product.Name = name;
            product.Description = description;
            product.Specifications = specifications.ToArray();
            product.Price = price;
            product.Availability = availability;
            product.CreatedTime = unixTime;

            // Add the product into system.
            await _products.InsertOneAsync(product, null, cancellationToken);

            // Return the newly created product.
            return product;
        }

        /// <summary>
        ///     <inheritdoc c />
        /// </summary>
        /// <param name="id"></param>
        /// <param name="categoryIds"></param>
        /// <param name="sku"></param>
        /// <param name="name"></param>
        /// <param name="description"></param>
        /// <param name="specifications"></param>
        /// <param name="price"></param>
        /// <param name="availability"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task<Product> EditProductAsync(Guid id,
            EditableFieldValueObject<HashSet<Guid>> categoryIds,
            EditableFieldValueObject<string> sku,
            EditableFieldValueObject<string> name, EditableFieldValueObject<string> description,
            EditableFieldValueObject<ProductSpecificationValueObject[]> specifications,
            EditableFieldValueObject<decimal> price,
            EditableFieldValueObject<MasterItemAvailabilities> availability,
            CancellationToken cancellationToken = default)
        {
            // Find the product with specific id.
            var findProductFilterDefinition = Builders<Product>.Filter
                .Eq(x => x.Id, id);

            // List of product update definitions.
            var updateProductDefinitions = new List<UpdateDefinition<Product>>();

            // Category ids are marked to be changed.
            if (categoryIds != null && categoryIds.HasModified)
            {
                var updateCategoryIdDefinition =
                    Builders<Product>.Update.Set(x => x.CategoryIds, categoryIds.Value.ToArray());
                updateProductDefinitions.Add(updateCategoryIdDefinition);
            }

            // Sku is marked to be changed.
            if (sku != null && sku.HasModified)
            {
                // Check whether sku is duplicated or not
                var findDuplicateSkuProductFilterDefinition = Builders<Product>.Filter
                    .Regex(x => x.Id, new BsonRegularExpression(new Regex($"^{sku.Value}$", RegexOptions.IgnoreCase)));

                // Find the duplicate sku product.
                var duplicateSkuProduct = await _products.Find(findDuplicateSkuProductFilterDefinition)
                    .Limit(1)
                    .FirstOrDefaultAsync(cancellationToken);

                // Duplicate product is found and it is not the same as current product.
                if (duplicateSkuProduct != null && duplicateSkuProduct.Id != id)
                    throw new HttpResponseException(HttpStatusCode.Conflict,
                        HttpResponseMessageCodeConstants.ProductSkuDuplicated);

                // Duplicate product is not found.
                if (duplicateSkuProduct == null)
                {
                    var updateSkuDefinition = Builders<Product>.Update.Set(x => x.Sku, sku.Value);
                    updateProductDefinitions.Add(updateSkuDefinition);
                }
            }

            // Name is marked to be changed.
            if (name != null && name.HasModified)
            {
                var updateNameDefinition = Builders<Product>.Update.Set(x => x.Name, name.Value);
                updateProductDefinitions.Add(updateNameDefinition);
            }

            // Description is marked to be changed.
            if (description != null && description.HasModified)
            {
                var updateDescriptionDefinition = Builders<Product>.Update.Set(x => x.Description, description.Value);
                updateProductDefinitions.Add(updateDescriptionDefinition);
            }

            // Specifications are marked to be changed.
            if (specifications != null && specifications.HasModified)
            {
                var updateSpecificationsDefinition =
                    Builders<Product>.Update.Set(x => x.Specifications, specifications.Value);
                updateProductDefinitions.Add(updateSpecificationsDefinition);
            }

            // Price is marked to be changed.
            if (specifications != null && specifications.HasModified)
            {
                var updatePriceDefinition = Builders<Product>.Update.Set(x => x.Price, price.Value);
                updateProductDefinitions.Add(updatePriceDefinition);
            }

            // Update definitions are defined.
            if (updateProductDefinitions.Count > 0)
            {
                var updateProductDefinition = Builders<Product>.Update.Combine(updateProductDefinitions);
                var findProductAndUpdateOptions = new FindOneAndUpdateOptions<Product>();
                findProductAndUpdateOptions.ReturnDocument = ReturnDocument.After;

                var product = await _products.FindOneAndUpdateAsync(findProductFilterDefinition,
                    updateProductDefinition,
                    findProductAndUpdateOptions, cancellationToken);

                return product;
            }

            return null;
        }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="id"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual Task<Product> DeleteProductAsync(Guid id, CancellationToken cancellationToken = default)
        {
            // Build the product filter.
            var findProductFilterDefinition = Builders<Product>.Filter
                .Eq(x => x.Id, id);

            // Find the active product.
            findProductFilterDefinition &= Builders<Product>.Filter
                .Eq(x => x.Availability, MasterItemAvailabilities.Available);

            // Find the product and update it to unavailable status.
            var updateProductAvailabilityDefinition = Builders<Product>.Update
                .Set(x => x.Availability, MasterItemAvailabilities.Unavailable);

            // Find the product and update the found item.
            var findAndUpdateOptions = new FindOneAndUpdateOptions<Product>();
            findAndUpdateOptions.ReturnDocument = ReturnDocument.After;

            return _products.FindOneAndUpdateAsync(findProductFilterDefinition,
                updateProductAvailabilityDefinition, findAndUpdateOptions, cancellationToken);
        }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="relativeUrl"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task<Photo> AddProductPhotoAsync(Guid productId, string relativeUrl,
            CancellationToken cancellationToken = default)
        {
            // Product filter builder.
            var productFilterBuilder = Builders<Product>.Filter;

            // Find product by id.
            var productFilterDefinition = productFilterBuilder
                .Eq(product => product.Id, productId);

            // Initialize product photo.
            var photo = new Photo(Guid.NewGuid());
            photo.RelativeUrl = relativeUrl;
            photo.Availability = MasterItemAvailabilities.Available;
            photo.CreatedTime = DateTime.UtcNow.ToUnixTime();

            var updateProductBuilder = Builders<Product>.Update;
            var updateProductDefinition = updateProductBuilder.Push(product => product.Photos, photo);

            var findOneAndUpdateOptions = new FindOneAndUpdateOptions<Product>();
            findOneAndUpdateOptions.ReturnDocument = ReturnDocument.After;
            var updatedProduct = await _products.FindOneAndUpdateAsync(productFilterDefinition, updateProductDefinition,
                findOneAndUpdateOptions,
                cancellationToken);

            if (updatedProduct == null)
                throw new HttpResponseException(HttpStatusCode.NotFound,
                    HttpResponseMessageCodeConstants.ProductNotFound);

            return photo;
        }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="photoId"></param>
        /// <param name="relativeUrl"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task<Product> EditProductPhotoAsync(Guid productId, Guid photoId,
            EditableFieldValueObject<string> relativeUrl,
            CancellationToken cancellationToken = default)
        {
            if (relativeUrl == null || !relativeUrl.HasModified)
                throw new HttpResponseException(HttpStatusCode.NotModified, string.Empty);

            // Product filter builder.
            var productFilterBuilder = Builders<Product>.Filter;
            var productFilterDefinition = FilterDefinition<Product>.Empty;

            // Find product photo by id.
            productFilterDefinition &= productFilterBuilder.Exists(product => product.Photos);
            productFilterDefinition &= productFilterBuilder.ElemMatch(product => product.Photos,
                Builders<Photo>.Filter.Eq(photo => photo.Id, photoId));

            // Update product image.
            var updateProductBuilder = Builders<Product>.Update;
            var updateProductFilterDefinition =
                updateProductBuilder.Set(product => product.Photos[-1].RelativeUrl, relativeUrl.Value);

            var findOneAndUpdateOptions = new FindOneAndUpdateOptions<Product>();
            findOneAndUpdateOptions.ReturnDocument = ReturnDocument.After;
            var updatedProduct = await _products.FindOneAndUpdateAsync(productFilterDefinition,
                updateProductFilterDefinition, findOneAndUpdateOptions, cancellationToken);

            if (updatedProduct == null)
                throw new HttpResponseException(HttpStatusCode.NotFound,
                    HttpResponseMessageCodeConstants.ProductNotFound);

            return updatedProduct;
        }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="photoId"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task<Product> DeleteProductPhotoAsync(Guid productId, Guid? photoId,
            CancellationToken cancellationToken = default)
        {
            // Product filter builder.
            var productFilterBuilder = Builders<Product>.Filter;
            var productFilterDefinition = FilterDefinition<Product>.Empty;
            var photoFilterBuilder = Builders<Photo>.Filter;
            var photoFilterDefinition = FilterDefinition<Photo>.Empty;

            // Photo is defined.
            if (photoId != null)
                photoFilterDefinition &= photoFilterBuilder.Eq(photo => photo.Id, photoId.Value);

            // Find product photo by id.
            productFilterDefinition &= productFilterBuilder.Exists(product => product.Photos);

            if (photoId != null)
                productFilterDefinition &= productFilterBuilder.ElemMatch(product => product.Photos,
                    Builders<Photo>.Filter.Eq(photo => photo.Id, photoId));

            // Update product image.
            var updateProductBuilder = Builders<Product>.Update;
            var updateProductFilterDefinition =
                updateProductBuilder.PullFilter(x => x.Photos, photoFilterDefinition);

            var findOneAndUpdateOptions = new FindOneAndUpdateOptions<Product>();
            findOneAndUpdateOptions.ReturnDocument = ReturnDocument.After;

            // Find and update the product.
            var updatedProduct = await _products.FindOneAndUpdateAsync(productFilterDefinition,
                updateProductFilterDefinition, findOneAndUpdateOptions, cancellationToken);
            if (updatedProduct == null)
                throw new HttpResponseException(HttpStatusCode.NotFound,
                    HttpResponseMessageCodeConstants.ProductPhotoNotFound);

            return updatedProduct;
        }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <returns></returns>
        public virtual Task<SearchResultValueObject<Product>> FindProductsAsync(HashSet<Guid> ids,
            HashSet<Guid> categoryIds, MasterItemAvailabilities? availabilities,
            NumericRangeComparisionModel<double> price,
            SortValueObject<ProductSortedProperties> sort = default,
            PagerValueObject pager = default, CancellationToken cancellationToken = default)
        {
            var productFilterBuilder = Builders<Product>.Filter;
            var productFilterDefinition = productFilterBuilder.Empty;

            // Ids are defined.
            if (ids != null && ids.Count > 0)
                productFilterDefinition &= productFilterBuilder.In(product => product.Id, ids);
                
            // Category ids are defined.
            if (categoryIds != null && categoryIds.Count > 0)
                productFilterDefinition &= productFilterBuilder.AnyIn(product => product.CategoryIds, categoryIds);

            // Availabilities are defined.
            if (availabilities != null)
                productFilterDefinition &=
                    productFilterBuilder.Eq(product => product.Availability, availabilities.Value);

            // Prices are defined.
            if (price != null)
                productFilterDefinition &= productFilterBuilder.SearchNumeric(product => product.Price, price);

            // Do products search.
            var findProductsFluent = _products.Find(productFilterDefinition);

            if (sort != null)
                switch (sort.Property)
                {
                    case ProductSortedProperties.Sku:
                        findProductsFluent = findProductsFluent.AdvanceSort(x => x.Sku, sort);
                        break;

                    case ProductSortedProperties.Name:
                        findProductsFluent = findProductsFluent.AdvanceSort(x => x.Name, sort);
                        break;

                    case ProductSortedProperties.Price:
                        findProductsFluent = findProductsFluent.AdvanceSort(x => x.Price, sort);
                        break;

                    case ProductSortedProperties.LastModifiedTime:
                        findProductsFluent = findProductsFluent.AdvanceSort(x => x.LastModifiedTime, sort);
                        break;

                    default:
                        findProductsFluent = findProductsFluent.AdvanceSort(x => x.CreatedTime, sort);
                        break;
                }
            else
                findProductsFluent = findProductsFluent.AdvanceSort(x => x.CreatedTime,
                    new SortValueObject<ProductSortedProperties>(ProductSortedProperties.CreatedTime,
                        SortDirections.Descending));

            return findProductsFluent.ToSearchResultAsync(pager, cancellationToken);
        }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="basketItem"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task<ProductBasket> UpdateProductBasketAsync(Guid customerId,
            ProductBasketItemValueObject basketItem, CancellationToken cancellationToken = default)
        {
            using (var session = _dbClient.StartSession())
            {
                try
                {
                    // Start the session.
                    session.StartTransaction();

                    // Find the product basket of specific customer.
                    var productBasketFilterBuilder = Builders<ProductBasket>.Filter;
                    var productBasketFilterDefinition = productBasketFilterBuilder.Eq(x => x.CustomerId, customerId);
                    productBasketFilterDefinition &=
                        productBasketFilterBuilder.Eq(x => x.Status, ProductBasketStatuses.Pending);

                    // Find the first product basket that belongs to specific customer.
                    var productBasket = await _productBaskets.Find(session, productBasketFilterDefinition)
                        .FirstOrDefaultAsync(cancellationToken);

                    // Product basket not found.
                    if (productBasket == null)
                    {
                        productBasket = new ProductBasket(Guid.NewGuid(), customerId);
                        productBasket.Items = new[] {basketItem};

                        // Insert a new product basket into system.
                        await _productBaskets.InsertOneAsync(session, productBasket, null, cancellationToken);
                    }
                    else
                    {
                        var items = productBasket.Items?.ToHashSet();
                        if (items == null)
                            items = new HashSet<ProductBasketItemValueObject>();

                        items.RemoveWhere(item => item.ProductId == basketItem.ProductId);
                        items.Add(basketItem);

                        var updateProductBasketDefinition = Builders<ProductBasket>
                            .Update
                            .Set(item => item.Items, items.ToArray())
                            .Set(x => x.LastModifiedTime, DateTime.UtcNow.ToUnixTime());

                        var findOneAndUpdateOptions = new FindOneAndUpdateOptions<ProductBasket>();
                        findOneAndUpdateOptions.ReturnDocument = ReturnDocument.After;

                        productBasket = _productBaskets.FindOneAndUpdate(session, productBasketFilterDefinition,
                            updateProductBasketDefinition, findOneAndUpdateOptions);
                    }

                    session.CommitTransaction(cancellationToken);
                    return productBasket;
                }
                catch
                {
                    session.AbortTransaction(cancellationToken);
                    throw;
                }
            }
        }

        /// <summary>
        /// Checkout the product basket
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public virtual Task<ProductBasket> CheckoutProductBasketAsync(Guid customerId, CancellationToken cancellationToken = default)
        {
            // Build the product search condition.
            var productBasketFilterBuilder = Builders<ProductBasket>.Filter;
            
            // Find the basket belongs to customer.
            var productBasketFilterDefinition = productBasketFilterBuilder.Eq(x => x.CustomerId, customerId);

            // Find the pending basket.
            productBasketFilterDefinition &=
                productBasketFilterBuilder.Eq(x => x.Status, ProductBasketStatuses.Pending);
            
            // Find the available basket.
            productBasketFilterDefinition &=
                productBasketFilterBuilder.Eq(x => x.Availability, MasterItemAvailabilities.Available);
            
            // Find the basket which contains at least one product.
            productBasketFilterDefinition &= productBasketFilterBuilder.Exists(x => x.Items[0].ProductId);
            
            // Build the product basket update.
            var productBasketUpdateBuilder = Builders<ProductBasket>.Update;
            var productBasketUpdateDefinition = productBasketUpdateBuilder
                .Set(x => x.Status, ProductBasketStatuses.Reserved);
            
            var findOneAndUpdateOptions = new FindOneAndUpdateOptions<ProductBasket>();
            findOneAndUpdateOptions.ReturnDocument = ReturnDocument.After;
            
            // Find the product basket and change its status to reserved.
            return _productBaskets.FindOneAndUpdateAsync(productBasketFilterDefinition, productBasketUpdateDefinition,
                findOneAndUpdateOptions, cancellationToken);
        }

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="productBasketId"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public virtual Task<ProductBasket> ReserveCheckedOutProductBasketAsync(Guid customerId, Guid productBasketId,
            CancellationToken cancellationToken = default)
        {
            // Find product basket using specific condition.
            var productBasketFilterBuilder = Builders<ProductBasket>.Filter;
            
            // Find the product basket by using customer id.
            var productBasketFilterDefinition = productBasketFilterBuilder.Eq(x => x.CustomerId, customerId);
            
            // Find the basket using its id.
            productBasketFilterDefinition &= productBasketFilterBuilder.Eq(x => x.Id, productBasketId);
            
            // Find the checked out product basket.
            productBasketFilterDefinition &=
                productBasketFilterBuilder.Eq(x => x.Status, ProductBasketStatuses.CheckedOut);
            
            // Build product basket update definition.
            var productBasketUpdateBuilder = Builders<ProductBasket>.Update;
            var productBasketUpdateDefinition = productBasketUpdateBuilder
                .Set(x => x.Status, ProductBasketStatuses.Reserved);
            
            var findOneAndUpdateDefinition = new FindOneAndUpdateOptions<ProductBasket>();
            findOneAndUpdateDefinition.ReturnDocument = ReturnDocument.After;

            return _productBaskets.FindOneAndUpdateAsync(productBasketFilterDefinition, productBasketUpdateDefinition,
                findOneAndUpdateDefinition, cancellationToken);
        }

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="productBasketId"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public virtual Task<ProductBasket> CancelCheckedOutProductBasketAsync(Guid customerId, Guid productBasketId, CancellationToken cancellationToken)
        {
            // Find product basket using specific condition.
            var productBasketFilterBuilder = Builders<ProductBasket>.Filter;
            
            // Find the product basket by using customer id.
            var productBasketFilterDefinition = productBasketFilterBuilder.Eq(x => x.CustomerId, customerId);
            
            // Find the basket using its id.
            productBasketFilterDefinition &= productBasketFilterBuilder.Eq(x => x.Id, productBasketId);
            
            // Find the checked out product basket.
            productBasketFilterDefinition &=
                productBasketFilterBuilder.Eq(x => x.Status, ProductBasketStatuses.CheckedOut);
            
            // Build product basket update definition.
            var productBasketUpdateBuilder = Builders<ProductBasket>.Update;
            var productBasketUpdateDefinition = productBasketUpdateBuilder
                .Set(x => x.Status, ProductBasketStatuses.Cancelled);
            
            var findOneAndUpdateDefinition = new FindOneAndUpdateOptions<ProductBasket>();
            findOneAndUpdateDefinition.ReturnDocument = ReturnDocument.After;

            return _productBaskets.FindOneAndUpdateAsync(productBasketFilterDefinition, productBasketUpdateDefinition,
                findOneAndUpdateDefinition, cancellationToken);
        }

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public virtual Task<ProductBasket> DeletePendingProductBasketAsync(Guid customerId, CancellationToken cancellationToken = default)
        {
            // Find the product basket.
            var productBasketFilterBuilder = Builders<ProductBasket>.Filter;
            
            // Find the product basket belongs to specific customer.
            var productBasketFilterDefinition = productBasketFilterBuilder.Eq(x => x.CustomerId, customerId);
            
            // Find the pending product basket.
            productBasketFilterDefinition &=
                productBasketFilterBuilder.Eq(x => x.Status, ProductBasketStatuses.Pending);

            // Find the available product basket.
            productBasketFilterDefinition &= productBasketFilterBuilder
                .Eq(x => x.Availability, MasterItemAvailabilities.Available);
            
            // Build product update definition.
            var updateProductBasketBuilder = Builders<ProductBasket>.Update;
            var updateProductBasketDefinition =
                updateProductBasketBuilder.Set(x => x.Availability, MasterItemAvailabilities.Unavailable);
            
            var findOneAndUpdateDefinition = new FindOneAndUpdateOptions<ProductBasket>();
            findOneAndUpdateDefinition.ReturnDocument = ReturnDocument.After;
            return _productBaskets.FindOneAndUpdateAsync(productBasketFilterDefinition, updateProductBasketDefinition, findOneAndUpdateDefinition, cancellationToken);
        }

        #endregion
    }
}