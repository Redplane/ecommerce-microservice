﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using MongoDB.Bson;
using MongoDB.Driver;
using ProductDomain.Models.Entities;
using ProductInfrastructure.Constants;
using ProductInfrastructure.Extensions;
using ProductInfrastructure.Factories.Interfaces;
using SharedDomain.Enums;
using SharedDomain.Extensions;
using SharedDomain.Models;
using SharedInfrastructure.Enums.SortProperties;
using SharedInfrastructure.Exceptions;
using SharedInfrastructure.Models;

namespace ProductInfrastructure.Factories.Implementations
{
    public class CategoryFactory : ICategoryFactory
    {
        #region Constructor

        public CategoryFactory(IMongoCollection<Category> categories,
            IHttpContextAccessor httpContextAccessor)
        {
            _categories = categories;
            _httpClient = httpContextAccessor.HttpContext;
        }

        #endregion

        #region Properties

        private readonly IMongoCollection<Category> _categories;

        private readonly HttpContext _httpClient;

        #endregion

        #region Methods

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="name"></param>
        /// <param name="description"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task<Category> AddCategoryAsync(string name, string description,
            CancellationToken cancellationToken = default)
        {
            // Find category by name.
            var findCategoryFilterBuilder = Builders<Category>.Filter;
            var findCategoryFilterDefinition = findCategoryFilterBuilder.Empty;

            findCategoryFilterDefinition &= findCategoryFilterBuilder
                .Regex(x => x.Name,
                    new BsonRegularExpression(new Regex($"^{name}$", RegexOptions.IgnoreCase)));

            // Check whether category is duplicated or not.
            var category = await _categories.Find(findCategoryFilterDefinition)
                .Limit(1)
                .FirstOrDefaultAsync(cancellationToken);

            // Category already added.
            if (category != null)
                throw new HttpResponseException(HttpStatusCode.Conflict,
                    HttpResponseMessageCodeConstants.CategoryDuplicated);

            // No category has been added before. Add a new one.
            category = new Category(Guid.NewGuid());
            category.Name = name;
            category.Description = description;
            category.Availability = MasterItemAvailabilities.Available;
            category.CreatedTime = DateTime.UtcNow.ToUnixTime();

            await _categories.InsertOneAsync(category, null, cancellationToken);
            return category;
        }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual Task<List<Category>> FindCategoriesByIdsAsync(HashSet<Guid> ids,
            CancellationToken cancellationToken = default)
        {
            var findCategoriesFilterBuilder = Builders<Category>.Filter;
            var findCategoriesFilterDefinition = findCategoriesFilterBuilder.Empty;

            if (ids != null && ids.Count > 0)
                findCategoriesFilterDefinition &= findCategoriesFilterBuilder.In(x => x.Id, ids);

            return _categories.Find(findCategoriesFilterDefinition)
                .ToListAsync(cancellationToken);
        }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <returns></returns>
        public virtual async Task<SearchResultValueObject<Category>> FindCategoriesAsync(HashSet<Guid> ids,
            MasterItemAvailabilities? availability,
            SortValueObject<CategorySortProperties> sort, PagerValueObject pager,
            CancellationToken cancellationToken = default)
        {
            var findCategoriesFilterBuilder = Builders<Category>.Filter;
            var findCategoriesFilterDefinition = findCategoriesFilterBuilder.Empty;

            // Ids are defined.
            if (ids != null && ids.Count > 0)
                findCategoriesFilterDefinition &= findCategoriesFilterBuilder.In(x => x.Id, ids);

            // Availability is defined.
            if (availability != null)
                findCategoriesFilterDefinition &=
                    findCategoriesFilterBuilder.Eq(x => x.Availability, availability.Value);

            // Find categories fluent.
            var findCategoriesFluent = _categories
                .Find(findCategoriesFilterDefinition);

            if (sort != null)
                switch (sort.Property)
                {
                    case CategorySortProperties.Name:
                        findCategoriesFluent = findCategoriesFluent.AdvanceSort(x => x.Name, sort);
                        break;

                    case CategorySortProperties.LastModifiedTime:
                        findCategoriesFluent = findCategoriesFluent.AdvanceSort(x => x.LastModifiedTime, sort);
                        break;

                    default:
                        findCategoriesFluent = findCategoriesFluent.AdvanceSort(x => x.CreatedTime, sort);
                        break;
                }

            return await findCategoriesFluent
                .ToSearchResultAsync(pager, cancellationToken);
        }

        #endregion
    }
}