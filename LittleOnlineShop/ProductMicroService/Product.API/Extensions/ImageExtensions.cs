﻿using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using SkiaSharp;

namespace ProductApi.Extensions
{
    public static class ImageExtensions
    {
        #region Methods

        /// <summary>
        ///     Convert form file to image asynchronously.
        /// </summary>
        /// <returns></returns>
        public static async Task<SKImage> ToImageAsync(this IFormFile file,
            CancellationToken cancellationToken = default)
        {
            using (var memoryStream = new MemoryStream())
            {
                await file.CopyToAsync(memoryStream, cancellationToken);
                memoryStream.Seek(0, SeekOrigin.Begin);
                var skBitmap = SKBitmap.Decode(memoryStream);
                return SKImage.FromBitmap(skBitmap);
            }
        }

        /// <summary>
        ///     Convert form file to image synchronously.
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public static SKImage ToImage(this IFormFile file)
        {
            using (var memoryStream = new MemoryStream())
            {
                file.CopyTo(memoryStream);
                memoryStream.Seek(0, SeekOrigin.Begin);
                var skBitmap = SKBitmap.Decode(memoryStream);
                return SKImage.FromBitmap(skBitmap);
            }
        }

        #endregion
    }
}