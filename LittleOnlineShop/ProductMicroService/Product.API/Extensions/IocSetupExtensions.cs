﻿using System.Reflection;
using Imgur.API.Authentication.Impl;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using ProductApi.Constants;
using ProductApi.Cqrs.Pipelines.Behaviours;
using ProductApi.Models;
using ProductApi.Services.Implementations;
using ProductApi.Services.Interfaces;
using ProductInfrastructure.Factories.Implementations;
using ProductInfrastructure.Factories.Interfaces;

namespace ProductApi.Extensions
{
    public static class IocSetupExtensions
    {
        #region Methods

        /// <summary>
        ///     Run IOC setup.
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        public static void AddIoc(this IServiceCollection services, IConfiguration configuration)
        {
            // Add image configuration.
            services.Configure<ImgurClientSettings>(
                configuration.GetSection(ConfigurationSectionKeyConstants.ImageUploadHostingClient));

            // Factories registration.
            services.AddScoped<IProductFactory, ProductFactory>();
            services.AddScoped<ICategoryFactory, CategoryFactory>();

            // Image client settings.
            services.AddScoped(x =>
            {
                var settings = x.GetService<IOptions<ImgurClientSettings>>()?.Value;
                if (settings == null)
                    return null;

                return new ImgurClient(settings.ClientId, settings.ClientSecret);
            });


            // Service registration.
            services.AddScoped<IProductPhotoService, ProductPhotoService>();

            // Add mediator.
            services.AddMediatR(typeof(Startup).GetTypeInfo().Assembly);

            // Request validation.
            services.AddScoped(typeof(IPipelineBehavior<,>), typeof(RequestValidationBehavior<,>));
        }

        #endregion
    }
}