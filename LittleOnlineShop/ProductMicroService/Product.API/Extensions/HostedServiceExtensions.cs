using Microsoft.Extensions.DependencyInjection;
using ProductApi.HostedServices;

namespace ProductApi.Extensions
{
    public static class HostedServiceExtensions
    {
        #region Methods

        public static void AddHostedServiceSetup(this IServiceCollection services)
        {
            services.AddHostedService<MqHostedService>();
        }
        
        #endregion
    }
}