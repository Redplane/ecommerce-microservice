﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;
using ProductDomain.Models;
using ProductDomain.Models.Entities;
using SharedDomain.Enums;
using SharedDomain.Extensions;

namespace ProductApi.Extensions
{
    public static class ProductDatabaseExtensions
    {
        #region Methods

        /// <summary>
        ///     Add initial categories if they do not exist.
        /// </summary>
        /// <param name="app"></param>
        /// <returns></returns>
        public static IApplicationBuilder AddInitialCategories(this IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.CreateScope())
            {
                var serviceProvider = serviceScope.ServiceProvider;
                var categories = serviceProvider.GetService<IMongoCollection<Category>>();

                // Count the number of categories in the database.
                var totalCategories = categories.CountDocuments(FilterDefinition<Category>.Empty);
                if (totalCategories > 0)
                    return app;

                // Initialize initial categories.
                var initialCategories = new LinkedList<Category>();

                for (var index = 0; index < 10; index++)
                {
                    var category = new Category(Guid.NewGuid());
                    category.Name = $"Category {index}";
                    category.Description = $"Category description {index}";
                    category.Availability = MasterItemAvailabilities.Available;
                    category.CreatedTime = DateTime.UtcNow.ToUnixTime();

                    initialCategories.AddLast(category);
                }

                categories.InsertMany(initialCategories);
            }

            return app;
        }

        /// <summary>
        ///     Add initial products there nothing available.
        /// </summary>
        /// <param name="app"></param>
        /// <returns></returns>
        public static IApplicationBuilder AddInitialProducts(this IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.CreateScope())
            {
                var serviceProvider = serviceScope.ServiceProvider;

                var categories = serviceProvider.GetService<IMongoCollection<Category>>();
                var products = serviceProvider.GetService<IMongoCollection<Product>>();

                // Count the number of categories in the database.
                var totalProducts = products.CountDocuments(FilterDefinition<Product>.Empty);
                if (totalProducts > 0)
                    return app;

                // Get all categories in the system.
                var loadedCategories = categories.Find(FilterDefinition<Category>.Empty)
                    .ToList();

                // Initialize initial categories.
                var initialProducts = new LinkedList<Product>();
                var random = new Random();

                for (var index = 0; index < 10; index++)
                {
                    var randomizedCategoryId = random.Next(loadedCategories.Count);
                    var product = new Product(Guid.NewGuid());
                    product.CategoryIds = new[] {loadedCategories[randomizedCategoryId].Id};
                    product.Sku = $"SKU_{index}";
                    product.Name = $"Product name {index}";
                    product.Description = $"Product description {index}";
                    product.Specifications = new[]
                    {
                        new ProductSpecificationValueObject("Height", "100 cm"),
                        new ProductSpecificationValueObject("Width", "120 cm")
                    };
                    product.Photos = new[]
                    {
                        new Photo(Guid.NewGuid())
                        {
                            RelativeUrl = "https://via.placeholder.com/640x480/09f/fff.png",
                            Availability = MasterItemAvailabilities.Available,
                            CreatedTime = DateTime.UtcNow.ToUnixTime()
                        }
                    };
                    product.Thumbnail = "https://via.placeholder.com/640x480/09f/fff.png";
                    product.Price = random.Next(1, 500);

                    initialProducts.AddLast(product);
                }

                products.InsertMany(initialProducts);
            }

            return app;
        }

        #endregion
    }
}