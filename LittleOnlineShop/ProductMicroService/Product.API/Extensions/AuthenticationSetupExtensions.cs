﻿using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Logging;
using ProductApi.Constants;
using SharedDomain.Constants;
using SharedInfrastructure.Models;
using SharedMicroService.AuthenticationHandlers.Implementations;
using SharedMicroService.AuthenticationHandlers.Requirements;
using SharedMicroService.Constants;

namespace ProductApi.Extensions
{
    public static class AuthenticationSetupExtensions
    {
        #region Methods

        public static void AddAuthenticationSetup(this IServiceCollection services, IConfiguration configuration)
        {
            // Get identity server settings.
            var identityServerSettings = new IdentityServerAuthenticationModel();

            var identityServerSection = configuration.GetSection(ConfigurationSectionKeyConstants.IdentityServerClient);
            services.Configure<IdentityServerAuthenticationModel>(identityServerSection);
            identityServerSection.Bind(identityServerSettings);

            services.AddScoped(typeof(IAuthorizationHandler), typeof(SolidUserRequirementHandler));
            services.AddScoped(typeof(IAuthorizationHandler), typeof(RoleRequirementHandler));

            // Add jwt validation.
            services.AddAuthentication(options =>
                {
                    options.DefaultScheme = IdentityServerAuthenticationDefaults.AuthenticationScheme;
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddIdentityServerAuthentication(options =>
                {
                    options.Authority = identityServerSettings.Authority;
                    options.ApiName = identityServerSettings.ApiName;
                    options.ApiSecret = identityServerSettings.ApiSecret;
                    options.RequireHttpsMetadata = identityServerSettings.RequireHttpsMetaData;
                    options.SaveToken = identityServerSettings.SaveToken;
                    options.SupportedTokens = identityServerSettings.SupportedToken;
                });

            services.AddAuthorization(options =>
            {
                options.AddPolicy(PolicyConstants.IsAdmin, policy =>
                    policy.Requirements.Add(new RoleRequirement(RoleConstants.Admin)));
            });

#if DEBUG
            IdentityModelEventSource.ShowPII = true;
#endif
        }

        #endregion
    }
}