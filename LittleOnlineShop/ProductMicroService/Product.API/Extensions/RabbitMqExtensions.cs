using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using ProductApi.Constants;
using SharedInfrastructure.Models;
using SharedInfrastructure.Services.Implementations;
using SharedInfrastructure.Services.Interfaces;

namespace ProductApi.Extensions
{
    public static class RabbitMqExtensions
    {
        #region Methods

        /// <summary>
        /// Add rabbit mq setup.
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        public static void AddRabbitMqSetup(this IServiceCollection services, IConfiguration configuration)
        {
            var rabbitMqConfigurationSection = configuration.GetSection(ConfigurationSectionKeyConstants.RabbitMq);
            var rabbitMqOption = new RabbitMqOption();
            rabbitMqConfigurationSection.Bind(rabbitMqOption);

            // Add rabbit mq option.
            services.AddSingleton(rabbitMqOption);
            
            // Register message queue manager service.
            var rabbitMqManagerService = new RabbitMqManagerService();
            services.AddSingleton<IMqManagerService>(rabbitMqManagerService);
        }

        #endregion
    }
}