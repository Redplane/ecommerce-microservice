﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Driver;
using ProductApi.Constants;
using ProductDomain.Models.Entities;

namespace ProductApi.Extensions
{
    public static class DatabaseSetup
    {
        #region Methods

        /// <summary>
        ///     Setup product micro-service database.
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        public static void AddProductMicroServiceDatabase(this IServiceCollection services,
            IConfiguration configuration)
        {
            // Setup database client.
            var dbConnectionString =
                configuration.GetConnectionString(DatabaseConnectionStringConstants.ProductDatabase);
            services.AddScoped<IMongoClient>(options => new MongoClient(new MongoUrl(dbConnectionString)));
            services.AddScoped(options =>
            {
                var dbClient = options.GetService<IMongoClient>();
                return dbClient.GetDatabase(DatabaseNameConstants.MainDatabase);
            });

            // Make mongodb save object with camel-cased naming convention.
            var conventionPack = new ConventionPack {new CamelCaseElementNameConvention()};
            ConventionRegistry.Register("camelCase", conventionPack, t => true);


            AddCategoriesCollection(services);
            AddProductsCollection(services);
            AddProductBasketsCollection(services);
        }

        /// <summary>
        ///     Add categories collection.
        /// </summary>
        private static void AddCategoriesCollection(IServiceCollection services)
        {
            // Add users collection.
            services.AddScoped(options =>
            {
                // Get db client.
                var database = options.GetService<IMongoDatabase>();
                return database.GetCollection<Category>(CollectionNameConstants.Category);
            });

            BsonClassMap.RegisterClassMap<Category>(options =>
            {
                options.AutoMap();
                options.MapCreator(x => new Category(x.Id));
                options.SetIgnoreExtraElements(true);
            });
        }

        /// <summary>
        ///     Add products collection.
        /// </summary>
        private static void AddProductsCollection(IServiceCollection services)
        {
            // Add users collection.
            services.AddScoped(options =>
            {
                // Get db client.
                var database = options.GetService<IMongoDatabase>();
                return database.GetCollection<Product>(CollectionNameConstants.Product);
            });

            BsonClassMap.RegisterClassMap<Product>(options =>
            {
                options.AutoMap();
                options.MapCreator(x => new Product(x.Id));
                options.SetIgnoreExtraElements(true);
            });
        }

        /// <summary>
        ///     Add products collection.
        /// </summary>
        private static void AddProductBasketsCollection(IServiceCollection services)
        {
            // Add users collection.
            services.AddScoped(options =>
            {
                // Get db client.
                var database = options.GetService<IMongoDatabase>();
                return database.GetCollection<ProductBasket>(CollectionNameConstants.ProductBasket);
            });

            BsonClassMap.RegisterClassMap<ProductBasket>(options =>
            {
                options.AutoMap();
                options.MapCreator(x => new ProductBasket(x.Id, x.CustomerId));
                options.SetIgnoreExtraElements(true);
            });
        }

        #endregion
    }
}