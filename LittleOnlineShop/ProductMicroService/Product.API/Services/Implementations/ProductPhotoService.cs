﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Imgur.API.Authentication.Impl;
using Imgur.API.Endpoints.Impl;
using Imgur.API.Models;
using Microsoft.AspNetCore.Http;
using ProductApi.Services.Interfaces;
using SkiaSharp;

namespace ProductApi.Services.Implementations
{
    public class ProductPhotoService : IProductPhotoService
    {
        #region Properties

        private readonly ImgurClient _imageClient;

        #endregion

        #region Constructors

        public ProductPhotoService(ImgurClient imageClient)
        {
            _imageClient = imageClient;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="bytes"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task<string> UploadAsync(byte[] bytes,
            CancellationToken cancellationToken = default)
        {
            var imageEndpoint = new ImageEndpoint(_imageClient);
            var uploadImageResult = await imageEndpoint.UploadImageBinaryAsync(bytes);
            return uploadImageResult.Link;
        }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="files"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task<string[]> UploadAsync(IFormFile[] files,
            CancellationToken cancellationToken = default)
        {
            var bytesList = new List<byte[]>();
            var loadBinariesTasks = new List<Task<byte[]>>();

            foreach (var file in files)
            {
                var loadBinariesTask = Task.Run(() =>
                {
                    using (var memoryStream = new MemoryStream())
                    {
                        file.CopyTo(memoryStream);
                        return memoryStream.ToArray();
                    }
                }, cancellationToken);

                loadBinariesTasks.Add(loadBinariesTask);
            }

            // Wait for tasks to be completed.
            var loadBinariesResults = await Task.WhenAll(loadBinariesTasks);

            // Run upload tasks.
            var uploadImageTasks = new List<Task<IImage>>();
            var imageEndpoint = new ImageEndpoint(_imageClient);

            foreach (var loadBinariesResult in loadBinariesResults)
            {
                var uploadImageTask = imageEndpoint.UploadImageBinaryAsync(loadBinariesResult);
                uploadImageTasks.Add(uploadImageTask);
            }

            var uploadImageResults = await Task.WhenAll(uploadImageTasks);
            return uploadImageResults.Select(x => x.Link).ToArray();
        }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <returns></returns>
        public Task<string> UploadAsync(SKImage skImage, int maxWidth, int maxHeight,
            CancellationToken cancellationToken = default)
        {
            using (var scaledImage = ResizeImage(skImage, maxWidth, maxHeight))
            using (var scaledImageData = scaledImage.Encode(SKEncodedImageFormat.Png, 75))
            {
                return UploadAsync(scaledImageData.ToArray(),
                    cancellationToken);
            }
        }

        /// <summary>
        ///     Upload file to an azure container.
        /// </summary>
        /// <param name="bytes"></param>
        /// <param name="containerKey"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        protected virtual async Task<string> UploadAsync(byte[] bytes, string containerKey,
            CancellationToken cancellationToken = default)
        {
            // Container hasn't been configured.
            if (string.IsNullOrWhiteSpace(containerKey))
                throw new Exception("Container not found");

            if (string.IsNullOrWhiteSpace(containerKey))
                throw new Exception("Container not found");


            // Get the reference to the block blob from the container
            var fileName = $"{Guid.NewGuid():D}.png";

            var imageEndpoint = new ImageEndpoint(_imageClient);
            var uploadImageResult = await imageEndpoint.UploadImageBinaryAsync(bytes, containerKey, fileName, fileName);
            return uploadImageResult.Link;
        }

        /// <summary>
        ///     Resize image but keeping the ratio.
        /// </summary>
        /// <returns></returns>
        protected virtual SKImage ResizeImage(SKImage skImage, int maxWidth, int maxHeight)
        {
            var width = maxWidth;
            var height = maxHeight;

            // Calculate the image actual ratio.
            var actualRatio = (double) skImage.Width / skImage.Height;

            // Photo is in horizontal position. Width should be calculated by height.
            if (actualRatio >= 1)
                width = (int) (height * actualRatio);
            else
                height = (int) (height * actualRatio);

            using (var skBitmap = SKBitmap.FromImage(skImage))
            using (var scaledSkBitmap = skBitmap.Resize(new SKImageInfo(width, height), SKFilterQuality.Medium))
            {
                return SKImage.FromBitmap(scaledSkBitmap);
            }
        }

        #endregion
    }
}