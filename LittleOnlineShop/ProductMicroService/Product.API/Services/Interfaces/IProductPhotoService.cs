﻿using System.Threading;
using System.Threading.Tasks;
using SharedMicroService.Services.Interfaces;
using SkiaSharp;

namespace ProductApi.Services.Interfaces
{
    public interface IProductPhotoService : IPhotoService
    {
        #region Methods

        /// <summary>
        ///     Upload photo asynchronously with resize option.
        /// </summary>
        /// <returns></returns>
        Task<string> UploadAsync(SKImage skImage, int maxWidth, int maxHeight,
            CancellationToken cancellationToken = default);

        #endregion
    }
}