using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using ProductApi.Cqrs.Notifications;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using SharedInfrastructure.Constants;
using SharedInfrastructure.Enums;
using SharedInfrastructure.Models;
using SharedInfrastructure.Models.MqMessages;
using SharedInfrastructure.Services.Interfaces;

namespace ProductApi.HostedServices
{
    public class MqHostedService : IHostedService
    {
        #region Properties

        private readonly IServiceScopeFactory _serviceScopeFactory;

        /// <summary>
        /// Instance for writing logs.
        /// </summary>
        private readonly ILogger<MqHostedService> _logger;

        /// <summary>
        /// Channels which have been added into the system.
        /// </summary>
        private LinkedList<RabbitMqChannel> _addedChannels;

        /// <summary>
        /// Connection factory.
        /// </summary>
        private ConnectionFactory _connectionFactory;

        /// <summary>
        /// RabbitMQ connection.
        /// </summary>
        private IConnection _connection;
        
        #endregion
        
        #region Constructor

        public MqHostedService(IServiceScopeFactory serviceScopeFactory, ILogger<MqHostedService> logger)
        {
            _serviceScopeFactory = serviceScopeFactory;
            _addedChannels = new LinkedList<RabbitMqChannel>();
            _logger = logger;

        }

        #endregion
        
        #region Methods
        
        /// <summary>
        /// Called when hosted service started.
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public virtual Task StartAsync(CancellationToken cancellationToken)
        {
            using (var serviceScope = _serviceScopeFactory.CreateScope())
            {
                var serviceProvider = serviceScope.ServiceProvider;
                var rabbitMqOption = serviceProvider.GetService<RabbitMqOption>();
                var mqManagerService = serviceProvider.GetService<IMqManagerService>();
                
                // Initialize rabbit mq connection factory.
                _connectionFactory = new ConnectionFactory();
                _connectionFactory.Uri = new Uri(rabbitMqOption.Endpoint);
                _connection = _connectionFactory.CreateConnection();

                foreach (var channelSetting in rabbitMqOption.Channels)
                {
                    var channel = _connection.CreateModel();
                    channel.QueueDeclare(channelSetting.Name, channelSetting.Durable, channelSetting.Exclusive, channelSetting.AutoDelete, null);

                    // Add the queue to registered list.
                    var mqChannel = new RabbitMqChannel(channel, channelSetting);

                    if (channelSetting.Kind == MqChannelKinds.Incoming)
                    {
                        // Rabbit mq consumer.
                        var basicConsumer = new EventingBasicConsumer(channel);

                        // Register message receive event.
                        basicConsumer.Received += async (model, args) =>
                        {
                            var body = args.Body;
                            var message = Encoding.UTF8.GetString(body);
                            var mqMessageBase = JsonConvert.DeserializeObject<MqMessageBase>(message);
                            await HandleRabbitMqIncomingMessage(mqChannel, mqMessageBase, message);

                            channel.BasicAck(args.DeliveryTag, true);
                        };

                        // Start consuming the queue.
                        channel.BasicConsume(channelSetting.Name, false, basicConsumer);
                    }
                    
                    _addedChannels.AddLast(mqChannel);
                    mqManagerService.AddMqChannelAsync(mqChannel, cancellationToken)
                        .Wait(cancellationToken);
                }
            }

            return Task.CompletedTask;
        }

        /// <summary>
        /// Called when hosted service stops.
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public virtual Task StopAsync(CancellationToken cancellationToken)
        {
            _connection?.Dispose();
            foreach (var addedChannel in _addedChannels)
            {
                // Dispose the added channel.
                addedChannel.Channel.Dispose();
            }

            return Task.CompletedTask;
        }

        
        /// <summary>
        /// Handle incoming message which sent through rabbit mq hosting.
        /// </summary>
        /// <param name="mqChannel"></param>
        /// <param name="mqMessageBase"></param>
        /// <param name="message"></param>
        protected virtual async Task<bool> HandleRabbitMqIncomingMessage(RabbitMqChannel mqChannel, MqMessageBase mqMessageBase, string message)
        {
            using (var serviceScope = _serviceScopeFactory.CreateScope())
            {
                var serviceProvider = serviceScope.ServiceProvider;
                var mediator = serviceProvider.GetService<IMediator>();
                var logger = serviceProvider.GetService<ILogger<MqHostedService>>();
                
                Task messagePublishingTask = null;
                
                if (mqMessageBase.EventName == MqEventNameConstant.SuccessfulBalanceDeduction)
                {
                    var successfulBalanceDeductionMqMessage = JsonConvert.DeserializeObject<SuccessfulBalanceDeductionMqMessage>(message);
                    var notification = new SuccessfulBalanceDeductionNotification(successfulBalanceDeductionMqMessage);
                    messagePublishingTask = mediator.Publish(notification);
                }

                if (mqMessageBase.EventName == MqEventNameConstant.FailedBalanceDeduction)
                {
                    var failedBalanceDeductionMessage =
                        JsonConvert.DeserializeObject<FailedBalanceDeductionMqMessage>(message);
                    var failedBalanceDeductionNotification = new FailedBalanceDeductionNotification(failedBalanceDeductionMessage);
                    messagePublishingTask = mediator.Publish(failedBalanceDeductionNotification);
                }
                
                if (messagePublishingTask == null)
                    return false;

                await messagePublishingTask;
                return messagePublishingTask.IsCompletedSuccessfully;
            }

        }
        
        #endregion
    }
}