﻿namespace ProductApi.Models
{
    // ReSharper disable once IdentifierTypo
    public class ImgurClientSettings
    {
        #region Properties

        public string ClientId { get; set; }

        public string ClientSecret { get; set; }

        #endregion
    }
}