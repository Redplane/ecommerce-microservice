﻿using AutoMapper;
using FluentValidation.AspNetCore;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using ProductApi.Extensions;
using ProductInfrastructure.MapperProfiles;
using SharedMicroService.AuthenticationHandlers.Requirements;
using SharedMicroService.Extensions;

namespace ProductApi
{
    public class Startup
    {
        #region Properties

        public IConfiguration Configuration { get; }

        #endregion

        #region Methods

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddOptions();

            // Setup rabbit mq.
            services.AddRabbitMqSetup(Configuration);
            
            // Ioc Setup
            services.AddIoc(Configuration);

            // Run authentication setup.
            services.AddAuthenticationSetup(Configuration);

            // Add product micro-service database.
            services.AddProductMicroServiceDatabase(Configuration);

            // Add hosted services.
            services.AddHostedServiceSetup();
            
            // Auto Mapper Config
            services.AddAutoMapper(options => options.AddProfile(typeof(DefaultProfile)),
                typeof(DefaultProfile).Assembly);
            
            services
                .AddControllers(options =>
                {
                    ////only allow authenticated users
                    var policy = new AuthorizationPolicyBuilder()
                        .RequireAuthenticatedUser()
                        .AddAuthenticationSchemes(IdentityServerAuthenticationDefaults.AuthenticationScheme)
#if !ALLOW_ANONYMOUS
                        .AddRequirements(new SolidUserRequirement())
#endif
                        .Build();

                    options.Filters.Add(new AuthorizeFilter(policy));
                })
                .AddFluentValidation(options =>
                    options.RegisterValidatorsFromAssembly(typeof(Startup).Assembly))
                .AddNewtonsoftJson(options =>
                {
                    var camelCasePropertyNamesContractResolver = new CamelCasePropertyNamesContractResolver();
                    options.SerializerSettings.ContractResolver = camelCasePropertyNamesContractResolver;
                    options.SerializerSettings.DefaultValueHandling = DefaultValueHandling.Ignore;
                })
                .SetCompatibilityVersion(CompatibilityVersion.Version_3_0);
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment()) app.UseDeveloperExceptionPage();
            
            // Use custom exception handler.
            app.UseApiExceptionHandler();
            
            // Seed the database.
            app.AddInitialCategories()
                .AddInitialProducts();
            
            // Use routing middleware.
            app.UseRouting();
            
            // Use endpoint middleware.
            app.UseEndpoints(options => { options.MapControllers(); });
    }

        #endregion
    }
}