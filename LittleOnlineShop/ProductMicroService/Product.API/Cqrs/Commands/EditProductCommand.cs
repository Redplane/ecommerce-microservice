﻿using System;
using System.Collections.Generic;
using MediatR;
using ProductDomain.Models;
using ProductDomain.Models.Entities;
using SharedDomain.Enums;
using SharedInfrastructure.Models;

namespace ProductApi.Cqrs.Commands
{
    public class EditProductCommand : IRequest<Product>
    {
        #region Properties

        public Guid Id { get; set; }

        public EditableFieldValueObject<HashSet<Guid>> CategoryIds { get; set; }

        public EditableFieldValueObject<string> Sku { get; set; }

        public EditableFieldValueObject<string> Name { get; set; }

        public EditableFieldValueObject<string> Description { get; set; }

        public EditableFieldValueObject<ProductSpecificationValueObject[]> Specifications { get; set; }

        public EditableFieldValueObject<double> Price { get; set; }

        public EditableFieldValueObject<double> Quantity { get; set; }

        public EditableFieldValueObject<MasterItemAvailabilities> Availability { get; set; }

        #endregion
    }
}