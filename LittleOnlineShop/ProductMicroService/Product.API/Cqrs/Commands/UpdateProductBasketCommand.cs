﻿using System;
using MediatR;
using ProductDomain.Models;

namespace ProductApi.Cqrs.Commands
{
    public class UpdateProductBasketCommand : IRequest<ProductBasketItemValueObject>
    {
        #region Properties

        /// <summary>
        ///     Id of product to reserve.
        /// </summary>
        public Guid ProductId { get; set; }

        /// <summary>
        ///     Quantity of product to reserve.
        /// </summary>
        public decimal Quantity { get; set; }

        #endregion
    }
}