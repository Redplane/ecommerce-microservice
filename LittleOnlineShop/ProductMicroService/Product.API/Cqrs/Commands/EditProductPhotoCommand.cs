﻿using System;
using MediatR;
using Microsoft.AspNetCore.Http;
using ProductApi.Extensions;
using ProductDomain.Models.Entities;
using SkiaSharp;

namespace ProductApi.Cqrs.Commands
{
    public class EditProductPhotoCommand : IRequest<Product>
    {
        #region Properties

        /// <summary>
        ///     Decoded image.
        /// </summary>
        private SKImage _decodedPhoto;

        #endregion

        #region Methods

        /// <summary>
        ///     Find decoded photo asynchronously from uploaded one.
        /// </summary>
        /// <returns></returns>
        public virtual SKImage LoadDecodedPhoto()
        {
            if (_decodedPhoto != null)
                return _decodedPhoto;

            _decodedPhoto = Photo?.ToImage();
            return _decodedPhoto;
        }

        #endregion

        #region Accessors

        /// <summary>
        ///     Id of product.
        /// </summary>
        public Guid ProductId { get; set; }

        /// <summary>
        ///     Id of product photo.
        /// </summary>
        public Guid PhotoId { get; set; }

        /// <summary>
        ///     Product photo. Ratio must be 4:3
        /// </summary>
        public IFormFile Photo { get; set; }

        #endregion

        #region Constructor

        #endregion
    }
}