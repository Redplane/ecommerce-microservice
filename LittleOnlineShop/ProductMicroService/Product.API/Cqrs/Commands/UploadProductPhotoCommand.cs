﻿using System;
using MediatR;
using Microsoft.AspNetCore.Http;
using ProductApi.Extensions;
using ProductDomain.Models.Entities;
using SkiaSharp;

namespace ProductApi.Cqrs.Commands
{
    public class UploadProductPhotoCommand : IRequest<Photo>
    {
        #region Methods

        /// <summary>
        ///     Find decoded photo asynchronously from uploaded one.
        /// </summary>
        /// <returns></returns>
        public virtual SKImage LoadDecodedPhoto()
        {
            if (_decodedPhoto != null)
                return _decodedPhoto;

            _decodedPhoto = _binaryImage.ToImage();
            return _decodedPhoto;
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Binary data that is submitted from client.
        /// </summary>
        private IFormFile _binaryImage;

        /// <summary>
        ///     Decoded image.
        /// </summary>
        private SKImage _decodedPhoto;

        #endregion

        #region Accessors

        /// <summary>
        ///     Id of product to upload image to.
        /// </summary>
        public Guid ProductId { get; set; }

        /// <summary>
        ///     Product photo. Ratio must be 4:3
        /// </summary>
        public IFormFile Photo
        {
            set
            {
                if (value == null)
                    return;

                // Bind binary stream property.
                _binaryImage = value;
            }
            get => _binaryImage;
        }

        #endregion
    }
}