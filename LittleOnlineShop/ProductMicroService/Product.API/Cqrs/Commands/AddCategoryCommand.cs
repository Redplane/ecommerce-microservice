﻿using MediatR;
using ProductDomain.Models.Entities;

namespace ProductApi.Cqrs.Commands
{
    public class AddCategoryCommand : IRequest<Category>
    {
        #region Properties

        public string Name { get; set; }

        public string Description { get; set; }

        #endregion
    }
}