﻿using System;
using System.Collections.Generic;
using MediatR;
using ProductDomain.Models;
using ProductDomain.Models.Entities;
using SharedDomain.Enums;

namespace ProductApi.Cqrs.Commands
{
    public class AddProductCommand : IRequest<Product>
    {
        #region Properties

        /// <summary>
        ///     Category ids that product belongs to.
        /// </summary>
        public HashSet<Guid> CategoryIds { get; set; }

        /// <summary>
        ///     Stock keeping unit.
        /// </summary>
        public string Sku { get; set; }

        /// <summary>
        ///     Product name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        ///     Product description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        ///     Product specifications.
        /// </summary>
        public HashSet<ProductSpecificationValueObject> Specifications { get; set; }

        /// <summary>
        ///     Product price.
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        ///     Whether product is available in the system or not.
        /// </summary>
        public MasterItemAvailabilities Availability { get; set; }

        #endregion
    }
}