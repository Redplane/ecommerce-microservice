﻿using System;
using MediatR;
using ProductDomain.Models.Entities;

namespace ProductApi.Cqrs.Commands
{
    public class DeleteProductPhotoCommand : IRequest<Product>
    {
        #region Properties

        /// <summary>
        ///     Id of product whose image should be deleted.
        /// </summary>
        public Guid ProductId { get; set; }

        /// <summary>
        ///     Id of photo which should be deleted.
        /// </summary>
        public Guid? PhotoId { get; set; }

        #endregion
    }
}