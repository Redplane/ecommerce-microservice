﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using ProductApi.Cqrs.Queries;
using ProductDomain.Models.Entities;
using ProductInfrastructure.Factories.Interfaces;
using SharedDomain.Constants;
using SharedDomain.Enums;
using SharedDomain.Models;
using SharedMicroService.Extensions;

namespace ProductApi.Cqrs.QueryHandlers
{
    public class FindProductsQueryHandler : IRequestHandler<FindProductsQuery, SearchResultValueObject<Product>>
    {
        #region Constructor

        public FindProductsQueryHandler(IProductFactory productFactory, IHttpContextAccessor httpContextAccessor)
        {
            _productFactory = productFactory;
            _httpContext = httpContextAccessor.HttpContext;
        }

        #endregion

        #region Methods

        public virtual async Task<SearchResultValueObject<Product>> Handle(FindProductsQuery query,
            CancellationToken cancellationToken)
        {
            // Get requester profile.
            var requesterProfile = _httpContext.FindProfile();

            // Product availability.
            var availability = query.Availability;

            // Ordinary users can only see available product only.
            if (requesterProfile == null || !requesterProfile.IsInRole(RoleConstants.Admin))
                availability = MasterItemAvailabilities.Available;

            return await _productFactory.FindProductsAsync(null, query.CategoryIds, availability, query.Price,
                query.Sort, query.Pager, cancellationToken);
        }

        #endregion

        #region Properties

        private readonly IProductFactory _productFactory;

        private readonly HttpContext _httpContext;

        #endregion
    }
}