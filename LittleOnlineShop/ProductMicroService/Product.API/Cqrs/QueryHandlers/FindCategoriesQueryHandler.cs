﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using ProductApi.Cqrs.Queries;
using ProductDomain.Models.Entities;
using ProductInfrastructure.Factories.Interfaces;
using SharedDomain.Models;

namespace ProductApi.Cqrs.QueryHandlers
{
    public class FindCategoriesQueryHandler : IRequestHandler<FindCategoriesQuery, SearchResultValueObject<Category>>
    {
        #region Constructor

        public FindCategoriesQueryHandler(ICategoryFactory categoryFactory, IHttpContextAccessor httpContextAccessor)
        {
            _categoryFactory = categoryFactory;
            _httpContext = httpContextAccessor.HttpContext;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual Task<SearchResultValueObject<Category>> Handle(FindCategoriesQuery request,
            CancellationToken cancellationToken)
        {
            return _categoryFactory.FindCategoriesAsync(request.Ids, request.Availability, request.Sort, request.Pager,
                cancellationToken);
        }

        #endregion

        #region Properties

        private readonly ICategoryFactory _categoryFactory;

        private readonly HttpContext _httpContext;

        #endregion
    }
}