﻿using System;
using System.Collections.Generic;
using MediatR;
using ProductDomain.Models.Entities;
using SharedDomain.Enums;
using SharedDomain.Models;
using SharedInfrastructure.Enums.SortProperties;
using SharedInfrastructure.Models;

namespace ProductApi.Cqrs.Queries
{
    public class FindCategoriesQuery : IRequest<SearchResultValueObject<Category>>
    {
        #region Properties

        /// <summary>
        ///     Category indexes.
        /// </summary>
        public HashSet<Guid> Ids { get; set; }

        /// <summary>
        ///     Availability of category.
        /// </summary>
        public MasterItemAvailabilities? Availability { get; set; }

        /// <summary>
        ///     Sort property & direction.
        /// </summary>
        public SortValueObject<CategorySortProperties> Sort { get; set; }

        /// <summary>
        ///     Pager information.
        /// </summary>
        public PagerValueObject Pager { get; set; }

        #endregion
    }
}