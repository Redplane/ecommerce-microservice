﻿using System;
using System.Collections.Generic;
using MediatR;
using ProductDomain.Models.Entities;
using ProductInfrastructure.Enums.SortedProperties;
using ProductInfrastructure.Models;
using SharedDomain.Enums;
using SharedDomain.Models;
using SharedInfrastructure.Models;

namespace ProductApi.Cqrs.Queries
{
    public class FindProductsQuery : IRequest<SearchResultValueObject<Product>>
    {
        #region Properties

        /// <summary>
        ///     Id of categories that product belongs to.
        /// </summary>
        public HashSet<Guid> CategoryIds { get; set; }

        /// <summary>
        ///     Product availabilities.
        /// </summary>
        public MasterItemAvailabilities? Availability { get; set; }

        /// <summary>
        ///     Product price.
        /// </summary>
        public NumericRangeComparisionModel<double> Price { get; set; }

        /// <summary>
        ///     Product sorted property.
        /// </summary>
        public SortValueObject<ProductSortedProperties> Sort { get; set; }

        /// <summary>
        ///     Pager information.
        /// </summary>
        public PagerValueObject Pager { get; set; }

        #endregion
    }
}