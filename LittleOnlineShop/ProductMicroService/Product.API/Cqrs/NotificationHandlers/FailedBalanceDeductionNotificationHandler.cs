using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using ProductApi.Cqrs.Notifications;
using ProductInfrastructure.Factories.Interfaces;
using SharedInfrastructure.Constants;
using SharedInfrastructure.Models.MqMessages;
using SharedInfrastructure.Services.Interfaces;

namespace ProductApi.Cqrs.NotificationHandlers
{
    public class FailedBalanceDeductionNotificationHandler : INotificationHandler<FailedBalanceDeductionNotification>
    {
        #region Properties

        private readonly IProductFactory _productFactory;

        private readonly IMqManagerService _mqManagerService;

        private readonly ILogger<FailedBalanceDeductionNotificationHandler> _logger;
        
        #endregion
        
        #region Constructor

        public FailedBalanceDeductionNotificationHandler(IProductFactory productFactory, 
            IMqManagerService mqManagerService,
            ILogger<FailedBalanceDeductionNotificationHandler> logger)
        {
            _productFactory = productFactory;
            _mqManagerService = mqManagerService;
            _logger = logger;
        }

        #endregion

        #region Methods
        
        public async Task Handle(FailedBalanceDeductionNotification notification, CancellationToken cancellationToken)
        {
            // Cancel product basket reservation.
            var cancelledProductBasket = await _productFactory.CancelCheckedOutProductBasketAsync(notification.CustomerId, notification.ProductBasketId,
                cancellationToken);
         
            // Broadcast a message about the reservation cancellation.
            var totalMoney = cancelledProductBasket.Items?.Sum(item => item.TotalMoney) ?? 0;
            var productBasketReservationCancellationMessage = new ProductBasketReservationCancellationMqMessage(
                cancelledProductBasket.Id, cancelledProductBasket.CustomerId, totalMoney);
            
            await _mqManagerService.PublishMqMessageAsync(MqChannelNameConstant.Product,
                productBasketReservationCancellationMessage, cancellationToken);
            
            _logger.LogInformation($"Product {cancelledProductBasket.Id} has been cancelled successfully.");
        }
        
        #endregion
    }
}