using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using ProductApi.Cqrs.Notifications;
using ProductInfrastructure.Factories.Interfaces;

namespace ProductApi.Cqrs.NotificationHandlers
{
    public class SuccessfulBalanceDeductionNotificationHandler : INotificationHandler<SuccessfulBalanceDeductionNotification>
    {
        #region Properties

        private readonly IProductFactory _productFactory;

        private readonly ILogger<SuccessfulBalanceDeductionNotificationHandler> _logger;
        
        #endregion
        
        #region Constructor

        public SuccessfulBalanceDeductionNotificationHandler(IProductFactory productFactory, ILogger<SuccessfulBalanceDeductionNotificationHandler> logger)
        {
            _productFactory = productFactory;
            _logger = logger;
        }
        
        #endregion
        
        #region Methods
        
        /// <summary>
        /// Handle the successful balance deduction notification from external service.
        /// </summary>
        /// <param name="notification"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task Handle(SuccessfulBalanceDeductionNotification notification, CancellationToken cancellationToken)
        {
            var productBasket = await _productFactory.ReserveCheckedOutProductBasketAsync(notification.CustomerId, notification.ProductBasketId, cancellationToken);
            if (productBasket == null)
                return;
            
            _logger.LogInformation($"Product {productBasket.Id} has been reserved successfully.");
        }
        
        #endregion
    }
}