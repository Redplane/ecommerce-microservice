﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using ProductApi.Constants;
using ProductApi.Cqrs.Commands;
using ProductApi.Services.Interfaces;
using ProductDomain.Models.Entities;
using ProductInfrastructure.Factories.Interfaces;
using SharedInfrastructure.Models;

namespace ProductApi.Cqrs.CommandHandlers
{
    public class EditProductPhotoCommandHandler : IRequestHandler<EditProductPhotoCommand, Product>
    {
        #region Constructor

        public EditProductPhotoCommandHandler(IProductPhotoService productPhotoService, IProductFactory productFactory)
        {
            _productPhotoService = productPhotoService;
            _productFactory = productFactory;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Handle product photo asynchronously.
        /// </summary>
        /// <param name="command"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task<Product> Handle(EditProductPhotoCommand command, CancellationToken cancellationToken)
        {
            // Upload photo to hosting.
            var decodedPhoto = command.LoadDecodedPhoto();
            var uploadedPhoto = await _productPhotoService.UploadAsync(decodedPhoto,
                ValidatorValueConstants.MaxProductImageSize.Width,
                ValidatorValueConstants.MaxProductImageSize.Height,
                cancellationToken);

            // Edit product photo.
            var product = await _productFactory.EditProductPhotoAsync(command.ProductId, command.PhotoId,
                new EditableFieldValueObject<string>(uploadedPhoto, true), cancellationToken);

            return product;
        }

        #endregion

        #region Properties

        private readonly IProductPhotoService _productPhotoService;

        private readonly IProductFactory _productFactory;

        #endregion
    }
}