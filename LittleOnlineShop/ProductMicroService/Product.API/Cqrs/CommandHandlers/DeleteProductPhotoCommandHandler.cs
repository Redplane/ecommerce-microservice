﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using ProductApi.Cqrs.Commands;
using ProductDomain.Models.Entities;
using ProductInfrastructure.Factories.Interfaces;

namespace ProductApi.Cqrs.CommandHandlers
{
    public class DeleteProductPhotoCommandHandler : IRequestHandler<DeleteProductPhotoCommand, Product>
    {
        #region Properties

        private readonly IProductFactory _productFactory;

        #endregion

        #region Constructor

        public DeleteProductPhotoCommandHandler(IProductFactory productFactory)
        {
            _productFactory = productFactory;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Handle product photo command.
        /// </summary>
        /// <param name="command"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task<Product> Handle(DeleteProductPhotoCommand command,
            CancellationToken cancellationToken)
        {
            var product =
                await _productFactory.DeleteProductPhotoAsync(command.ProductId, command.PhotoId, cancellationToken);
            return product;
        }

        #endregion
    }
}