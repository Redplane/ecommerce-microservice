using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using ProductApi.Cqrs.Commands;
using ProductDomain.Models.Entities;
using ProductInfrastructure.Constants;
using ProductInfrastructure.Factories.Interfaces;
using SharedInfrastructure.Constants;
using SharedInfrastructure.Exceptions;
using SharedInfrastructure.Models.MqMessages;
using SharedInfrastructure.Services.Interfaces;
using SharedMicroService.Extensions;

namespace ProductApi.Cqrs.CommandHandlers
{
    public class CheckoutPendingProductBasketCommandHandler : IRequestHandler<CheckoutPendingProductBasketCommand, ProductBasket>
    {
        #region Properties

        private readonly IProductFactory _productFactory;

        private readonly IMqManagerService _mqManagerService;
        
        private readonly HttpContext _httpContext;

        #endregion
        
        #region Constructor

        public CheckoutPendingProductBasketCommandHandler(IProductFactory productFactory, 
            IMqManagerService mqManagerService,
            IHttpContextAccessor httpContextAccessor)
        {
            _productFactory = productFactory;
            _mqManagerService = mqManagerService;
            _httpContext = httpContextAccessor.HttpContext;
        }

        #endregion
        
        #region Methods
        
        /// <summary>
        /// Handle pending product basket checkout.
        /// </summary>
        /// <param name="command"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public virtual async Task<ProductBasket> Handle(CheckoutPendingProductBasketCommand command, CancellationToken cancellationToken)
        {
            // Find the current person who is making request.
            var commandRequester = _httpContext.FindProfile(true);
            if (commandRequester == null)
                throw new HttpResponseException(HttpStatusCode.Unauthorized, HttpResponseMessageCodeConstants.InvalidGrant);
            
            // Do pending product basket checkout operation.
            var checkedOutProductBasket = await _productFactory.CheckoutProductBasketAsync(commandRequester.Id, cancellationToken);
            if (checkedOutProductBasket == null)
                throw new HttpResponseException(HttpStatusCode.NotFound, HttpResponseMessageCodeConstants.PendingProductBasketNotFound);

            // After product basket has been checked out. Broadcast a message to external services.
            var totalMoney = checkedOutProductBasket.Items.Sum(x => x.TotalMoney);
            var productBasketCheckedOutMqMessage =
                new ProductBasketCheckedOutMqMessage(checkedOutProductBasket.CustomerId, checkedOutProductBasket.Id,
                    totalMoney);
            
            await _mqManagerService.PublishMqMessageAsync(MqChannelNameConstant.Product, productBasketCheckedOutMqMessage,
                cancellationToken);

            return checkedOutProductBasket;
        }
        
        #endregion
    }
}