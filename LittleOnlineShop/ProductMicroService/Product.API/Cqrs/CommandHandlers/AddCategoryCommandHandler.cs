﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using ProductApi.Cqrs.Commands;
using ProductDomain.Models.Entities;
using ProductInfrastructure.Factories.Interfaces;

namespace ProductApi.Cqrs.CommandHandlers
{
    public class AddCategoryCommandHandler : IRequestHandler<AddCategoryCommand, Category>
    {
        #region Properties

        private readonly ICategoryFactory _categoryFactory;

        #endregion

        #region Constructor

        public AddCategoryCommandHandler(ICategoryFactory categoryFactory)
        {
            _categoryFactory = categoryFactory;
        }

        #endregion

        #region Methods

        public virtual Task<Category> Handle(AddCategoryCommand command, CancellationToken cancellationToken)
        {
            return _categoryFactory.AddCategoryAsync(command.Name, command.Description, cancellationToken);
        }

        #endregion
    }
}