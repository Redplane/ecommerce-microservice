﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using ProductApi.Constants;
using ProductApi.Cqrs.Commands;
using ProductApi.Services.Interfaces;
using ProductDomain.Models.Entities;
using ProductInfrastructure.Factories.Interfaces;

namespace ProductApi.Cqrs.CommandHandlers
{
    public class UploadProductPhotoCommandHandler : IRequestHandler<UploadProductPhotoCommand, Photo>
    {
        #region Constructor

        public UploadProductPhotoCommandHandler(IProductPhotoService productPhotoService,
            IProductFactory productFactory)
        {
            _productPhotoService = productPhotoService;
            _productFactory = productFactory;
        }

        #endregion

        #region Methods

        public virtual async Task<Photo> Handle(UploadProductPhotoCommand command, CancellationToken cancellationToken)
        {
            // Upload product photo to image hosting.
            var decodedPhoto = command.LoadDecodedPhoto();
            var relativeUrl = await _productPhotoService.UploadAsync(decodedPhoto,
                ValidatorValueConstants.MaxProductImageSize.Width,
                ValidatorValueConstants.MaxProductImageSize.Height,
                cancellationToken);

            // Add product photo asynchronously.
            var photo = await _productFactory.AddProductPhotoAsync(command.ProductId, relativeUrl, cancellationToken);

            return photo;
        }

        #endregion

        #region Properties

        private readonly IProductPhotoService _productPhotoService;

        private readonly IProductFactory _productFactory;

        #endregion
    }
}