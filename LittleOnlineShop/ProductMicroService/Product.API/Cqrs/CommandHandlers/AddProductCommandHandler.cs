﻿using System.Net;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using ProductApi.Cqrs.Commands;
using ProductDomain.Models.Entities;
using ProductInfrastructure.Constants;
using ProductInfrastructure.Factories.Interfaces;
using SharedInfrastructure.Exceptions;

namespace ProductApi.Cqrs.CommandHandlers
{
    public class AddProductCommandHandler : IRequestHandler<AddProductCommand, Product>
    {
        #region Constructor

        public AddProductCommandHandler(IProductFactory productFactory, ICategoryFactory categoryFactory)
        {
            _productFactory = productFactory;
            _categoryFactory = categoryFactory;
        }

        #endregion

        #region Methods

        public virtual async Task<Product> Handle(AddProductCommand request, CancellationToken cancellationToken)
        {
            // Find the categories by ids.
            var categories = await _categoryFactory.FindCategoriesByIdsAsync(request.CategoryIds, cancellationToken);

            // Category not found.
            if (categories == null || categories.Count != request.CategoryIds.Count)
                throw new HttpResponseException(HttpStatusCode.NotFound,
                    HttpResponseMessageCodeConstants.AtLeastOneProductCategoryNotExists);

            return await _productFactory.AddProductAsync(request.CategoryIds, request.Sku, request.Name,
                request.Description,
                request.Specifications, request.Price, request.Availability, cancellationToken);
        }

        #endregion

        #region Properties

        private readonly IProductFactory _productFactory;

        private readonly ICategoryFactory _categoryFactory;

        #endregion
    }
}