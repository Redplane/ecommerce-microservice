﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using ProductApi.Cqrs.Commands;
using ProductDomain.Models;
using ProductInfrastructure.Constants;
using ProductInfrastructure.Factories.Interfaces;
using SharedDomain.Enums;
using SharedDomain.Models;
using SharedInfrastructure.Exceptions;
using SharedMicroService.Extensions;

namespace ProductApi.Cqrs.CommandHandlers
{
    public class
        AddProductIntoBasketCommandHandler : IRequestHandler<UpdateProductBasketCommand, ProductBasketItemValueObject>
    {
        #region Constructor

        public AddProductIntoBasketCommandHandler(IProductFactory productFactory,
            IHttpContextAccessor httpContextAccessor)
        {
            _productFactory = productFactory;
            _httpContext = httpContextAccessor.HttpContext;
        }

        #endregion

        #region Methods

        public async Task<ProductBasketItemValueObject> Handle(UpdateProductBasketCommand command,
            CancellationToken cancellationToken)
        {
            // Find the current user who is making the request.
            var requester = _httpContext.FindProfile(true);
            
            // Find the product using its id.
            var ids = new HashSet<Guid>();
            ids.Add(command.ProductId);

            // Find the product in the system.
            var products = await _productFactory.FindProductsAsync(ids, availabilities: MasterItemAvailabilities.Available,
                pager: new PagerValueObject(1, 1), cancellationToken: cancellationToken);

            // Find the first product in search result.
            var product = products?.Records?.FirstOrDefault();
            if (product == null)
                throw new HttpResponseException(HttpStatusCode.NotFound, HttpResponseMessageCodeConstants.ProductNotFound);

            var item = new ProductBasketItemValueObject(command.ProductId, command.Quantity, product.Price * command.Quantity);
            await _productFactory.UpdateProductBasketAsync(requester.Id, item, cancellationToken);
            return item;
        }

        #endregion

        #region Properties

        private readonly IProductFactory _productFactory;

        private readonly HttpContext _httpContext;

        #endregion
    }
}