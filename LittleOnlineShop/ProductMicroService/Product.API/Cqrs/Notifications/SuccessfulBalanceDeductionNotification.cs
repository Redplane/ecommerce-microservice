using System;
using MediatR;
using SharedInfrastructure.Models.MqMessages;

namespace ProductApi.Cqrs.Notifications
{
    public class SuccessfulBalanceDeductionNotification : SuccessfulBalanceDeductionMqMessage, INotification
    {
        #region Constructor
        
        public SuccessfulBalanceDeductionNotification(Guid productBasketId, Guid customerId) : base(productBasketId, customerId)
        {
        }

        public SuccessfulBalanceDeductionNotification(SuccessfulBalanceDeductionMqMessage mqMessage) : base(mqMessage.ProductBasketId, mqMessage.CustomerId)
        {
        }
        
        #endregion
    }
}