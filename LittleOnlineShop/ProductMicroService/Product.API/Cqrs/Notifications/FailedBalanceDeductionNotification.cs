using System;
using MediatR;
using SharedInfrastructure.Models.MqMessages;

namespace ProductApi.Cqrs.Notifications
{
    public class FailedBalanceDeductionNotification : FailedBalanceDeductionMqMessage, INotification
    {
        public FailedBalanceDeductionNotification(Guid productBasketId, Guid customerId) : base(productBasketId,
            customerId)
        {
        }

        public FailedBalanceDeductionNotification(FailedBalanceDeductionMqMessage message) : base(
            message.ProductBasketId, message.CustomerId)
        {
        }
    }
}