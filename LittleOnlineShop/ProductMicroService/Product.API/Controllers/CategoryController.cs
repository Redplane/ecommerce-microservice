﻿using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ProductApi.Cqrs.Commands;
using ProductApi.Cqrs.Queries;
using ProductDomain.Models.Entities;
using SharedDomain.Models;
using SharedMicroService.Attributes;
using SharedMicroService.Constants;

namespace ProductApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        #region Properties

        private readonly IMediator _mediator;

        #endregion

        #region Constructor

        public CategoryController(IMediator mediator)
        {
            _mediator = mediator;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Add category to system asynchronously.
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost("")]
        [Authorize(Policy = PolicyConstants.IsAdmin)]
        public async Task<ActionResult<Category>> AddCategoryAsync([FromBody] AddCategoryCommand command)
        {
            if (command == null)
                command = new AddCategoryCommand();

            var category = await _mediator.Send(command);
            return Ok(category);
        }

        /// <summary>
        ///     Find categories asynchronously.
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost("search")]
        [LenientAuthorization]
        public async Task<ActionResult<SearchResultValueObject<Category>>> FindCategoriesAsync(
            [FromBody] FindCategoriesQuery command)
        {
            var loadCategoriesResult = await _mediator.Send(command);
            return Ok(loadCategoriesResult);
        }

        #endregion
    }
}