﻿using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using ProductApi.Cqrs.Commands;

namespace ProductApi.Controllers
{
    [Route("api/product-basket")]
    public class ProductBasketController : Controller
    {
        #region Properties

        private readonly IMediator _mediator;

        #endregion

        #region Constructor

        public ProductBasketController(IMediator mediator)
        {
            _mediator = mediator;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Update product basket asynchronously.
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPut("")]
        public async Task<IActionResult> UpdateProductBasketAsync([FromBody] UpdateProductBasketCommand command)
        {
            if (command == null)
                command = new UpdateProductBasketCommand();

            var addedProduct = await _mediator.Send(command);
            return Ok(addedProduct);
        }

        /// <summary>
        /// Check out the pending product basket.
        /// </summary>
        /// <returns></returns>
        [HttpPost("checkout")]
        public async Task<IActionResult> CheckoutPendingProductBasketAsync()
        {
            var pendingProductBasketCheckoutCommand = new CheckoutPendingProductBasketCommand();
            var checkedOutProductBasket = await _mediator.Send(pendingProductBasketCheckoutCommand);
            return Ok(checkedOutProductBasket);
        }
        

        #endregion
    }
}