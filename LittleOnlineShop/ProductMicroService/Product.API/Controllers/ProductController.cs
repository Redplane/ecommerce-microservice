﻿using System;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using ProductApi.Cqrs.Commands;
using ProductApi.Cqrs.Queries;
using ProductDomain.Models.Entities;
using SharedDomain.Models;
using SharedMicroService.Attributes;

namespace ProductApi.Controllers
{
    [Route("api/product")]
    public class ProductController : Controller
    {
        #region Properties

        private readonly IMediator _mediator;

        #endregion

        #region Constructor

        public ProductController(IMediator mediator)
        {
            _mediator = mediator;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Add product into system.
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost("")]
        public virtual async Task<ActionResult<Product>> AddProductAsync([FromBody] AddProductCommand command)
        {
            if (command == null)
                command = new AddProductCommand();

            return await _mediator.Send(command);
        }

        /// <summary>
        ///     Edit product in the system.
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPut("{productId}")]
        public virtual async Task<ActionResult<Product>> EditProductAsync([FromRoute] Guid productId,
            [FromBody] EditProductCommand command)
        {
            if (command == null)
                command = new EditProductCommand();

            command.Id = productId;
            var product = await _mediator.Send(command);
            return product;
        }

        /// <summary>
        ///     Upload product photo asynchronously.
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost("photo")]
        public virtual async Task<ActionResult<Photo>> UploadProductPhotoAsync(
            [FromBody] UploadProductPhotoCommand command)
        {
            if (command == null)
                command = new UploadProductPhotoCommand();

            var uploadedPhoto = await _mediator.Send(command);
            return uploadedPhoto;
        }

        /// <summary>
        ///     Edit product photo asynchronously.
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPut("photo/{productId}")]
        public virtual async Task<ActionResult<Product>> EditProductPhotoAsync([FromRoute] Guid productId,
            [FromBody] EditProductPhotoCommand command)
        {
            if (command == null)
                command = new EditProductPhotoCommand();

            var updatedProduct = await _mediator.Send(command);
            return updatedProduct;
        }

        /// <summary>
        ///     Delete product photo asynchronously.
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpDelete("photo")]
        public virtual async Task<ActionResult<Product>> DeleteProductPhotoASync(
            [FromQuery] DeleteProductPhotoCommand command)
        {
            if (command == null)
                command = new DeleteProductPhotoCommand();

            var product = await _mediator.Send(command);
            return product;
        }

        /// <summary>
        ///     Search products asynchronously
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpPost("search")]
        [LenientAuthorization]
        public virtual async Task<ActionResult<SearchResultValueObject<Product>>> SearchProductAsync(
            [FromBody] FindProductsQuery query)
        {
            if (query == null)
                query = new FindProductsQuery();

            return await _mediator.Send(query);
        }

        #endregion
    }
}