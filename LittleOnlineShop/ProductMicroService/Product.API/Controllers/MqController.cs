using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SharedInfrastructure.Constants;
using SharedInfrastructure.Models.MqMessages;
using SharedInfrastructure.Services.Interfaces;

namespace ProductApi.Controllers
{
    [Route("api/mq")]
    [AllowAnonymous]
    public class MqController : Controller
    {
        #region Properties

        private readonly IMqManagerService _mqManagerService;

        #endregion

        #region Constructor

        public MqController(IMqManagerService mqManagerService)
        {
            _mqManagerService = mqManagerService;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Publish product reserved message.
        /// </summary>
        /// <returns></returns>
        [HttpPost("product-reserved-event")]
        public async Task<IActionResult> PublishProductReservedMessage()
        {
            return Ok();
        }
        
        #endregion
    }
}