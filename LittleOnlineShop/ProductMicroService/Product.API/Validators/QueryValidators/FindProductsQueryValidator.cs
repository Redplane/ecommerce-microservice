﻿using System.Collections.Generic;
using FluentValidation;
using ProductApi.Cqrs.Queries;
using ProductDomain.Models.Entities;

namespace ProductApi.Validators.QueryValidators
{
    public class FindProductsQueryValidator : AbstractValidator<FindProductsQuery>
    {
        #region Constructor

        public FindProductsQueryValidator()
        {
            var supportedQueriedProperties = new HashSet<string>
            {
                nameof(Product.Id), nameof(Product.Name),
                nameof(Product.Sku), nameof(Product.Price),
                nameof(Product.CreatedTime), nameof(Product.LastModifiedTime)
            };

            //RuleFor(query => query.Conditions)
            //    .Must(filteredProperties => filteredProperties.All(x => supportedQueriedProperties.Contains(x.Name)))
            //    .WithMessage(query => HttpResponseMessageCodeConstants.NotSupportedFilteredPropertyName)
            //    .When(query => query.Conditions != null && query.Conditions.Length > 0);
        }

        #endregion
    }
}