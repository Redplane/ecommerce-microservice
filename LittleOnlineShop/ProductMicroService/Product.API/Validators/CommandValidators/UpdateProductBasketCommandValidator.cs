﻿using System;
using FluentValidation;
using ProductApi.Cqrs.Commands;
using ProductInfrastructure.Constants;

namespace ProductApi.Validators.CommandValidators
{
    public class UpdateProductBasketCommandValidator : AbstractValidator<UpdateProductBasketCommand>
    {
        #region Constructor

        public UpdateProductBasketCommandValidator()
        {
            RuleFor(command => command.ProductId)
                .Must(productId => productId != Guid.Empty)
                .WithMessage(command => $"{nameof(command.ProductId)} is invalid.")
                .WithErrorCode(ValidatorMessageCodeConstants.InvalidProductId);

            RuleFor(command => command.Quantity)
                .Must(quantity => quantity > 0)
                .WithMessage(command => $"{nameof(command.Quantity)} must be greater than 0.")
                .WithErrorCode(ValidatorMessageCodeConstants.InvalidQuantity);
        }

        #endregion
    }
}