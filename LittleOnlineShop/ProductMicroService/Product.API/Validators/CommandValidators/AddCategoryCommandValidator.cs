﻿using FluentValidation;
using ProductApi.Cqrs.Commands;

namespace ProductApi.Validators.CommandValidators
{
    public class AddCategoryCommandValidator : AbstractValidator<AddCategoryCommand>
    {
        #region Constructor

        public AddCategoryCommandValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty();

            RuleFor(x => x.Description)
                .NotEmpty();
        }

        #endregion
    }
}