﻿using FluentValidation;
using ProductApi.Constants;
using ProductApi.Cqrs.Commands;

namespace ProductApi.Validators.CommandValidators
{
    public class EditProductCommandValidator : AbstractValidator<EditProductCommand>
    {
        #region Constructor

        public EditProductCommandValidator()
        {
            RuleFor(command => command.CategoryIds.Value)
                .NotNull()
                .WithMessage(ValidatorMessageConstants.FieldIsRequired)
                .Must(categories => categories.Count > 0)
                .WithMessage(ValidatorMessageConstants.FieldIsRequired)
                .When(command => command.CategoryIds != null && command.CategoryIds.HasModified);

            RuleFor(command => command.Sku.Value)
                .NotEmpty()
                .WithMessage(ValidatorMessageConstants.FieldIsRequired)
                .When(command => command.Sku != null && command.Sku.HasModified);

            RuleFor(command => command.Name.Value)
                .NotEmpty()
                .WithMessage(ValidatorMessageConstants.FieldIsRequired)
                .When(command => command.Name != null && command.Name.HasModified);

            RuleFor(command => command.Price.Value)
                .Must(price => price >= 0)
                .WithMessage(ValidatorMessageConstants.FieldCannotSmallerThanZero)
                .When(command => command.Price != null && command.Price.HasModified);

            RuleFor(command => command.Quantity.Value)
                .Must(quantity => quantity >= 0)
                .WithMessage(ValidatorMessageConstants.FieldCannotSmallerThanZero)
                .When(command => command.Quantity != null && command.Quantity.HasModified);

            RuleFor(command => command.Availability.Value)
                .IsInEnum()
                .WithMessage(ValidatorMessageConstants.FieldIsInvalid)
                .When(command => command.Availability != null && command.Availability.HasModified);
        }

        #endregion
    }
}