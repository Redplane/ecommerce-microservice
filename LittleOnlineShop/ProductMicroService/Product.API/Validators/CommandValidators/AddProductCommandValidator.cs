﻿using FluentValidation;
using ProductApi.Constants;
using ProductApi.Cqrs.Commands;

namespace ProductApi.Validators.CommandValidators
{
    public class AddProductCommandValidator : AbstractValidator<AddProductCommand>
    {
        #region Constructor

        public AddProductCommandValidator()
        {
            RuleFor(command => command.CategoryIds)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotNull()
                .WithMessage(ValidatorMessageConstants.FieldIsRequired)
                .Must(command => command.Count > 0)
                .WithMessage(ValidatorMessageConstants.FieldIsRequired);

            RuleFor(command => command.Sku)
                .NotEmpty()
                .WithMessage(ValidatorMessageConstants.FieldIsRequired);

            RuleFor(command => command.Name)
                .NotEmpty()
                .WithMessage(ValidatorMessageConstants.FieldIsRequired);

            RuleFor(command => command.Price)
                .Must(price => price >= 0)
                .WithMessage(ValidatorMessageConstants.FieldCannotSmallerThanZero);

            RuleFor(command => command.Availability)
                .IsInEnum()
                .WithMessage(ValidatorMessageConstants.FieldIsInvalid);
        }

        #endregion
    }
}