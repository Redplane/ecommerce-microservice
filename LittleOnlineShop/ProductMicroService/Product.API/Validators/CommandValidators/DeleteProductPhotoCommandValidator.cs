﻿using System;
using FluentValidation;
using ProductApi.Constants;
using ProductApi.Cqrs.Commands;

namespace ProductApi.Validators.CommandValidators
{
    public class DeleteProductPhotoCommandValidator : AbstractValidator<DeleteProductPhotoCommand>
    {
        #region Constructor

        public DeleteProductPhotoCommandValidator()
        {
            RuleFor(command => command.ProductId)
                .Must(productId => productId != Guid.Empty)
                .WithMessage(ValidatorMessageConstants.FieldIsRequired);
        }

        #endregion
    }
}