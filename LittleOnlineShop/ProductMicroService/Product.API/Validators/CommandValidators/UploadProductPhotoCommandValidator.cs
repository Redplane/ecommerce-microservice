﻿using FluentValidation;
using ProductApi.Constants;
using ProductApi.Cqrs.Commands;

namespace ProductApi.Validators.CommandValidators
{
    public class UploadProductPhotoCommandValidator : AbstractValidator<UploadProductPhotoCommand>
    {
        #region Constructor

        public UploadProductPhotoCommandValidator()
        {
            RuleFor(command => command.ProductId)
                .NotNull()
                .WithMessage(ValidatorMessageConstants.FieldIsInvalid);

            RuleFor(command => command.LoadDecodedPhoto())
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotNull()
                .WithName(command => $"{command.Photo}")
                .Must(photo => photo.Width < ValidatorValueConstants.MaxProductImageSize.Width)
                .WithMessage(ValidatorMessageConstants.MaxProductPhotoWidthExceeded)
                .Must(photo => photo.Height < ValidatorValueConstants.MaxProductImageSize.Height)
                .WithMessage(ValidatorMessageConstants.MaxProductPhotoHeightExceeded)
                .Must(photo =>
                {
                    var ratio = (double) photo.Width / photo.Height;
                    return 1.3 <= ratio && ratio < 1.4;
                })
                .WithMessage(ValidatorMessageConstants.ProductPhotoRatioInvalid);
        }

        #endregion
    }
}