﻿namespace ProductApi.Constants
{
    public class ConfigurationSectionKeyConstants
    {
        #region Properties

        /// <summary>
        ///     Identity server client settings.
        /// </summary>
        public const string IdentityServerClient = "identityServer";

        /// <summary>
        ///     Image upload hosting client.
        /// </summary>
        public const string ImageUploadHostingClient = "imageUploadHostingClient";

        /// <summary>
        /// Rabbit mq configuration.
        /// </summary>
        public const string RabbitMq = "rabbitMq";

        #endregion
    }
}