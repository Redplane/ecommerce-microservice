﻿namespace ProductApi.Constants
{
    public class DatabaseConnectionStringConstants
    {
        #region Properties

        /// <summary>
        ///     Main database of product micro service.
        /// </summary>
        public const string ProductDatabase = "productDatabase";

        #endregion
    }
}