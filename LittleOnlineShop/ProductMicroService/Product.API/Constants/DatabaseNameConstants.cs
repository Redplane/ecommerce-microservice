﻿namespace ProductApi.Constants
{
    public class DatabaseNameConstants
    {
        #region Properties

        /// <summary>
        ///     Main database constant.
        /// </summary>
        public const string MainDatabase = "product_ms";

        #endregion
    }
}