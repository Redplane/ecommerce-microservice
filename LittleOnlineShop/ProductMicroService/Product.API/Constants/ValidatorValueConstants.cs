﻿using SharedDomain.Models;

namespace ProductApi.Constants
{
    public class ValidatorValueConstants
    {
        #region Properties

        /// <summary>
        ///     Maximum product photo image size.
        /// </summary>
        public static ImageSizeValueObject MaxProductImageSize => new ImageSizeValueObject(800, 600);

        #endregion
    }
}