﻿namespace ProductApi.Constants
{
    public class CollectionNameConstants
    {
        #region Properties

        /// <summary>
        ///     Product category.
        /// </summary>
        public const string Category = "categories";

        /// <summary>
        ///     Products.
        /// </summary>
        public const string Product = "products";

        /// <summary>
        ///     Product baskets.
        /// </summary>
        public const string ProductBasket = "product-baskets";

        #endregion
    }
}