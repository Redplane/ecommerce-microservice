﻿namespace ProductApi.Constants
{
    public class ValidatorMessageConstants
    {
        #region Properties

        /// <summary>
        ///     Field is required.
        /// </summary>
        public const string FieldIsRequired = "field_is_required";

        /// <summary>
        ///     Field cannot be smaller than zero.
        /// </summary>
        public const string FieldCannotSmallerThanZero = "cannot_be_smaller_than_zero";

        /// <summary>
        ///     Field is invalid.
        /// </summary>
        public const string FieldIsInvalid = "field_is_invalid";

        /// <summary>
        ///     Product photo ratio invalid.
        /// </summary>
        public const string ProductPhotoRatioInvalid = "product_photo_raio_invalid";

        /// <summary>
        ///     Maximum product photo height exceeded.
        /// </summary>
        public const string MaxProductPhotoHeightExceeded = "maximum_product_photo_height_exceeded";

        /// <summary>
        ///     Maximum product photo width exceeded.
        /// </summary>
        public const string MaxProductPhotoWidthExceeded = "maximum_product_photo_width_exceeded";

        #endregion
    }
}