﻿using DomainShared.Models;
using Product.Business.ViewModels.Request;
using Product.Business.ViewModels.Response;
using System;
using System.Threading.Tasks;

namespace Product.Business.Services.Category
{
    public interface ICategoryService
    {
        CategoryResponse Create(CategoryViewModel model);

        Guid? Update(Guid id, CategoryViewModel model);

        Guid? Delete(Guid id);

        Task<CategoryResponse> Get(Guid id);

        Task<SearchResultValueObject<CategoryResponse>> Search(SearchCategoryViewModel model);
    }
}
