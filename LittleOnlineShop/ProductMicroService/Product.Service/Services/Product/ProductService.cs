﻿using AutoMapper;
using DomainShared.Models;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using Product.Business.ViewModels.Request;
using Product.Business.ViewModels.Response;
using Product.Domain.SeedWork;
using Product.Infrastructure.Repositories;
using ServicesShared.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Product.Business.Services.Product
{
    public class ProductService : IProductService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IProductRepository _productRepository;
        private readonly IBaseTimeService _timeService;
        private readonly IMapper _mapper;

        public ProductService(IUnitOfWork unitOfWork, IProductRepository productRepository, IBaseTimeService timeService, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _productRepository = productRepository;
            _timeService = timeService;
            _mapper = mapper;
        }

        public ProductResponse Create(ProductViewModel model)
        {
            var product = new Domain.Entities.Product
            {
                Name = model.Name,
                Description = model.Description,
                CategoryId = model.CategoryId,
                AvailableQuantity = model.AvailableQuantity,
                TotalQuantity = model.TotalQuantity,
                IsActive = true,
                Photo = string.Empty,
                Price = model.Price,
                CreatedTime = _timeService.DateTimeUtcToUnix(DateTime.UtcNow),
                Sku = Guid.NewGuid().ToString(),
                Specifications = model.Specifications
            };

            var productAdded = _productRepository.Add(product);

            return _mapper.Map<ProductResponse>(productAdded);
        }

        public Guid? Update(Guid id, ProductViewModel model)
        {
            var product = _productRepository.GetById(id);

            if (product == null)
                throw new Exception("Product Not Found");
            product.Name = model.Name;
            product.CategoryId = model.CategoryId;
            product.AvailableQuantity = model.AvailableQuantity;
            product.Description = model.Description;
            product.IsActive = model.IsActive;
            product.Photo = model.Photo;
            product.Price = model.Price;
            product.Sku = model.Sku;
            product.Specifications = model.Specifications;
            product.UpdatedTime = _timeService.DateTimeUtcToUnix(DateTime.UtcNow);

            _productRepository.Update(product);

            return product.Id;
        }

        public Guid? Delete(Guid id)
        {
            var toDelete = _productRepository.GetById(id);

            if (toDelete == null)
                throw new Exception("Product Not Found");

            _productRepository.Delete(toDelete);

            return id;
        }

        public async Task<ProductResponse> Get(Guid id)
        {
            var product = await _productRepository.GetByIdAsync(id);

            if (product == null)
                throw new Exception("Product Not Found");

            return _mapper.Map<ProductResponse>(product);
        }

        public async Task<SearchResultValueObject<ProductResponse>> Search(SearchProductViewModel model)
        {
            var query = _productRepository.AsQueryable();


            if (!string.IsNullOrEmpty(model.Name))
            {
                query = query.Where(c => c.Name.Contains(model.Name) && c.IsActive);
            }

            if (model.Price > 0)
            {
                query = query.Where(c => c.Price == model.Price && c.IsActive);
            }

            // Get total pages
            var total = await ((IMongoQueryable<Domain.Entities.Product>)query).CountAsync();

            // Pagging
            query = query.Skip(model.Skip).Take(model.Records);

            var result = await ((IMongoQueryable<Domain.Entities.Product>)query).ToListAsync();

            var response = _mapper.Map<List<ProductResponse>>(result);

            return new SearchResultValueObject<ProductResponse>(response, total);
        }
    }
}
