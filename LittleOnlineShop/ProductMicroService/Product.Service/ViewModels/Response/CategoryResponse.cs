﻿using System;

namespace Product.Business.ViewModels.Response
{
    public class CategoryResponse
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime CreatedTime { get; set; }

        public DateTime UpdatedTime { get; set; }

        public bool IsActive { get; set; }
    }
}
