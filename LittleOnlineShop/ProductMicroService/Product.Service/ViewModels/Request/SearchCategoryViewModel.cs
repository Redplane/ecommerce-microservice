﻿namespace Product.Business.ViewModels.Request
{
    public class SearchCategoryViewModel : BaseSearchViewModel
    {
        public string KeyWord { get; set; }
    }
}
