namespace ProductDomain.Enums
{
    public enum ProductBasketStatuses
    {
        Pending,
        CheckedOut,
        Reserved,
        Cancelled
    }
}