﻿using System;

namespace ProductDomain.Models.Entities
{
    public class Product : Entity
    {
        #region Constructor

        public Product(Guid id) : base(id)
        {
        }

        #endregion

        #region Properties

        /// <summary>
        ///     List of categories the product belongs to.
        /// </summary>
        public Guid[] CategoryIds { get; set; }

        /// <summary>
        ///     Stock keeping unit.
        /// </summary>
        public string Sku { get; set; }

        /// <summary>
        ///     Product name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        ///     Description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        ///     Specification of product.
        /// </summary>
        public ProductSpecificationValueObject[] Specifications { get; set; }

        /// <summary>
        ///     Photos the product has.
        /// </summary>
        public Photo[] Photos { get; set; }

        /// <summary>
        ///     Product thumbnail image.
        /// </summary>
        public string Thumbnail { get; set; }

        /// <summary>
        ///     Product price.
        /// </summary>
        public decimal Price { get; set; }

        #endregion
    }
}