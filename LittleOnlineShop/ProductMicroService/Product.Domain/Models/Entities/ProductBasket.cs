﻿using System;
using ProductDomain.Enums;
using SharedDomain.Enums;

namespace ProductDomain.Models.Entities
{
    public class ProductBasket : Entity
    {
        #region Constructor

        public ProductBasket(Guid id, Guid customerId) : base(id)
        {
            CustomerId = customerId;
            Items = new ProductBasketItemValueObject[0];
            Availability = MasterItemAvailabilities.Available;
            Status = ProductBasketStatuses.Pending;
        }

        #endregion

        #region Properties

        // ReSharper disable once AutoPropertyCanBeMadeGetOnly.Local
        public Guid CustomerId { get; private set; }

        public ProductBasketItemValueObject[] Items { get; set; }

        /// <summary>
        /// Status of product basket.
        /// </summary>
        public ProductBasketStatuses Status { get; set; }
        
        #endregion
    }
}