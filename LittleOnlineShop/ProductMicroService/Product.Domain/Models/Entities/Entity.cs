﻿using System;
using SharedDomain.Enums;
using SharedDomain.Extensions;
using SharedDomain.Interfaces;

namespace ProductDomain.Models.Entities
{
    public class Entity : IEntity
    {
        #region Constructor

        public Entity(Guid id)
        {
            Id = id;
            Availability = MasterItemAvailabilities.Available;
            CreatedTime = DateTime.UtcNow.ToUnixTime();
        }

        #endregion

        #region Properties

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        public Guid Id { get; }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        public MasterItemAvailabilities Availability { get; set; }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        public double CreatedTime { get; set; }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        public double? LastModifiedTime { get; set; }

        #endregion
    }
}