﻿using System;

namespace ProductDomain.Models.Entities
{
    public class Photo : Entity
    {
        #region Constructor

        public Photo(Guid id) : base(id)
        {
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Photo relative url.
        /// </summary>
        public string RelativeUrl { get; set; }

        #endregion
    }
}