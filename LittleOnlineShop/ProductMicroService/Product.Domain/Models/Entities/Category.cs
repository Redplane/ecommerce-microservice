﻿using System;

namespace ProductDomain.Models.Entities
{
    public class Category : Entity
    {
        #region Constructor

        public Category(Guid id) : base(id)
        {
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Name of category.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        ///     Category description.
        /// </summary>
        public string Description { get; set; }

        #endregion
    }
}