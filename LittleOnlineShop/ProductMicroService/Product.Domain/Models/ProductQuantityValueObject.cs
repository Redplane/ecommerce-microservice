﻿using System.Collections.Generic;
using SharedDomain.Models;

namespace ProductDomain.Models
{
    public class ProductQuantityValueObject : ValueObject
    {
        #region Constructor

        public ProductQuantityValueObject(double availableProducts, double remainingProducts)
        {
            AvailableProducts = availableProducts;
            RemainingProducts = remainingProducts;
        }

        #endregion

        #region Methods

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return AvailableProducts;
            yield return RemainingProducts;
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Number of available product.
        /// </summary>
        // ReSharper disable once AutoPropertyCanBeMadeGetOnly.Local
        public double AvailableProducts { get; private set; }

        /// <summary>
        ///     Number of remaining product.
        /// </summary>
        // ReSharper disable once AutoPropertyCanBeMadeGetOnly.Local
        public double RemainingProducts { get; private set; }

        #endregion
    }
}