﻿using System.Collections.Generic;
using SharedDomain.Models;

namespace ProductDomain.Models
{
    public class ProductSpecificationValueObject : ValueObject
    {
        #region Constructor

        public ProductSpecificationValueObject(string name, string description)
        {
            Name = name;
            Description = description;
        }

        #endregion

        #region Methods

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Name;
        }

        #endregion

        #region Properties

        public string Name { get; }

        public string Description { get; }

        #endregion
    }
}