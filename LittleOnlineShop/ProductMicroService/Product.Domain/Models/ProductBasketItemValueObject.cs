﻿using System;
using System.Collections.Generic;
using SharedDomain.Models;

namespace ProductDomain.Models
{
    public class ProductBasketItemValueObject : ValueObject
    {
        #region Constructor

        public ProductBasketItemValueObject(Guid productId, decimal quantity, decimal totalMoney)
        {
            ProductId = productId;
            Quantity = quantity;
            TotalMoney = totalMoney;
        }
        
        #endregion

        #region Methods

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return ProductId;
        }

        #endregion

        #region Properties

        // ReSharper disable once AutoPropertyCanBeMadeGetOnly.Local
        public Guid ProductId { get; private set; }

        // ReSharper disable once AutoPropertyCanBeMadeGetOnly.Local
        public decimal Quantity { get; private set; }
        
        // ReSharper disable once AutoPropertyCanBeMadeGetOnly.Local
        public decimal TotalMoney { get; private set; }

        #endregion
    }
}