## Customer micro service
### Description:
- A small service manage user information & authentication.

### Database update:
- To add a new migration, use command: `Add-Migration -Name <name of migration> -OutputDir "Migrations/CustomerMs" -Context CustomerDbContext`.
- Run command `Update-Database -Context CustomerDbContext` to update database.