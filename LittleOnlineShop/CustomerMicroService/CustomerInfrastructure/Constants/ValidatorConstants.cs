namespace CustomerInfrastructure.Constants
{
    public class ValidatorConstants
    {
        #region Properties

        /// <summary>
        /// Minimum allowed balance that customer can have.
        /// </summary>
        public const decimal MinimumAllowedBalance = -2000;

        #endregion
    }
}