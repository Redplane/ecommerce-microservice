namespace CustomerInfrastructure.Constants
{
    public class HttpResponseCodeConstants
    {
        #region Properties

        /// <summary>
        /// Tell the client the searching user is not found.
        /// </summary>
        public const string UserNotFound = "user_not_found";

        /// <summary>
        /// Tell the client he/she reached minimum allowed balance reached.
        /// </summary>
        public const string MinimumAllowedBalanceReached = "minimum_allowed_balance_reached";

        #endregion
    }
}