﻿using AutoMapper;
using IdentityServer4.Models;

namespace CustomerInfrastructure.MapperProfiles
{
    public class DefaultMapperProfile : Profile
    {
        public DefaultMapperProfile()
        {
            CreateMap<ApiResource, ApiResource>();
        }
    }
}