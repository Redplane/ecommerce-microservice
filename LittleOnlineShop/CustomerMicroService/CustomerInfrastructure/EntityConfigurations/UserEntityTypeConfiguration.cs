﻿using CustomerDomain.Entities;
using CustomerDomain.Models;
using CustomerInfrastructure.Constants;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CustomerInfrastructure.EntityConfigurations
{
    /// <summary>
    ///     Configuration schema of customer
    /// </summary>
    public class UserEntityTypeConfiguration : IEntityTypeConfiguration<User>
    {
        #region Methods

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="user"></param>
        public void Configure(EntityTypeBuilder<User> user)
        {
            // Setup primary key.
            user.HasKey(x => x.Id);

            // Username must be indexed and unique.
            user.HasIndex(x => x.Username)
                .IsUnique();

            // Username is required.
            user.Property(x => x.Username)
                .IsRequired();

            // Birthday is specified.
            user.Property(x => x.Birthday)
                .HasColumnType(SqlDataTypeConstants.Date);

            // Role is required.
            user.Property(x => x.Role)
                .IsRequired();

            // Balance value object persisted as owned entity in EF Core 2.0
//            user.OwnsOne(o => o.Balance)
//                .Property(x => x.RemainingBalance)
//                .HasColumnName(nameof(BalanceValueObject.RemainingBalance));
//
//            user.OwnsOne(o => o.Balance)
//                .Property(x => x.AvailableBalance)
//                .HasColumnName(nameof(BalanceValueObject.AvailableBalance));
        }

        #endregion
    }
}