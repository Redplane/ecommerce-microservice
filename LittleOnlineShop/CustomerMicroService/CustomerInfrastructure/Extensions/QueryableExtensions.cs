using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SharedDomain.Models;

namespace CustomerInfrastructure.Extensions
{
    public static class QueryableExtensions
    {
        #region Methods

        /// <summary>
        /// Query items and returns search result.
        /// </summary>
        /// <param name="items"></param>
        /// <param name="pager"></param>
        /// <param name="cancellationToken"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static async Task<SearchResultValueObject<T>> ToSearchResultAsync<T>(this IQueryable<T> items, 
            PagerValueObject pager, CancellationToken cancellationToken = default)
        {
            // Count the total records that match with the search condition.
            var totalRecords = await items.CountAsync(cancellationToken);
            
            // Items should be queried.
            if (pager.ShouldItemQueried())
            {
                var pageIndex = pager.Page - 1;
                if (pageIndex < 0)
                    pageIndex = 0;

                var skippedRecords = pageIndex * pager.Records;
                var preloadingItems = items.Skip(skippedRecords)
                    .Take(skippedRecords);
                
                var loadedItems = await preloadingItems.ToListAsync(cancellationToken);
                return new SearchResultValueObject<T>(loadedItems, totalRecords);
            }
            
            return new SearchResultValueObject<T>(new List<T>(), totalRecords);
        }
        
        #endregion
    }
}