﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using CustomerDomain.Entities;
using CustomerDomain.Enums;
using IdentityModel;
using SharedDomain.Constants;

namespace CustomerInfrastructure.Extensions
{
    public static class UserExtensions
    {
        #region Methods

        public static IList<Claim> ToClaims(this User user)
        {
            var claims = new List<Claim>();

            claims.Add(new Claim(JwtClaimTypeConstants.Id, user.Id.ToString("D")));
            claims.Add(new Claim(JwtClaimTypes.Subject, user.Id.ToString("D")));
            claims.Add(new Claim(JwtClaimTypeConstants.Username, user.Username));

            if (!string.IsNullOrWhiteSpace(user.Email))
                claims.Add(new Claim(JwtClaimTypeConstants.Email, user.Email));

            var userBirthday = user.Birthday?.ToString();
            if (!string.IsNullOrWhiteSpace(userBirthday))
                claims.Add(new Claim(JwtClaimTypeConstants.Birthday, userBirthday));
            claims.Add(new Claim(JwtClaimTypeConstants.FullName, user.FullName));
            claims.Add(new Claim(JwtClaimTypeConstants.AuthenticationProvider,
                Enum.GetName(typeof(AuthenticationProviders), user.AuthenticationProvider)));
            claims.Add(new Claim(JwtClaimTypeConstants.UserStatus, Enum.GetName(typeof(UserStatuses), user.Status)));
            claims.Add(new Claim(JwtClaimTypeConstants.Role, user.Role));
            claims.Add(new Claim(JwtClaimTypeConstants.JoinedTime, $"{user.JoinedTime}"));

            return claims;
        }

        #endregion
    }
}