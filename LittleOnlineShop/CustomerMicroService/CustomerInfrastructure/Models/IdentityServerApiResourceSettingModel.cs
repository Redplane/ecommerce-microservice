﻿using System.Collections.Generic;
using IdentityServer4.Models;

namespace CustomerInfrastructure.Models
{
    public class IdentityServerApiResourceSettingModel
    {
        #region Properties

        public string Name { get; set; }

        public string DisplayName { get; set; }

        public string Description { get; set; }

        public bool Enabled { get; set; }

        public List<string> ApiSecrets { get; set; }

        public List<Scope> Scopes { get; set; }

        public Dictionary<string, string> Properties { get; set; }

        public List<string> UserClaims { get; set; }

        #endregion
    }
}