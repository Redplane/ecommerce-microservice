﻿using System.Collections.Generic;
using IdentityServer4.Models;

namespace CustomerInfrastructure.Models
{
    public class IdentityServerClientSettingModel
    {
        #region Properties

        public string ClientId { get; set; }

        public List<string> ClientSecrets { get; set; }

        public List<string> AllowedGrantTypes { get; set; }

        public List<string> AllowedScopes { get; set; }

        public bool AllowOfflineAccess { get; set; }

        public TokenExpiration RefreshTokenExpiration { get; set; }

        public TokenUsage RefreshTokenUsage { get; set; }

        public int SlidingRefreshTokenLifeTime { get; set; }

        #endregion
    }
}