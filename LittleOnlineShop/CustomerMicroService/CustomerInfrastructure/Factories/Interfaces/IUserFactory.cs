﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using CustomerDomain.Entities;
using CustomerDomain.Enums;
using CustomerDomain.Models;
using SharedDomain.Models;

namespace CustomerInfrastructure.Factories.Interfaces
{
    public interface IUserFactory
    {
        #region Methods

        /// <summary>
        ///     Add user into system asynchronously.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="email"></param>
        /// <param name="password"></param>
        /// <param name="fullName"></param>
        /// <param name="birthday"></param>
        /// <param name="balance"></param>
        /// <param name="authenticationProvider"></param>
        /// <param name="status"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<User> AddUserAsync(string username, string email, string password, string fullName, DateTime? birthday,
            decimal balance, AuthenticationProviders authenticationProvider, UserStatuses status,
            CancellationToken cancellationToken = default);

        /// <summary>
        ///     Find user by using specific condition.
        /// </summary>
        /// <param name="username">Username belongs to user (required)</param>
        /// <param name="password">Original password(optional)</param>
        /// <param name="authenticationProvider">(optional)</param>
        /// <param name="status">(optional)</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<User> FindUserAsync(string username, string password, AuthenticationProviders? authenticationProvider,
            UserStatuses? status,
            CancellationToken cancellationToken = default);

        /// <summary>
        ///     Find user by ids asynchronously.
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<List<User>> FindUsersByIdsAsync(HashSet<Guid> ids, CancellationToken cancellationToken = default);

        /// <summary>
        /// Deduct user balance asynchronously.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="money"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<User> DeductUserBalanceAsync(Guid userId, decimal money, CancellationToken cancellationToken = default);

        /// <summary>
        /// Find users using specific conditions.
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="usernames"></param>
        /// <param name="emails"></param>
        /// <param name="authenticationProviders"></param>
        /// <param name="statuses"></param>
        /// <param name="roles"></param>
        /// <param name="pager"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<SearchResultValueObject<User>> FindUsersAsync(HashSet<Guid> ids, HashSet<string> usernames,
            HashSet<string> emails, HashSet<AuthenticationProviders> authenticationProviders,
            HashSet<UserStatuses> statuses, HashSet<string> roles, PagerValueObject pager = default,
            CancellationToken cancellationToken = default);

        #endregion
    }
}