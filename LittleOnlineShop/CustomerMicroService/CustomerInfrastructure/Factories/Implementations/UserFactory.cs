﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;
using CustomerDomain.Entities;
using CustomerDomain.Enums;
using CustomerDomain.Models;
using CustomerInfrastructure.Constants;
using CustomerInfrastructure.DbContexts;
using CustomerInfrastructure.Exceptions;
using CustomerInfrastructure.Extensions;
using CustomerInfrastructure.Factories.Interfaces;
using Microsoft.EntityFrameworkCore;
using SharedDomain.Extensions;
using SharedDomain.Models;
using SharedInfrastructure.Exceptions;
using SharedInfrastructure.Services.Interfaces;

namespace CustomerInfrastructure.Factories.Implementations
{
    public class UserFactory : IUserFactory
    {
        #region Contructor

        public UserFactory(IBaseEncryptionService baseEncryptionService,
            CustomerDbContext dbContext)
        {
            _dbContext = dbContext;
            _baseEncryptionService = baseEncryptionService;
        }

        #endregion

        #region Fields

        /// <summary>
        ///     Customer micro-service database context.
        /// </summary>
        private readonly CustomerDbContext _dbContext;

        /// <summary>
        ///     Service which is for data encryption | decryption.
        /// </summary>
        private readonly IBaseEncryptionService _baseEncryptionService;

        #endregion

        #region Methods

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="username"></param>
        /// <param name="email"></param>
        /// <param name="password"></param>
        /// <param name="fullName"></param>
        /// <param name="birthday"></param>
        /// <param name="balance"></param>
        /// <param name="authenticationProvider"></param>
        /// <param name="status"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task<User> AddUserAsync(string username, string email, string password, string fullName,
            DateTime? birthday, decimal balance,
            AuthenticationProviders authenticationProvider, UserStatuses status,
            CancellationToken cancellationToken = default)
        {
            var user = new User(Guid.NewGuid(), username);

            if (!string.IsNullOrEmpty(password))
                user.HashedPassword = _baseEncryptionService.Md5Hash(password);

            user.Email = email;
            user.FullName = fullName;
            user.Birthday = birthday;
            
            if (balance < ValidatorConstants.MinimumAllowedBalance)
                throw new InvalidBalanceException();
            
            user.Balance = balance;
            user.AuthenticationProvider = authenticationProvider;
            user.Status = status;

            // Update user joined time by getting current time.
            user.JoinedTime = DateTime.UtcNow.ToUnixTime();

            var addedUser = _dbContext.Users.Add(user);
            await _dbContext.SaveChangesAsync(cancellationToken);

            return addedUser.Entity;
        }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="authenticationProvider"></param>
        /// <param name="status"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task<User> FindUserAsync(string username, string password,
            AuthenticationProviders? authenticationProvider,
            UserStatuses? status,
            CancellationToken cancellationToken = default)
        {
            var users = _dbContext.Users.AsQueryable();

            // Find user by using username (case insensitive)
            users = users.Where(x => x.Username.Equals(username));

            // Password is defined.
            if (!string.IsNullOrEmpty(password))
            {
                var hashedPassword = _baseEncryptionService.Md5Hash(password);
                users = users.Where(x =>
                    x.HashedPassword.Equals(hashedPassword, StringComparison.InvariantCultureIgnoreCase));
            }

            // Authentication provide is defined.
            if (authenticationProvider != null)
                users = users.Where(x => x.AuthenticationProvider == authenticationProvider.Value);

            if (status != null)
                users = users.Where(x => x.Status == status.Value);

            return await users.FirstOrDefaultAsync(cancellationToken);
        }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual Task<List<User>> FindUsersByIdsAsync(HashSet<Guid> ids,
            CancellationToken cancellationToken = default)
        {
            var users = _dbContext.Users.AsQueryable();
            users = users.Where(user => ids.Contains(user.Id));

            return users.ToListAsync(cancellationToken);
        }

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public virtual async Task<User> DeductUserBalanceAsync(Guid userId, decimal money,
            CancellationToken cancellationToken = default)
        {
            using (var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                var user = await _dbContext.Users
                    .Where(x => x.Id == userId && x.Status == UserStatuses.Active)
                    .FirstOrDefaultAsync(cancellationToken);

                if (user == null)
                    throw new HttpResponseException(HttpStatusCode.NotFound, HttpResponseCodeConstants.UserNotFound);

                var deductedBalance = user.Balance - money;
                if (deductedBalance < ValidatorConstants.MinimumAllowedBalance)
                    throw new HttpResponseException(HttpStatusCode.Forbidden,
                        HttpResponseCodeConstants.MinimumAllowedBalanceReached);

                user.Balance = deductedBalance;
                await _dbContext.SaveChangesAsync(cancellationToken);

                // Commit the transaction.
                transaction.Complete();

                return user;
            }
        }

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="usernames"></param>
        /// <param name="emails"></param>
        /// <param name="authenticationProviders"></param>
        /// <param name="statuses"></param>
        /// <param name="roles"></param>
        /// <param name="pager"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public virtual Task<SearchResultValueObject<User>> FindUsersAsync(HashSet<Guid> ids, HashSet<string> usernames, HashSet<string> emails, HashSet<AuthenticationProviders> authenticationProviders, HashSet<UserStatuses> statuses,
            HashSet<string> roles, PagerValueObject pager = default, CancellationToken cancellationToken = default)
        {
            // Users list.
            var users = _dbContext.Users.AsQueryable();

            // Ids are defined.
            if (ids != null && ids.Count > 0)
                users = users.Where(user => ids.Contains(user.Id));
            
            // Usernames are defined.
            if (usernames != null && usernames.Count > 0)
                users = users.Where(user => usernames.Contains(user.Username));

            // Emails are defined.
            if (emails != null && emails.Count > 0)
                users = users.Where(user => emails.Contains(user.Email));

            // Authentication providers are defined.
            if (authenticationProviders != null && authenticationProviders.Count > 0)
                users = users.Where(user => authenticationProviders.Contains(user.AuthenticationProvider));
            
            // Statuses are defined.
            if (statuses != null && statuses.Count > 0)
                users = users.Where(user => statuses.Contains(user.Status));
            
            // Roles are defined.
            if (roles != null && roles.Count > 0)
                users = users.Where(user => roles.Contains(user.Role));

            return users.ToSearchResultAsync(pager, cancellationToken);
        }

        #endregion
    }
}