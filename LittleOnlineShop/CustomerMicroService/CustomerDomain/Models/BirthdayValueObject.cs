﻿using System.Collections.Generic;
using SharedDomain.Models;

namespace CustomerDomain.Models
{
    public class BirthdayValueObject : ValueObject
    {
        #region Constructors

        public BirthdayValueObject(double? birthday)
        {
            Birthday = birthday;
        }

        #endregion

        #region Properties

        // ReSharper disable once AutoPropertyCanBeMadeGetOnly.Local
        public double? Birthday { get; private set; }

        #endregion

        #region Methods

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return null;
        }

        #endregion
    }
}