﻿namespace CustomerDomain.Enums
{
    public enum UserStatuses
    {
        Disabled,
        Pending,
        Active
    }
}