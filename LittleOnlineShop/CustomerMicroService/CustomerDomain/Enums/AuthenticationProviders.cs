﻿namespace CustomerDomain.Enums
{
    public enum AuthenticationProviders
    {
        Basic,
        Facebook,
        Google
    }
}