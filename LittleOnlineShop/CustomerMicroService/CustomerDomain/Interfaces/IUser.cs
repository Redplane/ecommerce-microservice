﻿using System;
using CustomerDomain.Enums;

namespace CustomerDomain.Interfaces
{
    public interface IUser
    {
        // ReSharper disable once AutoPropertyCanBeMadeGetOnly.Local
        Guid Id { get; }

        // ReSharper disable once AutoPropertyCanBeMadeGetOnly.Local
        string Username { get; }

        string Email { get; }

        DateTime? Birthday { get; }

        string FullName { get; }

        AuthenticationProviders AuthenticationProvider { get; }

        UserStatuses Status { get; }

        double JoinedTime { get; }

        double? LastModifiedTime { get; }
    }
}