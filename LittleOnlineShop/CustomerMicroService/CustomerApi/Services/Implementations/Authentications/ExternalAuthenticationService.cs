﻿using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CustomerApi.Services.Interfaces;
using Newtonsoft.Json;

namespace CustomerApi.Services.Implementations.Authentications
{
    public class ExternalAuthenticationService : IExternalAuthenticationService
    {
        #region Properties

        private readonly HttpClient _httpClient;

        #endregion

        #region Constructor

        public ExternalAuthenticationService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <returns></returns>
        public virtual async Task<Dictionary<string, string>> AccessTokenToFacebookClaimsAsync(string accessToken,
            List<string> fields,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var accessTokenToFacebookClaimsEndpointBuilder = new StringBuilder("https://graph.facebook.com/v2.12/me");
            accessTokenToFacebookClaimsEndpointBuilder.Append("?");
            accessTokenToFacebookClaimsEndpointBuilder.Append($"access_token={accessToken}");
            accessTokenToFacebookClaimsEndpointBuilder.Append("&");
            accessTokenToFacebookClaimsEndpointBuilder.Append($"fields={string.Join("%2C", fields)}");

            var fullUrl = accessTokenToFacebookClaimsEndpointBuilder.ToString();
            var httpResponseMessage = await _httpClient.GetAsync(fullUrl,
                cancellationToken);

            var httpContent = httpResponseMessage.Content;

            if (httpContent == null)
                return null;

            // Read content as string.
            var stringContent = await httpContent.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<Dictionary<string, string>>(stringContent);
        }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="idToken"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task<Dictionary<string, string>> IdTokenToGoogleClaimAsync(string idToken,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var idTokenToClaimsEndpoint = $"https://www.googleapis.com/oauth2/v3/tokeninfo?id_token={idToken}";
            var httpResponseMessage = await _httpClient.GetAsync(idTokenToClaimsEndpoint, cancellationToken);

            var httpContent = httpResponseMessage.Content;
            if (httpContent == null)
                return null;

            var stringContent = await httpContent.ReadAsStringAsync();
            if (string.IsNullOrEmpty(stringContent))
                return null;

            return JsonConvert.DeserializeObject<Dictionary<string, string>>(stringContent);
        }

        #endregion
    }
}