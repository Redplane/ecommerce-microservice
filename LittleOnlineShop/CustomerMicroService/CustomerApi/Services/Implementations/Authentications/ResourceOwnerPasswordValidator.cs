﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using CustomerApi.Constants;
using CustomerDomain.Enums;
using CustomerInfrastructure.Extensions;
using CustomerInfrastructure.Factories.Interfaces;
using IdentityModel;
using IdentityServer4.Models;
using IdentityServer4.Validation;
using Microsoft.AspNetCore.Http;
using SharedInfrastructure.Models;

namespace CustomerApi.Services.Implementations.Authentications
{
    public class ResourceOwnerPasswordValidator : IResourceOwnerPasswordValidator
    {
        #region Constructor

        public ResourceOwnerPasswordValidator(
            IUserFactory userFactory,
            IHttpContextAccessor httpContextAccessor)
        {
            _userFactory = userFactory;
            _httpContext = httpContextAccessor.HttpContext;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public virtual async Task ValidateAsync(ResourceOwnerPasswordValidationContext context)
        {
            // Hash the password.
            var username = context.UserName;
            var password = context.Password;

            // Find the user in the system with defined username and password.
            var user = await _userFactory.FindUserAsync(username, password, AuthenticationProviders.Basic,
                UserStatuses.Active);

            // No user is found.
            if (user == null)
            {
                context.Result =
                    new GrantValidationResult(TokenRequestErrors.InvalidGrant,
                        HttpResponseMessageConstants.InvalidCredential);
                return;
            }

            var userCredential = new UserCredential(user.ToClaims());
            context.Result = new GrantValidationResult(userCredential.Id.ToString("D"),
                OidcConstants.AuthenticationMethods.Password, DateTime.UtcNow,
                userCredential.ToClaims());

            _httpContext.Items[ClaimTypes.UserData] = userCredential;
        }

        #endregion

        #region Properties

        private readonly HttpContext _httpContext;

        private readonly IUserFactory _userFactory;

        #endregion
    }
}