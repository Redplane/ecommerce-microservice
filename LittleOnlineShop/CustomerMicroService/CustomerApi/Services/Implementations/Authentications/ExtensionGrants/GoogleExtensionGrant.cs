﻿using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using CustomerApi.Services.Interfaces;
using CustomerDomain.Enums;
using CustomerDomain.Models;
using CustomerInfrastructure.Extensions;
using CustomerInfrastructure.Factories.Interfaces;
using IdentityModel;
using IdentityServer4.Models;
using IdentityServer4.Validation;
using Microsoft.AspNetCore.Http;
using SharedInfrastructure.Constants;
using SharedInfrastructure.Models;

namespace CustomerApi.Services.Implementations.Authentications.ExtensionGrants
{
    public class GoogleExtensionGrant : IExtensionGrantValidator
    {
        #region Constructor

        public GoogleExtensionGrant(IExternalAuthenticationService externalAuthenticationService,
            IUserFactory userFactory,
            IHttpContextAccessor httpContextAccessor)
        {
            _externalAuthenticationService = externalAuthenticationService;
            _userFactory = userFactory;
            _httpContext = httpContextAccessor.HttpContext;
        }

        #endregion

        #region Methods

        public virtual async Task ValidateAsync(ExtensionGrantValidationContext context)
        {
            // Get id token that has been passed in request body.
            var idToken = context.Request.Raw.Get("id_token");

            // Get id token to claim.
            var claims = await _externalAuthenticationService.IdTokenToGoogleClaimAsync(idToken);
            if (claims == null)
            {
                context.Result = new GrantValidationResult(TokenRequestErrors.InvalidGrant,
                    SharedHttpMessageCodeConstants.InvalidClaims);
                return;
            }

            // Subject cannot be found.
            if (!claims.TryGetValue(JwtClaimTypes.Email, out var subject) || string.IsNullOrEmpty(subject))
            {
                context.Result = new GrantValidationResult(TokenRequestErrors.InvalidGrant,
                    SharedHttpMessageCodeConstants.InvalidSubject);
                return;
            }

            // Find google user by using username.
            var user = await _userFactory.FindUserAsync(subject, null, AuthenticationProviders.Google, null);
            if (user == null)
                // Register user if user is not available.
                user = await _userFactory.AddUserAsync(subject, subject,
                    null,
                    claims.TryGetValue(JwtClaimTypes.Name, out var fullName) ? fullName : null,
                    null, 0,
                    AuthenticationProviders.Google,
                    UserStatuses.Active);

            // User cannot be created.
            if (user == null || user.Status != UserStatuses.Active)
            {
                context.Result = new GrantValidationResult(TokenRequestErrors.UnauthorizedClient,
                    SharedHttpMessageCodeConstants.InvalidClient);
                return;
            }

            var userCredential = new UserCredential(user.ToClaims());
            _httpContext.Items[ClaimTypes.UserData] = userCredential;

            // Convert dictionary to claims.
            context.Result =
                new GrantValidationResult(subject, GrantType, claims.Select(x => new Claim(x.Key, x.Value)));
        }

        #endregion

        #region Properties

        public string GrantType => "Google";

        #endregion

        #region Properties

        private readonly IExternalAuthenticationService _externalAuthenticationService;

        private readonly IUserFactory _userFactory;

        private readonly HttpContext _httpContext;

        #endregion
    }
}