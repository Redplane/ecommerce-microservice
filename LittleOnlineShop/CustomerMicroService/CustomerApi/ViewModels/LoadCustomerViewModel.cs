﻿using System.Collections.Generic;

namespace CustomerApi.ViewModels
{
    public class LoadCustomerViewModel
    {
        #region Properties

        /// <summary>
        ///     Customers' name.
        /// </summary>
        public HashSet<string> Names { get; set; }

        public HashSet<string> Descriptions { get; set; }

        public int Age { get; set; }

        #endregion
    }
}