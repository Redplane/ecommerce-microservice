﻿namespace CustomerApi.Constants
{
    public class AppSettingConstants
    {
        #region Properties

        /// <summary>
        ///     Configuration key for identity server setting.
        /// </summary>
        public const string IdentityServerSettings = "identityServer";
        
        /// <summary>
        /// Rabbit mq configuration.
        /// </summary>
        public const string RabbitMq = "rabbitMq";
        
        #endregion
    }
}