﻿namespace CustomerApi.Constants
{
    public class ConnectionStringKeyConstants
    {
        #region Properties

        /// <summary>
        ///     Connection string for identity server.
        /// </summary>
        public const string IdentityServer = "customerIdentityServerMs";

        /// <summary>
        ///     Main database for micro service.
        /// </summary>
        public const string CustomerDatabase = "customerDatabase";

        #endregion
    }
}