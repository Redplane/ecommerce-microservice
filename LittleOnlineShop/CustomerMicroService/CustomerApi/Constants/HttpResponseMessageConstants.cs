﻿namespace CustomerApi.Constants
{
    public class HttpResponseMessageConstants
    {
        #region Properties

        /// <summary>
        ///     Credential is invalid.
        /// </summary>
        public const string InvalidCredential = "invalid_credential";

        #endregion
    }
}