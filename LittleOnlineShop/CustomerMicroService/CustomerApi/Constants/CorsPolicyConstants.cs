﻿namespace CustomerApi.Constants
{
    public class CorsPolicyConstants
    {
        #region Properties

        /// <summary>
        ///     Unblock cors.
        /// </summary>
        public const string AllowAll = nameof(AllowAll);

        #endregion
    }
}