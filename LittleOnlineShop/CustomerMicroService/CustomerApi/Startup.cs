﻿using CustomerApi.Constants;
using CustomerApi.Extensions;
using FluentValidation.AspNetCore;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using SharedMicroService.AuthenticationHandlers.Requirements;

namespace CustomerApi
{
    public class Startup
    {
        #region Properties

        public IConfiguration Configuration { get; }

        #endregion

        #region Methods

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        /// <summary>
        ///     This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            // Setup database.
            services.AddDatabaseSetup(Configuration);

            // Cors setup.
            services.AddCorsSetup(Configuration);

            // Run ioc setup.
            services.AddIocSetup(Configuration);

            // Setup authentication methods.
            services.AddAuthenticationSetup(Configuration);
            
            // Setup rabbit mq.
            services.AddRabbitMqSetup(Configuration);

            // Setup hosted service.
            services.AddHostedServiceSetup();
            
            // Add swagger
            services.AddSwaggerDocument(option =>
            {
                option.DocumentName = "customer-micro-service";
                option.Title = "Customer micro service";
            });
            
            services
                .AddControllers(options =>
                {
                    ////only allow authenticated users
                    var policy = new AuthorizationPolicyBuilder()
                        .RequireAuthenticatedUser()
                        .AddAuthenticationSchemes(IdentityServerAuthenticationDefaults.AuthenticationScheme)
#if !ALLOW_ANONYMOUS
                        .AddRequirements(new SolidUserRequirement())
#endif
                        .Build();

                    options.Filters.Add(new AuthorizeFilter(policy));
                })
                .AddFluentValidation(options =>
                    options.RegisterValidatorsFromAssembly(typeof(Startup).Assembly))
                .AddNewtonsoftJson(options =>
                {
                    var camelCasePropertyNamesContractResolver = new CamelCasePropertyNamesContractResolver();
                    options.SerializerSettings.ContractResolver = camelCasePropertyNamesContractResolver;
                    options.SerializerSettings.DefaultValueHandling = DefaultValueHandling.Ignore;
                })
                .SetCompatibilityVersion(CompatibilityVersion.Version_3_0);
        }

        /// <summary>
        ///     This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();
            else
                app.UseHsts();

            // Use routing middleware.
            app.UseRouting();
            
            // Seed authentication database.
            //AuthenticationSetup.Seed(app);

            // Seed the identity server default value.
            app.UseIdentityServerItems();
            
            // Seed customer micro service default value.
            app.UseDatabaseDefaultItems();

            // Add static file middleware.
            app.UseStaticFiles();

            // Apply cors policy.
            app.UseCors(CorsPolicyConstants.AllowAll);
            app.UseHttpsRedirection();

            // Enable identity server.
            app.UseIdentityServer();

            // Enable authentication.
            app.UseAuthentication();
            
            // Enable swagger documentation.
            app.UseOpenApi();
            app.UseSwaggerUi3();

            // Enable mvc pipeline.
            app
                .UseEndpoints(endpointBuilder => endpointBuilder.MapControllers());
        }

        #endregion
    }
}