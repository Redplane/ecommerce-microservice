﻿using System;
using System.Threading.Tasks;
using CustomerApi.Cqrs.Commands;
using CustomerApi.Cqrs.Queries;
using CustomerApi.ViewModels;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CustomerApi.Controllers
{
    [Route("api/user")]
    public class UserController : Controller
    {
        #region Properties

        private readonly IMediator _mediator;

        #endregion

        #region Constructor

        public UserController(IMediator mediator)
        {
            _mediator = mediator;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Find user profile asynchronously.
        /// </summary>
        /// <returns></returns>
        [HttpGet("profile/{id?}")]
        public virtual async Task<IActionResult> FindUserProfileAsync([FromRoute] Guid? id)
        {
            var command = new FindUserProfileQuery();
            command.Id = id;

            var user = await _mediator.Send(command);
            return Ok(user);
        }

        /// <summary>
        ///     Load customer asynchronously.
        /// </summary>
        /// <returns></returns>
        [HttpPost("search")]
        public virtual async Task<IActionResult> LoadCustomers([FromBody] LoadCustomerViewModel condition)
        {
            return Ok();
        }


#if DEBUG

        /// <summary>
        ///     Add a customer into system.
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost("")]
        [AllowAnonymous]
        public async Task<IActionResult> Post([FromBody] AddUserCommand command)
        {
            var user = await _mediator.Send(command);
            return Ok(user);
        }

#endif

        #endregion
    }
}