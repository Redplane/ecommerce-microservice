﻿using System.Linq;
using CustomerApi.Cqrs.Commands;
using FluentValidation;
using SharedDomain.Constants;

namespace CustomerApi.Validators
{
    public class AddUserCommandValidator : AbstractValidator<AddUserCommand>
    {
        public AddUserCommandValidator()
        {
            RuleFor(command => command.Username)
                .NotEmpty();

            RuleFor(command => command.Email)
                .NotEmpty();

            RuleFor(command => command.Password)
                .NotEmpty();

            RuleFor(command => command.FullName)
                .NotEmpty();

            RuleFor(command => command.AuthenticationProvider)
                .IsInEnum();

            RuleFor(command => command.Status)
                .IsInEnum();

            RuleFor(command => command.Role)
                .Must(role => RoleConstants.ValidRoles.Contains(role))
                .WithMessage(command =>
                    $"{nameof(command.Role)} must be one of {string.Join(',', RoleConstants.ValidRoles)}");
        }
    }
}