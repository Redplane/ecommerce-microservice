﻿using CustomerApi.Cqrs.Commands;
using FluentValidation;

namespace CustomerApi.Validators
{
    public class UpdateCustomerAccountValidator : AbstractValidator<UpdateCustomerCommand>
    {
        public UpdateCustomerAccountValidator()
        {
            RuleFor(command => command.Id).NotEmpty()
                .WithMessage("Id can not be empty or default value");
            RuleFor(command => command.AvailableMoney).GreaterThan(0)
                .WithMessage("AvalableMoney must be greater than value 0");
            RuleFor(command => command.CurrentMoney).GreaterThan(0)
                .WithMessage("CurrentMoney must be greater than value 0");
            RuleFor(command => command.AvailableMoney).GreaterThan(curr => curr.CurrentMoney)
                .WithMessage("AvailableMoney can not be greater than CurrentMoney");
        }
    }
}