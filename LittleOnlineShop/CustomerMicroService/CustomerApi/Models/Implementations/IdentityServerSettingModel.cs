﻿using System.Collections.Generic;
using CustomerInfrastructure.Models;
using IdentityServer4.Models;
using SharedInfrastructure.Models;

namespace CustomerApi.Models.Implementations
{
    public class IdentityServerSettingModel
    {
        #region Properties

        public IdentityServerAuthenticationModel Default { get; set; }

        public List<IdentityServerClientSettingModel> Clients { get; set; }

        public List<IdentityServerApiResourceSettingModel> ApiResources { get; set; }

        public List<IdentityResource> IdentityResources { get; set; }

        #endregion
    }
}