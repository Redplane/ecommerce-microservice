﻿using System;
using System.Collections.Generic;
using System.Linq;
using CustomerApi.Constants;
using CustomerDomain.Entities;
using CustomerDomain.Enums;
using CustomerDomain.Models;
using CustomerInfrastructure.DbContexts;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using SharedDomain.Extensions;

namespace CustomerApi.Extensions
{
    public static class DatabaseSetupExtensions
    {
        #region Methods
        
        /// <summary>
        /// Add database setup into customer micro service.
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        public static void AddDatabaseSetup(this IServiceCollection services, IConfiguration configuration)
        {
//            var migrationsAssembly = typeof(Startup).GetTypeInfo().Assembly.GetName().Name;
//            services.AddDbContext<CustomerDbContext>(optionsBuilder => optionsBuilder
//                .UseSqlServer(configuration.GetConnectionString(ConnectionStringKeyConstants.CustomerDatabase),
//                    b => b.MigrationsAssembly(migrationsAssembly)));
//
            services.AddDbContext<CustomerDbContext>(builder =>
                builder.UseInMemoryDatabase(ConnectionStringKeyConstants.CustomerDatabase));
        }

        /// <summary>
        /// Seed database items for customer micro service.
        /// </summary>
        /// <param name="app"></param>
        public static void UseDatabaseDefaultItems(this IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.CreateScope())
            {
                var serviceProvider = serviceScope.ServiceProvider;
                var customerDbContext = serviceProvider.GetService<CustomerDbContext>();
                
                if (!customerDbContext.Users.Any())
                {
                    var linhNguyen = new User(Guid.Parse("b5b4feef-3cda-4504-8881-c22f02392fb0"), "linh.nguyen");
                    linhNguyen.Email = "linh.nguyen@gmail.com";
                    linhNguyen.HashedPassword = "5773961b8fb0e85fa14aec3681647c7d";
                    linhNguyen.Birthday = new DateTime(1994, 8, 19);
                    linhNguyen.Balance = 2500;
                    linhNguyen.FullName = "Nguyen Duy Linh";
                    linhNguyen.AuthenticationProvider = AuthenticationProviders.Basic;
                    linhNguyen.Status = UserStatuses.Active;
                    linhNguyen.Role = "admin";
                    linhNguyen.JoinedTime = DateTime.UtcNow.ToUnixTime();

                    customerDbContext.Users.Add(linhNguyen);
                }

                customerDbContext.SaveChanges();
            }
        }
        
        #endregion
    }
}