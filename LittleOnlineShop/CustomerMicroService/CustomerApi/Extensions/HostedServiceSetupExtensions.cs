﻿using CustomerApi.HostedServices;
using Microsoft.Extensions.DependencyInjection;

namespace CustomerApi.Extensions
{
    public static class HostedServiceExtensions
    {
        public static void AddHostedServiceSetup(this IServiceCollection services)
        {
            // Add hosted services.
            services.AddHostedService<MqHostedService>();
        }
    }
}