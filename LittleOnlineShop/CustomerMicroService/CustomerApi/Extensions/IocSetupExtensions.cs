﻿using System.Reflection;
using CustomerApi.Cqrs.Pipelines.Behaviours;
using CustomerApi.Services.Implementations.Authentications;
using CustomerApi.Services.Implementations.Authentications.ExtensionGrants;
using CustomerApi.Services.Interfaces;
using CustomerInfrastructure.Factories.Implementations;
using CustomerInfrastructure.Factories.Interfaces;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SharedInfrastructure.Services.Implementations;
using SharedInfrastructure.Services.Interfaces;

namespace CustomerApi.Extensions
{
    public static class IocSetupExtensions
    {
        /// <summary>
        ///     Run Ioc setup.
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        public static void AddIocSetup(this IServiceCollection services, IConfiguration configuration)
        {
            // Register configuration.
            services.AddSingleton(configuration);

            // Register services.
            services.AddScoped<IBaseEncryptionService, BaseEncryptionService>();
            services.AddScoped<IExternalAuthenticationService, ExternalAuthenticationService>();

            // Add http client injectors.
            services.AddHttpClient();

            // Add http client to external authentication service.
            services.AddHttpClient<IExternalAuthenticationService, ExternalAuthenticationService>();

            services.AddHttpContextAccessor();

            // Register factory.
            services.AddScoped<IUserFactory, UserFactory>();

            // Add mediatr.
            services.AddMediatR(typeof(Startup).GetTypeInfo().Assembly);

            // Auto validate command
            services.AddScoped(typeof(IPipelineBehavior<,>), typeof(RequestValidationBehavior<,>));

            // Register grant.
            services.AddScoped<GoogleExtensionGrant>();
            services.AddScoped<FacebookExtensionGrant>();
        }
    }
}