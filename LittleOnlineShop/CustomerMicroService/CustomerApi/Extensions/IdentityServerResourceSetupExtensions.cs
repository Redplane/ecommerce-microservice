﻿using System.Collections.Generic;
using CustomerApi.Models.Implementations;
using CustomerInfrastructure.Models;
using IdentityServer4.EntityFramework.DbContexts;
using IdentityServer4.EntityFramework.Mappers;
using IdentityServer4.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace CustomerApi.Extensions
{
    public static class IdentityServerResourceSetupExtensions
    {
        #region Methods

        /// <summary>
        ///     Load pre-defined api resources.
        /// </summary>
        /// <returns></returns>
        private static IEnumerable<ApiResource> LoadApiResources(
            List<IdentityServerApiResourceSettingModel> apiResourceSettings)
        {
            var apiResources = new List<ApiResource>();

            if (apiResourceSettings == null || apiResourceSettings.Count < 1)
                return apiResources;

            foreach (var apiResourceSetting in apiResourceSettings)
            {
                var apiResource = new ApiResource(apiResourceSetting.Name, apiResourceSetting.DisplayName,
                    apiResourceSetting.UserClaims);
                apiResource.Description = apiResourceSetting.Description;

                // Build secrets list.
                var apiSecrets = apiResourceSetting.ApiSecrets;
                if (apiSecrets != null && apiSecrets.Count > 0)
                {
                    var encryptedSecrets = new List<Secret>();
                    foreach (var apiSecret in apiSecrets)
                    {
                        if (string.IsNullOrWhiteSpace(apiSecret))
                            continue;

                        var encryptedSecret = new Secret(apiSecret.Sha256());
                        encryptedSecrets.Add(encryptedSecret);
                    }

                    apiResource.ApiSecrets = encryptedSecrets;
                }

                apiResource.Enabled = apiResourceSetting.Enabled;
                apiResource.Properties = apiResourceSetting.Properties;

                // Add the api resource to list.
                apiResources.Add(apiResource);
            }

            return apiResources;
        }

        /// <summary>
        ///     Load pre-defined clients.
        /// </summary>
        /// <returns></returns>
        private static IEnumerable<Client> LoadClients(
            List<IdentityServerClientSettingModel> identityServerClientSettings)
        {
            var clients = new List<Client>();

            if (identityServerClientSettings == null || identityServerClientSettings.Count < 1)
                return clients;

            foreach (var identityServerClientSetting in identityServerClientSettings)
            {
                var client = new Client();
                client.ClientId = identityServerClientSetting.ClientId;

                // Secrets are defined.
                var clientSecrets = identityServerClientSetting.ClientSecrets;
                if (clientSecrets != null && clientSecrets.Count > 0)
                {
                    client.ClientSecrets = new List<Secret>();
                    foreach (var clientSecret in clientSecrets)
                    {
                        // Secret is empty.
                        if (string.IsNullOrWhiteSpace(clientSecret))
                            continue;

                        client.ClientSecrets.Add(new Secret(clientSecret.Sha256()));
                    }
                }

                client.AllowedGrantTypes = identityServerClientSetting.AllowedGrantTypes;
                client.AllowedScopes = identityServerClientSetting.AllowedScopes;
                client.AllowOfflineAccess = identityServerClientSetting.AllowOfflineAccess;
                client.RefreshTokenExpiration = identityServerClientSetting.RefreshTokenExpiration;
                client.RefreshTokenUsage = identityServerClientSetting.RefreshTokenUsage;
                client.SlidingRefreshTokenLifetime = identityServerClientSetting.SlidingRefreshTokenLifeTime;

                clients.Add(client);
            }

            return clients;
        }

        /// <summary>
        ///     Install default identity server settings.
        /// </summary>
        /// <param name="applicationBuilder"></param>
        public static void UseIdentityServerItems(this IApplicationBuilder applicationBuilder)
        {
            var serviceProviders = applicationBuilder.ApplicationServices;

            using (var serviceScope = serviceProviders.CreateScope())
            {
                var identityServerSetting = serviceScope.ServiceProvider
                    .GetService<IOptions<IdentityServerSettingModel>>()?.Value;

                if (identityServerSetting == null)
                    return;

                // Get configuration database context.
                var configurationDbContext = serviceScope.ServiceProvider.GetService<ConfigurationDbContext>();

                if (configurationDbContext == null)
                    return;

                // Get clients settings.
                var clients = identityServerSetting.Clients;
                var identityResources = identityServerSetting.IdentityResources;
                var apiResources = identityServerSetting.ApiResources;

                // Whether context has been modified or not.
                var hasContextModified = false;

                // No client has been added into the database.
                var hasAvailableClients = configurationDbContext.Clients.AnyAsync().Result;
                if (!hasAvailableClients && clients != null && clients.Count > 0)
                {
                    foreach (var client in LoadClients(clients))
                    {
                        configurationDbContext.Clients.Add(client.ToEntity());
                        hasContextModified = true;
                    }
                }
                
                // No identity resource has been added to database.
                var hasAvailableIdentityResources = configurationDbContext.IdentityResources.AnyAsync().Result;
                if (!hasAvailableIdentityResources && identityResources != null &&
                    identityResources.Count > 0)
                {
                    foreach (var identityResource in identityServerSetting.IdentityResources)
                    {
                        configurationDbContext.IdentityResources.Add(identityResource.ToEntity());
                        hasContextModified = true;
                    }
                }
                

                // No api resource has been added into database.
                var hasAvailableApiResources = configurationDbContext.ApiResources.AnyAsync().Result;
                if (!hasAvailableApiResources && apiResources != null && apiResources.Count > 0)
                {
                    foreach (var apiResource in LoadApiResources(identityServerSetting.ApiResources))
                    {
                        configurationDbContext.ApiResources.Add(apiResource.ToEntity());
                        hasContextModified = true;
                    }
                }

                if (hasContextModified)
                    configurationDbContext.SaveChanges();
            }
        }

        #endregion
    }
}