﻿using System;
using System.Reflection;
using CustomerApi.Constants;
using CustomerApi.Models.Implementations;
using CustomerApi.Services.Implementations.Authentications;
using CustomerApi.Services.Implementations.Authentications.ExtensionGrants;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Logging;
using SharedMicroService.AuthenticationHandlers.Implementations;

namespace CustomerApi.Extensions
{
    public static class AuthenticationSetupExtensions
    {
        #region Methods

        public static void AddAuthenticationSetup(this IServiceCollection services, IConfiguration configuration)
        {
            // Get identity server settings.
            var identityServerSettings = new IdentityServerSettingModel();

            var identityServerSetting = configuration.GetSection(AppSettingConstants.IdentityServerSettings);
            services.Configure<IdentityServerSettingModel>(identityServerSetting);
            identityServerSetting
                .Bind(identityServerSettings);

            // No default configuration for authentication is found.
            if (identityServerSettings.Default == null)
                throw new Exception("No default configuration for authentication is found");

            var customerIdentityServerMsConnectionString =
                configuration.GetConnectionString(ConnectionStringKeyConstants.IdentityServer);
            var migrationsAssembly = typeof(Startup).GetTypeInfo().Assembly.GetName().Name;

            // Authorization handlers.
            services.AddScoped(typeof(IAuthorizationHandler), typeof(SolidUserRequirementHandler));

            services
                .AddIdentityServer()
                .AddConfigurationStore(options =>
                {
//                    options.ConfigureDbContext = builder =>
//                        builder.UseSqlServer(customerIdentityServerMsConnectionString,
//                            sql => sql.MigrationsAssembly(migrationsAssembly));

                    options.ConfigureDbContext = builder =>
                        builder.UseInMemoryDatabase(ConnectionStringKeyConstants.CustomerDatabase);
                })
                .AddOperationalStore(options =>
                {
//                    options.ConfigureDbContext = builder =>
//                        builder.UseSqlServer(customerIdentityServerMsConnectionString,
//                            sql => sql.MigrationsAssembly(migrationsAssembly));

                    options.ConfigureDbContext = builder =>
                        builder.UseInMemoryDatabase(ConnectionStringKeyConstants.CustomerDatabase);

                    // this enables automatic token cleanup. this is optional.
                    options.EnableTokenCleanup = true;
                    options.TokenCleanupInterval = 30; // interval in seconds
                })
                .AddProfileService<ProfileService>()
                .AddResourceOwnerValidator<ResourceOwnerPasswordValidator>()
                .AddExtensionGrantValidator<GoogleExtensionGrant>()
                .AddExtensionGrantValidator<FacebookExtensionGrant>()
                .AddDeveloperSigningCredential();

            // Add jwt validation.
            services.AddAuthentication(options =>
                {
                    options.DefaultScheme = IdentityServerAuthenticationDefaults.AuthenticationScheme;
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddIdentityServerAuthentication(options =>
                {
                    options.Authority = identityServerSettings.Default.Authority;
                    options.ApiName = identityServerSettings.Default.ApiName;
                    options.ApiSecret = identityServerSettings.Default.ApiSecret;
                    options.RequireHttpsMetadata = identityServerSettings.Default.RequireHttpsMetaData;
                    options.SaveToken = identityServerSettings.Default.SaveToken;
                    options.SupportedTokens = identityServerSettings.Default.SupportedToken;
                });

#if DEBUG
            IdentityModelEventSource.ShowPII = true;
#endif
        }

        #endregion
    }
}