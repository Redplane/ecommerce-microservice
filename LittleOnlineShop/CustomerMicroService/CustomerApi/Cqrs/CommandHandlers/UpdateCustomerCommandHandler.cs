﻿using System.Threading;
using System.Threading.Tasks;
using CustomerApi.Cqrs.Commands;
using CustomerInfrastructure.Factories.Interfaces;
using MediatR;

namespace CustomerApi.Cqrs.CommandHandlers
{
    public class UpdateCustomerCommandHandler : IRequestHandler<UpdateCustomerCommand, bool>
    {
        private readonly IMediator _mediator;
        private readonly IUserFactory _userFactory;

        /// <summary>
        ///     Inject DI
        /// </summary>
        public UpdateCustomerCommandHandler(IMediator mediator,
            IUserFactory userFactory)
        {
            _mediator = mediator;
            _userFactory = userFactory;
        }

        public async Task<bool> Handle(UpdateCustomerCommand request, CancellationToken cancellationToken)
        {
            return true;
        }
    }
}