﻿using System.Threading;
using System.Threading.Tasks;
using CustomerApi.Cqrs.Commands;
using CustomerDomain.Entities;
using CustomerInfrastructure.Factories.Interfaces;
using MediatR;

namespace CustomerApi.Cqrs.CommandHandlers
{
    /// <summary>
    ///     This class for implement create a new customer
    /// </summary>
    public class AddUserCommandHandler : IRequestHandler<AddUserCommand, User>
    {
        #region Fields

        /// <summary>
        ///     The Factory For Make CRUD Customer
        /// </summary>
        private readonly IUserFactory _userFactory;

        #endregion

        #region Contructor

        /// <summary>
        ///     Inject IUserFactory and IMediator to constructor
        /// </summary>
        public AddUserCommandHandler(
            IUserFactory userFactory)
        {
            _userFactory = userFactory;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Handle function for create new customer
        /// </summary>
        /// <param name="command"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task<User> Handle(AddUserCommand command, CancellationToken cancellationToken)
        {
            var user = await _userFactory.AddUserAsync(command.Username, command.Email, command.Password,
                command.FullName, command.Birthday,
                command.Balance, command.AuthenticationProvider, command.Status, cancellationToken);

            return user;
        }

        #endregion
    }
}