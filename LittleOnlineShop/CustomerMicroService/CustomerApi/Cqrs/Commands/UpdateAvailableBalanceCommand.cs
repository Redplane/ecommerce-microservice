﻿using System;
using System.Runtime.Serialization;
using MediatR;

namespace CustomerApi.Cqrs.Commands
{
    /// <summary>
    ///     This class work for update current money balanceValueObject
    /// </summary>
    [DataContract]
    public class UpdateAvailableBalanceCommand : IRequest<bool>
    {
        /// <summary>
        ///     Customer Id
        /// </summary>
        [DataMember]
        public Guid CustomerId { get; set; }

        /// <summary>
        ///     Total money to update
        /// </summary>
        [DataMember]
        public double Total { get; set; }
    }
}