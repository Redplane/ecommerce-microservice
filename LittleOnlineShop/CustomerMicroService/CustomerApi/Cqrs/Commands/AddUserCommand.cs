﻿using CustomerDomain.Entities;
using CustomerDomain.Enums;
using CustomerDomain.Models;
using MediatR;
using SharedDomain.Models;

namespace CustomerApi.Cqrs.Commands
{
    public class AddUserCommand : IRequest<User>
    {
        #region Properties

        /// <summary>
        ///     Account username.
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        ///     Account email.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        ///     Password for account.
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        ///     Customer full name.
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        ///     Customer birthday.
        /// </summary>
        public DateValueObject Birthday { get; set; }

        /// <summary>
        ///     Authentication provider for user account.
        /// </summary>
        public AuthenticationProviders AuthenticationProvider { get; set; }

        /// <summary>
        ///     Role that is assigned to user.
        /// </summary>
        public string Role { get; }

        /// <summary>
        ///     Account status.
        /// </summary>
        public UserStatuses Status { get; set; }

        public decimal Balance { get; set; }

        #endregion
    }
}