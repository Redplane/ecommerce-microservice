﻿using System;
using System.Runtime.Serialization;
using MediatR;

namespace CustomerApi.Cqrs.Commands
{
    /// <summary>
    ///     Update Customer Command
    /// </summary>
    public class UpdateCustomerCommand : IRequest<bool>
    {
        /// <summary>
        ///     Identity Id
        /// </summary>
        [DataMember]
        public Guid Id { get; set; }

        /// <summary>
        ///     Address1
        /// </summary>
        [DataMember]
        public string Address1 { get; set; }

        /// <summary>
        ///     Address2
        /// </summary>
        [DataMember]
        public string Address2 { get; set; }

        /// <summary>
        ///     Phone1
        /// </summary>
        [DataMember]
        public string Phone1 { get; set; }

        /// <summary>
        ///     Phone2
        /// </summary>
        [DataMember]
        public string Phone2 { get; set; }

        /// <summary>
        ///     RemainingBalance Money
        /// </summary>
        [DataMember]
        public double CurrentMoney { get; set; }

        /// <summary>
        ///     AvailableBalance Money
        /// </summary>
        [DataMember]
        public double AvailableMoney { get; set; }
    }
}