using System;
using System.Collections.Generic;
using CustomerDomain.Entities;
using CustomerDomain.Enums;
using MediatR;
using SharedDomain.Models;

namespace CustomerApi.Cqrs.Queries
{
    public class FindUsersQuery : IRequest<SearchResultValueObject<User>>
    {
        #region Properties

        /// <summary>
        /// Ids of user.
        /// </summary>
        public HashSet<Guid> Ids { get; set; }

        /// <summary>
        /// Usernames to be searched.
        /// </summary>
        public HashSet<string> Usernames { get; set; }

        /// <summary>
        /// Emails to be searched.
        /// </summary>
        public HashSet<string> Email { get; set; }

        /// <summary>
        /// Authentication provider to be searched.
        /// </summary>
        public HashSet<AuthenticationProviders> AuthenticationProviders { get; set; }

        /// <summary>
        /// User statuses.
        /// </summary>
        public HashSet<UserStatuses> Statuses { get; set; }
        
        public HashSet<string> Roles { get; set; }
        
        /// <summary>
        /// Pager information.
        /// </summary>
        public PagerValueObject Pager { get; set; }
        
        #endregion
    }
}