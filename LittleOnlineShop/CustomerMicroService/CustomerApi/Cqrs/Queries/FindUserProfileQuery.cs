﻿using System;
using CustomerDomain.Entities;
using MediatR;

namespace CustomerApi.Cqrs.Queries
{
    public class FindUserProfileQuery : IRequest<User>
    {
        #region Properties

        /// <summary>
        ///     Id of user to be retrieved.
        /// </summary>
        public Guid? Id { get; set; }

        #endregion
    }
}