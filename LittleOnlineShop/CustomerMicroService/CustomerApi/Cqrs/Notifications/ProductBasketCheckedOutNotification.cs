using System;
using MediatR;
using SharedInfrastructure.Models.MqMessages;

namespace CustomerApi.Cqrs.Notifications
{
    public class ProductBasketCheckedOutNotification : ProductBasketCheckedOutMqMessage, INotification
    {
        #region Constructors
        
        public ProductBasketCheckedOutNotification(ProductBasketCheckedOutMqMessage mqMessage) 
            : base(mqMessage.CustomerId, mqMessage.ProductBasketId, mqMessage.TotalMoney)
        {
        }
        
        #endregion
    }
}