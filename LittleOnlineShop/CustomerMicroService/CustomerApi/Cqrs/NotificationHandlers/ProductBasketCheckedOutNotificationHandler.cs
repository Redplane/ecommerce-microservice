using System;
using System.Threading;
using System.Threading.Tasks;
using CustomerApi.Cqrs.Notifications;
using CustomerInfrastructure.Factories.Interfaces;
using MediatR;
using Microsoft.Extensions.Logging;
using SharedInfrastructure.Constants;
using SharedInfrastructure.Models.MqMessages;
using SharedInfrastructure.Services.Interfaces;

namespace CustomerApi.Cqrs.NotificationHandlers
{
    public class ProductBasketCheckedOutNotificationHandler : INotificationHandler<ProductBasketCheckedOutNotification>
    {
        #region Properties

        private readonly ILogger<ProductBasketCheckedOutNotificationHandler> _logger;

        private readonly IUserFactory _userFactory;

        private readonly IMqManagerService _mqManagerService;

        #endregion

        #region Constructor

        public ProductBasketCheckedOutNotificationHandler(ILogger<ProductBasketCheckedOutNotificationHandler> logger,
            IUserFactory userFactory,
            IMqManagerService mqManagerService)
        {
            _logger = logger;
            _userFactory = userFactory;
            _mqManagerService = mqManagerService;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Handle the notification about checked out product basket.
        /// </summary>
        /// <param name="notification"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task Handle(ProductBasketCheckedOutNotification notification, CancellationToken cancellationToken)
        {
            try
            {
                await _userFactory.DeductUserBalanceAsync(notification.CustomerId, notification.TotalMoney,
                    cancellationToken);

                _logger.LogInformation(
                    $"Deducted user [{notification.CustomerId}] money with value: {notification.TotalMoney} for product basket id: {notification.ProductBasketId}");

                var successfulBalanceDeductionMqMessage =
                    new SuccessfulBalanceDeductionMqMessage(notification.ProductBasketId, notification.CustomerId);
                
                await _mqManagerService.PublishMqMessageAsync(MqChannelNameConstant.Customer,
                    successfulBalanceDeductionMqMessage, cancellationToken);

                _logger.LogInformation(
                    $"Sent message: {successfulBalanceDeductionMqMessage.EventName} through {MqChannelNameConstant.Customer} with info {successfulBalanceDeductionMqMessage.ToJson()}");
            }
            catch (Exception exception)
            {
                var failedBalanceDeductionMqMessage = new FailedBalanceDeductionMqMessage(notification.ProductBasketId, notification.CustomerId);
                await _mqManagerService.PublishMqMessageAsync(MqChannelNameConstant.Customer,
                    failedBalanceDeductionMqMessage, cancellationToken);
                
                _logger.LogError(exception.Message, exception);
            }
        }

        #endregion
    }
}