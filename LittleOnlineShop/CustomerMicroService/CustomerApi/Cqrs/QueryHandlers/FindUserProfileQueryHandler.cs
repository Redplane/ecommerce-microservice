﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CustomerApi.Cqrs.Queries;
using CustomerDomain.Entities;
using CustomerInfrastructure.Factories.Interfaces;
using MediatR;
using Microsoft.AspNetCore.Http;
using SharedMicroService.Extensions;

namespace CustomerApi.Cqrs.QueryHandlers
{
    public class FindUserProfileQueryHandler : IRequestHandler<FindUserProfileQuery, User>
    {
        #region Constructor

        public FindUserProfileQueryHandler(IUserFactory userFactory, IHttpContextAccessor httpContextAccessor)
        {
            _userFactory = userFactory;
            _httpContext = httpContextAccessor.HttpContext;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Find user profile by exchanging access_token when id is not specified.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task<User> Handle(FindUserProfileQuery query, CancellationToken cancellationToken)
        {
            // Get user id that will be used for query purposes.
            var userId = query.Id;

            // No id is specified, that means 
            if (userId == null)
                userId = _httpContext.FindProfile(true)?.Id;

            if (userId == null)
                return null;

            var users = await _userFactory.FindUsersByIdsAsync(new HashSet<Guid> {userId.Value}, cancellationToken);
            if (users == null)
                return null;

            return users.FirstOrDefault();
        }

        #endregion

        #region Properties

        private readonly IUserFactory _userFactory;

        private readonly HttpContext _httpContext;

        #endregion
    }
}