using System.Threading;
using System.Threading.Tasks;
using CustomerApi.Cqrs.Queries;
using CustomerDomain.Entities;
using CustomerInfrastructure.Factories.Interfaces;
using MediatR;
using Microsoft.AspNetCore.Http;
using SharedDomain.Models;

namespace CustomerApi.Cqrs.QueryHandlers
{
    public class FindUsersQueryHandler : IRequestHandler<FindUsersQuery, SearchResultValueObject<User>>
    {
        #region Properties

        private readonly IUserFactory _userFactory;

        private readonly HttpContext _httpContext;

        #endregion

        #region Constructors

        public FindUsersQueryHandler(IUserFactory userFactory, HttpContext httpContext)
        {
            _userFactory = userFactory;
            _httpContext = httpContext;
        }

        #endregion

        #region Methods

        public virtual Task<SearchResultValueObject<User>> Handle(FindUsersQuery request, CancellationToken cancellationToken)
        {
            throw new System.NotImplementedException();
        }

        #endregion
    }
}