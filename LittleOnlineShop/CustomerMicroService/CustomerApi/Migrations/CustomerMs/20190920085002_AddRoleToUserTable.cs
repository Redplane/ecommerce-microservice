﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CustomerApi.Migrations.CustomerMs
{
    public partial class AddRoleToUserTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                "Username",
                "Users",
                nullable: false,
                oldClrType: typeof(string));

            migrationBuilder.AddColumn<string>(
                "Role",
                "Users",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateIndex(
                "IX_Users_Username",
                "Users",
                "Username",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                "IX_Users_Username",
                "Users");

            migrationBuilder.DropColumn(
                "Role",
                "Users");

            migrationBuilder.AlterColumn<string>(
                "Username",
                "Users",
                nullable: false,
                oldClrType: typeof(string));
        }
    }
}