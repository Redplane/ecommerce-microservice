﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CustomerApi.Migrations.CustomerMs
{
    public partial class ChangedUsernameToRequired : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                "Username",
                "Users",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                "Username",
                "Users",
                nullable: true,
                oldClrType: typeof(string));
        }
    }
}