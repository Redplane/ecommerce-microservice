﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CustomerApi.Migrations.CustomerMs
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                "Users",
                table => new
                {
                    Id = table.Column<Guid>(),
                    Username = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    HashedPassword = table.Column<string>(nullable: true),
                    Birthday = table.Column<DateTime>("Date", nullable: true),
                    RemainingBalance = table.Column<double>(),
                    AvailableBalance = table.Column<double>(),
                    FullName = table.Column<string>(nullable: true),
                    AuthenticationProvider = table.Column<int>(),
                    Status = table.Column<int>(),
                    JoinedTime = table.Column<double>(),
                    LastModifiedTime = table.Column<double>(nullable: true)
                },
                constraints: table => { table.PrimaryKey("PK_Users", x => x.Id); });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                "Users");
        }
    }
}