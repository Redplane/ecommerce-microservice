# SEQUENCE DIAGRAM

## Description:
- Show system flow in diagram.

## How to use
- Please access to [https://sequencediagram.org/](https://sequencediagram.org/) to view sequence diagram.